
## xPort 是什么

xPort 是 DXMesh 的运行时，负责加载和运行 DXC(DXMesh Component 应用组件)。xPort 为 DXC 提供以下功能：

- 消息的发送 / 接收
- 节点间传输
- 日志收集
- 服务的加载/卸载


xPort 内置基于 protobuf 开发的 RPC 协议：xRPC。xRPC 用于 xPort 间通讯，对于应用是透明的。

## 使用方法

- 在任一目录下 下载代码, 命令如下：

  ```git
  git clone  https://gitee.com/DXmesh/xPort.git
  ```

  
  代码下载完成后，目录如下：
  ```
  ├─xPort
      ├─protos
      └─src
          ├─airport
          ├─log
          ├─net
          ├─protocol
          └─service
              └─inject_api
  ```

## 编译 xPort

进入 xPort 目录，在控制台执行命令：

```
cargo build
```
运行 xPort
```
cargo run
```


## 主要代码目录说明

- airport   
  该目录做为 xPort 消息的分发中心，所有的消息，都是进入 airport.rs 然后再由其根据消息所使用的协议，将消息分发给本地的服务，或者远程的服务。组成文件由：
  - channel_manager.rs 通道管理，所有和本节点（xPort）连接的通道，都存放于此。目前的通道由 TCP 通道。
  - channel.rs 消息通道 trait 目前只有 tcp 实现
  - dxc_manager.rs 服务组件管理，所有的服务组件存放于此，负责本地服务的加载/卸载，远程服务的创建等。
  - dxc.rs 对应每一个服务组件，管理本地服务和远程服务
  - message_manager.rs 消息中心，所有接收到的消息都存放于此。
  - tcp_channel.rs  channel.rs 的 tcp 实现。

- log
  日志中心，收集 xPort 和 DXC 所输出的日志。

- net
  网络层，目前支持 tcp。

  - tcp_net.rs

    负责 xRPC（节点间的通信协议） 的消息，在节点间流转。

- protocol

  xRPC 的协议，目前使用的协议为 v1。

- service
  服务组件的实例，目前分本地服务（local service）和远程服务（remote service）。本地服务为本节点启动的服务，当前支持的服务类型为动态库，后面会支持基于 WASM 编写的服务。

  - inject 该目录提供的二进制的 API 接口，当基于动态库开发的应用组件，被加载时，这些API接口会注入到组件中。目前提供的接口有和 xPort 交互的接口、HTTP 接口、文件操作接口。
  - loader.rs 为服务加载器，目前只支持基于动态库编写的 dxc
  - sys_service_api 为 xPort 调用 [xport_service](https://gitee.com/DXmesh/xPortService) 服务的接口


## 配置说明

配置文件为 在 xPort 目录下 的 dxmesh.toml 文件，内容如下：

``` toml
[xport]
xrpc-ip = "127.0.0.1" # xrpc 监听ip 默认为 0.0.0.0
xrpc-port = "8090"   # xrpc 监听ip 默认为 8090
file-path="./file"
log-path = "./log"
log-level = "info"
log-writer = ["console"]
default-dxc = ["XComService-0.0.1"] # 默认启动的 dxc
message-max-size = "10M"


# 默认启动时，会将以下配置，传给 dxc
["XComService-0.0.1"]
system = true
```
具体说明如下：

- xrpc-ip  xrpc 监听ip 默认为 0.0.0.0
- xrpc-port  xrpc 监听ip 默认为 8090
- file-path  xPort 接收到的文件存储的路径，默认为 ./file
- log-path 日志文件输出路径，默认为   ./log
- log-level 日志输出等级，默认为 info
- log-writer 日志输出的地方，目前支持输出到控制台和文件，输出到文件时必选项
- default-dxc 默认启动的 dxc，如果配置文件中，出现和 dxc 名字一样的 配置，则该配置内容会在dxc 启动时，传入。
- message-max-size xRPC 接收最大的消息长度













