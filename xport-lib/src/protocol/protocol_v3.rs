/**
 * 推送消息
 */
use std::sync::Arc;

use crate::{airport::airport::Airport, application::Application};

use super::{protocol_handler::ProtocolHandler, MsgType};

use async_trait::async_trait;
use tracing::info;
use x_common_lib::{
    base::id_generator::get_dxc_id_by_service_id,
    protocol::{
        protocol_v3::{ProtocolV3Reader, ProtocolV3Writer},
        XID,
    },
    service::sys_service_api::TopicKey,
};
// ==============

pub struct ProtocolV3Handler;

impl ProtocolV3Handler {
    pub fn new() -> Self {
        ProtocolV3Handler {}
    }

    async fn publish_to_dxc(
        conn_id: i64,
        topic_key: &TopicKey,
        xid: XID,
        message: Arc<Vec<u8>>,
    ) -> bool {
        let airport = Application::get_airport();
        let stream = airport.get_stream();

        let subscribes = stream.get_subscribers(conn_id, &topic_key);
        if subscribes.is_empty() {
            return false;
        }
        let message_manager = airport.get_message_manager();
        let dxc_manager = airport.get_dxc_manager();
        for subscribe_id in subscribes.into_iter() {
            let dxc_id = get_dxc_id_by_service_id(subscribe_id);
            let dxc = dxc_manager.get_dxc_by_id(dxc_id).await;
            if dxc.is_none() {
                info!("dxc 不存在");
                continue;
            }
            let dxc = dxc.unwrap();
            message_manager.ref_stream_message(xid);
            // info!("v3 消息分发...");
            let is_succeed = dxc.message_in(&message).await;
            if !is_succeed {
                message_manager.deref_stream_message(&xid);
                continue;
            }
        }

        true
    }

    async fn handle_normal_message(
        &self,
        from_addr: String,
        send_channel_id: i64,
        message: Vec<u8>,
    ) {
        let message = Arc::new(message);

        let protocol_reader = ProtocolV3Reader::new(&message);
        //
        let airport = Application::get_airport();
        let message_manager = airport.get_message_manager();
        let xid = protocol_reader.xid();
        // 将消息添加到消息仓库
        //
        message_manager.add_stream_message(send_channel_id, xid, from_addr, message.clone());
        //
        // let stream = airport.get_stream();
        let topic_key = protocol_reader.topic_key();
        if let Err(err) = topic_key {
            info!("获取消息:{:?} 的topic 失败:{}...", xid, err.err_msg);
            return;
        }
        let topic_key = topic_key.unwrap();
        message_manager.ref_stream_message(xid);
        let found1 = Self::publish_to_dxc(xid.conn_id, &topic_key, xid, message.clone()).await;
        let found2 = Self::publish_to_dxc(0, &topic_key, xid, message).await;
        message_manager.deref_stream_message(&xid);

        if !found1 && !found2 {
            info!("订阅者 为空");
        }
    }
}

#[async_trait]
impl ProtocolHandler for ProtocolV3Handler {
    fn send_msg(&self, _data: Vec<u8>) {}

    async fn handle_msg(
        &self,
        airport: &Airport,
        send_channel_id: i64,
        receive_channel_id: i64,
        mut message: Vec<u8>,
    ) {
        // info!("v3 消息.... send_channel_id {}, receive_channel_id {}", send_channel_id, receive_channel_id);

        if let Err(_) = self
            .check_receive_is_exist(airport, receive_channel_id)
            .await
        {
            info!("接收者不存在...");
            return;
        }

        let check_result = self
            .check_send_channel_have_conn_id(airport, send_channel_id)
            .await;
        if let Err(_err_msg) = check_result {
            info!("检查错误...");
            return;
        }
        ProtocolV3Writer::write_conn_id_to_message(&mut message, check_result.unwrap());
        //
        let v3_reader = ProtocolV3Reader::new(&message);
        let msg_type = v3_reader.msg_type();
        if msg_type.is_none() {
            info!("msg_type, 无法识别...");
            return;
        }
        //
        let msg_type = msg_type.unwrap();
        match msg_type {
            MsgType::Stream => {
                // 校验信息
                let msg_body = v3_reader.msg_body();
                if v3_reader.is_sign() {
                    let signer = Application::get_signer();
                    // 获取签名
                    let ret = signer.verify(msg_body);
                    if let Err(_) = ret {
                        info!("消息 {:?} 签名验证失败。。。", v3_reader.xid());
                        return;
                    }
                    let from_address = ret.unwrap();
                    self.handle_normal_message(from_address, send_channel_id, message)
                        .await
                } else {
                    //
                    self.handle_normal_message(String::default(), send_channel_id, message)
                        .await
                }
            }
            _ => {
                info!("v3 消息协议，不处理类型：{:?} 的消息", msg_type)
            }
        }
    }

    fn get_xid(&self, data: &Vec<u8>) -> XID {
        let protocol_reader = ProtocolV3Reader::new(&data);
        protocol_reader.xid()
    }

    fn get_version(&self) -> u16 {
        3
    }
}
