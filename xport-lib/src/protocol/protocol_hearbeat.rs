use async_trait::async_trait;
use tracing::debug;
use x_common_lib::protocol::XID;

use crate::airport::airport::Airport;

use super::protocol_handler::ProtocolHandler;

pub fn make_hearbeat_message() -> Vec<u8> {
    let mut msg_buffer = Vec::with_capacity(6);

    unsafe {
        msg_buffer.set_len(6);

        let len: u32 = 6;

        let len_bytes = len.to_le_bytes();

        let len_ptr = msg_buffer.as_mut_ptr();

        std::ptr::copy_nonoverlapping(len_bytes.as_ptr(), len_ptr, 4);
        //
        let msg_type: u16 = 0;

        let type_bytes = msg_type.to_le_bytes();

        let type_ptr = msg_buffer.as_mut_ptr().offset(4);

        std::ptr::copy_nonoverlapping(type_bytes.as_ptr(), type_ptr, 2);
    }
    msg_buffer
}

pub struct ProtocolHeartbeatHandler {}

impl ProtocolHeartbeatHandler {
    pub fn new() -> ProtocolHeartbeatHandler {
        ProtocolHeartbeatHandler {}
    }
}

#[async_trait]
impl ProtocolHandler for ProtocolHeartbeatHandler {
    /**
     * 发送消息
     */
    fn send_msg(&self, _data: Vec<u8>) {}
    /**
     * 处理消息
     */
    async fn handle_msg(
        &self,
        _airport: &Airport,
        _send_channel_id: i64,
        _receive_channel_id: i64,
        _data: Vec<u8>,
    ) {
        debug!("{} 心跳...", _send_channel_id);
    }

    /**
     *  获取id
     */
    fn get_xid(&self, _data: &Vec<u8>) -> XID {
        XID::default()
    }

    fn get_version(&self) -> u16 {
        0
    }
}
