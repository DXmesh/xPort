/**
 * 使用v2签名的信息
 */
use std::sync::Arc;

use crate::{
    airport::{airport::Airport, channel::SELF_CHANNEL},
    application::Application,
    event,
    protocol::pack_status_message_from_v2,
};

use super::{protocol_handler::ProtocolHandler, MsgType};

use async_trait::async_trait;
use semver::Version;
use tracing::info;
use x_common_lib::{
    base::status::Status,
    protocol::{
        protocol_v2::{ProtocolV2Reader, ProtocolV2Writer},
        XID,
    },
    service::sys_service_api::ServiceKey,
};
// ==============

pub struct ProtocolV2Handler {
    // message_in_num: AtomicI32,
}

impl ProtocolV2Handler {
    pub fn new() -> Self {
        ProtocolV2Handler {}
    }

    async fn handle_normal_message(
        &self,
        from_addr: String,
        receiver_service_key: Box<ServiceKey>,
        send_channel_id: i64,
        receive_channel_id: i64,
        is_normal: bool,
        message: Vec<u8>,
        is_redirect: bool,
    ) {
        let dxc_version = Version::parse(&receiver_service_key.dxc_version);
        if let Err(_) = dxc_version {
            let err_msg = format!(
                "dxc 版本号：{} 格式出错：",
                receiver_service_key.dxc_version
            );
            ProtocolV2Handler::return_err_msg(send_channel_id, &message, err_msg).await;
            return;
        }

        let dxc_version = dxc_version.unwrap();
        let protocol_reader = ProtocolV2Reader::new(&message);
        //
        let airport = Application::get_airport();
        let message_manager = airport.get_message_manager();
        let xid = protocol_reader.xid();
        let message = Arc::new(message);
        // 将消息添加到消息仓库
        let _is_succeed = message_manager
            .add_message(send_channel_id, xid, from_addr, is_normal, message.clone())
            .await;

        let dxc_manager = airport.get_dxc_manager();

        let dxc = dxc_manager
            .get_dxc_by_name_and_version(&receiver_service_key.dxc_name, &dxc_version)
            .await;

        if let Some(dxc) = dxc {
            let is_succeed = dxc.message_in(message.as_ref()).await;
            if is_succeed {
                return;
            }
        }
        // 不是 resp 并且未重定向，则寻找兼容的 dxc
        if is_normal && !is_redirect {
            // 寻找兼容版本
            let dxc = dxc_manager
                .get_dxc_by_name_compatible_version(
                    &receiver_service_key.dxc_name,
                    &receiver_service_key.dxc_version,
                    receive_channel_id,
                )
                .await;
            //
            if let Some(dxc) = dxc {
                let is_succeed = dxc.message_in(message.as_ref()).await;
                if is_succeed {
                    return;
                }
            }
        }
        // dxc 不存在，处理消息
        if is_normal {
            message_manager.delete_normal_message(xid).await;
            let err_msg = format!("{:?} 实例不存在 v2！", receiver_service_key);
            ProtocolV2Handler::return_err_msg(send_channel_id, &message, err_msg).await;
        } else {
            message_manager.delete_resp_message(xid).await;
        }
    }

    async fn return_err_msg(send_channel_id: i64, message: &Vec<u8>, err_msg: String) {
        let airport = Application::get_airport();
        let v2_reader = ProtocolV2Reader::new(&message);
        let msg_type = v2_reader.msg_type();
        if msg_type.is_none() {
            info!("读取消息类型失败，忽略返回");
            return;
        }
        let msg_type = msg_type.unwrap();
        if msg_type == MsgType::RspNormal {
            info!("忽略 RspNormal 类型的消息响应");
            return;
        }
        let v1_msg = pack_status_message_from_v2(&v2_reader, Status::error(err_msg));
        airport
            .message_in(SELF_CHANNEL, send_channel_id, v1_msg)
            .await;
    }
}

#[async_trait]
impl ProtocolHandler for ProtocolV2Handler {
    fn send_msg(&self, _data: Vec<u8>) {
        //
    }
    async fn handle_msg(
        &self,
        airport: &Airport,
        send_channel_id: i64,
        receive_channel_id: i64,
        mut message: Vec<u8>,
    ) {
        // 1， 如果有指定接收通道，判断接收通道是否存在，并且设置了节点id
        if let Err(err_msg) = self
            .check_receive_is_exist(airport, receive_channel_id)
            .await
        {
            ProtocolV2Handler::return_err_msg(send_channel_id, &message, err_msg).await;
            return;
        }
        //
        let check_result: Result<i64, Status> = self
            .check_send_channel_have_conn_id(airport, send_channel_id)
            .await;

        if let Err(err) = check_result {
            ProtocolV2Handler::return_err_msg(send_channel_id, &message, err.err_msg).await;
            return;
        }
        ProtocolV2Writer::write_conn_id_to_message(&mut message, check_result.unwrap());
        let v2_reader = ProtocolV2Reader::new(&message);
        let msg_type = v2_reader.msg_type();
        if msg_type.is_none() {
            ProtocolV2Handler::return_err_msg(
                send_channel_id,
                &message,
                "获取消息类型失败！".into(),
            )
            .await;
            return;
        }

        //
        let msg_type = msg_type.unwrap();
        match msg_type {
            MsgType::RspNormal => {
                let msg_body_with_vrs = v2_reader.msg_body_with_vrs();
                let signer = Application::get_signer();
                let ret = signer.verify(msg_body_with_vrs);
                if let Err(err) = ret {
                    ProtocolV2Handler::return_err_msg(send_channel_id, &message, err.err_msg).await;
                    return;
                }
                let from_address = ret.unwrap();
                let receiver_service_key = v2_reader.receiver();

                self.handle_normal_message(
                    from_address,
                    receiver_service_key,
                    send_channel_id,
                    receive_channel_id,
                    false,
                    message,
                    false,
                )
                .await
            }
            MsgType::Normal | MsgType::System => {
                // 校验信息
                let msg_body_with_vrs = v2_reader.msg_body_with_vrs();
                // v2_reader.
                let signer = Application::get_signer();
                // 获取签名
                let ret = signer.verify(msg_body_with_vrs);
                if let Err(err) = ret {
                    ProtocolV2Handler::return_err_msg(send_channel_id, &message, err.err_msg).await;
                    return;
                }
                let from_address = ret.unwrap();
                // 要判断是否是本节点
                // 读取 api 出错
                let api = if msg_type == MsgType::Normal {
                    let api = v2_reader.api();
                    if let Err(err) = api {
                        ProtocolV2Handler::return_err_msg(send_channel_id, &message, err.err_msg)
                            .await;
                        return;
                    }
                    api.unwrap()
                } else {
                    String::default()
                };

                let mut verify_info =
                    self.make_judge_api_verify_info(send_channel_id, v2_reader.receiver(), api);
                // 写入地址
                verify_info.from_address = from_address.clone();
                // 发布验证信息
                let ret = event::publish_message_in(verify_info).await;
                match ret {
                    Err(err) => {
                        ProtocolV2Handler::return_err_msg(send_channel_id, &message, err.err_msg)
                            .await;
                    }
                    Ok((service_key, is_redirect)) => {
                        self.handle_normal_message(
                            from_address,
                            service_key,
                            send_channel_id,
                            receive_channel_id,
                            true,
                            message,
                            is_redirect,
                        )
                        .await
                    }
                }
            }
            _ => {
                info!("v2 消息协议，不处理类型：{:?} 的消息", msg_type);
                ProtocolV2Handler::return_err_msg(
                    send_channel_id,
                    &message,
                    format!("v2 消息协议，不处理类型：{:?} 的消息", msg_type),
                )
                .await;
            }
        }
    }

    fn get_xid(&self, data: &Vec<u8>) -> XID {
        let protocol_reader = ProtocolV2Reader::new(&data);
        protocol_reader.xid()
    }

    fn get_version(&self) -> u16 {
        2
    }
}
