use std::sync::Arc;

use crate::{
    airport::{airport::Airport, channel::SELF_CHANNEL},
    application::Application,
    event,
};

use super::{pack_status_message_from_v1, protocol_handler::ProtocolHandler, MsgType};

use async_trait::async_trait;
use semver::Version;
use tracing::info;
use x_common_lib::{
    base::status::Status,
    protocol::{
        protocol_v1::{ProtocolV1Reader, ProtocolV1Writer},
        XID,
    },
    service::sys_service_api::ServiceKey,
};
// =====
pub struct ProtocolV1Handler;

impl ProtocolV1Handler {
    pub fn new() -> Self {
        ProtocolV1Handler {}
    }
    async fn handle_normal_message(
        &self,
        receiver_service_key: Box<ServiceKey>,
        send_channel_id: i64,
        receive_channel_id: i64,
        is_normal: bool,
        message: Vec<u8>,
        is_redirect: bool,
    ) {
        //

        let protocol_reader = ProtocolV1Reader::new(&message);

        let airport = Application::get_airport();

        let message_manager = airport.get_message_manager();

        let xid = protocol_reader.xid();
        // 转换为内部协议
        let message = Arc::new(message);

        let _is_succeed = message_manager
            .add_message(
                send_channel_id,
                xid,
                String::default(),
                is_normal,
                message.clone(),
            )
            .await;
        // todo

        let dxc_manager = airport.get_dxc_manager();
        let service_version = Version::parse(&receiver_service_key.dxc_version).unwrap();
        let dxc = dxc_manager
            .get_dxc_by_name_and_version(&receiver_service_key.dxc_name, &service_version)
            .await;

        if let Some(dxc) = dxc {
            let _is_succeed = dxc.message_in(message.as_ref()).await;
            if _is_succeed {
                return;
            }
        }
        // 不是 resp 并且未重定向，则寻找兼容的 dxc
        if is_normal && !is_redirect {
            // 寻找兼容版本
            let dxc = dxc_manager
                .get_dxc_by_name_compatible_version(
                    &receiver_service_key.dxc_name,
                    &receiver_service_key.dxc_version,
                    receive_channel_id,
                )
                .await;
            //
            if let Some(dxc) = dxc {
                let is_succeed = dxc.message_in(message.as_ref()).await;
                if is_succeed {
                    return;
                }
            }
        }

        // dxc 不存在，处理消息
        if is_normal {
            message_manager.delete_normal_message(xid).await;
            let v1_reader = ProtocolV1Reader::new(&message);
            let status = Status::error(format!("{} 实例不存在 v1！", receiver_service_key.dxc_name));
            let v1_msg = pack_status_message_from_v1(&v1_reader, status);
            airport
                .message_in(SELF_CHANNEL, send_channel_id, v1_msg)
                .await;
        } else {
            message_manager.delete_resp_message(xid).await;
        }

        //
    }

    async fn return_err_msg(send_channel_id: i64, message: &Vec<u8>, err_msg: String) {
        let airport = Application::get_airport();
        let v1_reader = ProtocolV1Reader::new(&message);
        let msg_type = v1_reader.msg_type();
        if msg_type.is_none() {
            info!("读取消息类型失败，忽略返回");
            return;
        }
        let msg_type = msg_type.unwrap();
        if msg_type == MsgType::RspNormal {
            info!("忽略 RspNormal 类型的消息响应");
            return;
        }

        let v1_msg = pack_status_message_from_v1(&v1_reader, Status::error(err_msg));
        airport
            .message_in(SELF_CHANNEL, send_channel_id, v1_msg)
            .await;
    }
}

#[async_trait]
impl ProtocolHandler for ProtocolV1Handler {
    fn send_msg(&self, _data: Vec<u8>) {}

    async fn handle_msg(
        &self,
        airport: &Airport,
        send_channel_id: i64,
        receive_channel_id: i64,
        mut message: Vec<u8>,
    ) {
        let v1_reader: ProtocolV1Reader<'_> = ProtocolV1Reader::new(&message);
        // 1， 如果有指定接收通道，判断接收通道是否存在，并且设置了节点id
        if let Err(err_msg) = self
            .check_receive_is_exist(airport, receive_channel_id)
            .await
        {
            ProtocolV1Handler::return_err_msg(send_channel_id, &message, err_msg).await;
            return;
        }

        // 2，设置接收消息的，节点 id
        let check_result = self
            .check_send_channel_have_conn_id(airport, send_channel_id)
            .await;
        if let Err(err) = check_result {
            ProtocolV1Handler::return_err_msg(send_channel_id, &message, err.err_msg).await;
            return;
        }
        let msg_type = v1_reader.msg_type();
        if msg_type.is_none() {
            // 返回错误消息，给发送方
            ProtocolV1Handler::return_err_msg(
                send_channel_id,
                &message,
                "获取消息类型失败！".into(),
            )
            .await;
            return;
        }

        ProtocolV1Writer::write_conn_id_to_message(&mut message, check_result.unwrap());
        let v1_reader: ProtocolV1Reader<'_> = ProtocolV1Reader::new(&message);
        let msg_type = msg_type.unwrap();
        //
        match msg_type {
            MsgType::RspNormal => {
                let receiver_service_key = v1_reader.receiver();
                self.handle_normal_message(
                    receiver_service_key,
                    send_channel_id,
                    receive_channel_id,
                    false,
                    message,
                    false,
                )
                .await
            }
            MsgType::Normal | MsgType::System => {

                let api = if msg_type == MsgType::Normal {
                    let api = v1_reader.api();
                    if let Err(_) = api {
                        info!("读取api 出错，忽略消息...");
                        ProtocolV1Handler::return_err_msg(
                            send_channel_id,
                            &message,
                            "读取 api 出错！".into(),
                        )
                        .await;
                        return;
                    }
                    api.unwrap()
                } else {

                    String::default()
                };

                let verify_info =
                    self.make_judge_api_verify_info(send_channel_id, v1_reader.receiver(), api);
                let ret = event::publish_message_in(verify_info).await;
                match ret {
                    Err(err) => {
                        let v1_msg = pack_status_message_from_v1(&v1_reader, err);
                        airport
                            .message_in(SELF_CHANNEL, send_channel_id, v1_msg)
                            .await;
                    }
                    Ok((service_key, is_redirect)) => {
                        self.handle_normal_message(
                            service_key,
                            send_channel_id,
                            receive_channel_id,
                            true,
                            message,
                            is_redirect,
                        )
                        .await
                    }
                }
            }
            _ => {
                info!("v1 消息协议，不处理类型：{:?} 的消息", msg_type);
                ProtocolV1Handler::return_err_msg(
                    send_channel_id,
                    &message,
                    format!("v1 消息协议，不处理类型：{:?} 的消息", msg_type),
                )
                .await;
            }
        }
    }

    fn get_xid(&self, data: &Vec<u8>) -> XID {
        let protocol_reader = ProtocolV1Reader::new(&data);
        protocol_reader.xid()
    }

    fn get_version(&self) -> u16 {
        1
    }
}
