use async_trait::async_trait;
use tracing::info;
use x_common_lib::{
    base::status::Status,
    protocol::XID,
    service::sys_service_api::{verify_info::VerifyPhase, ServiceKey, VerifyInfo},
};

use crate::{
    airport::{
        airport::Airport,
        channel::{is_fixed_channel, SELF_CHANNEL},
    },
    event,
};

#[async_trait]
pub trait ProtocolHandler: Send + Sync {
    /**
     * 发送消息
     */
    fn send_msg(&self, data: Vec<u8>);
    /**
     * 处理消息
     */
    async fn handle_msg(
        &self,
        airport: &Airport,
        send_channel_id: i64,
        receive_channel_id: i64,
        data: Vec<u8>,
    );

    /**
     *  获取id
     */
    fn get_xid(&self, data: &Vec<u8>) -> XID;

    fn get_version(&self) -> u16;

    fn make_judge_conn_id_verify_info(&self, send_channel_id: i64) -> VerifyInfo {
        let mut verify_info = VerifyInfo::default();
        verify_info.phase = Some(VerifyPhase::JudgeConnId);
        verify_info.channel_id = send_channel_id;
        return verify_info;
    }

    fn make_judge_api_verify_info(
        &self,
        send_channel_id: i64,
        receiver_key: Box<ServiceKey>,
        api: String,
    ) -> VerifyInfo {
        let mut verify_info = VerifyInfo::default();
        verify_info.phase = Some(VerifyPhase::JudgeConnId);
        verify_info.channel_id = send_channel_id;
        verify_info.dxc_name = receiver_key.dxc_name;
        verify_info.dxc_version = receiver_key.dxc_version;

        verify_info.dxc_md5 = receiver_key.dxc_md5;
        verify_info.phase = Some(VerifyPhase::JudgeApi);
        verify_info.api = api;

        return verify_info;
    }

    async fn check_receive_is_exist(
        &self,
        airport: &Airport,
        receive_channel_id: i64,
    ) -> Result<(), String> {
        if is_fixed_channel(receive_channel_id) {
            //     //
            let channel_manager = airport.get_channel_manager();
            let channel = channel_manager.get_channel(receive_channel_id).await;
            //
            if channel.is_some() {
                Ok(())
            } else {
                Err(format!("channel id {} 不存在", receive_channel_id))
            }
        } else {
            Ok(())
        }
    }

    async fn check_send_channel_have_conn_id(
        &self,
        airport: &Airport,
        send_channel_id: i64,
    ) -> Result<i64, Status> {
        if send_channel_id != SELF_CHANNEL {
            let send_channel = airport
                .get_channel_manager()
                .get_channel(send_channel_id)
                .await;
            // 检查发送通道，是否设置节点id
            //
            if send_channel.is_none() {
                info!("找不到发送通道 {} 忽略消息...", send_channel_id);
                return Err(Status::error(format!(
                    "找不到发送通道 {} 忽略消息...",
                    send_channel_id
                )));
            }
            let send_channel = send_channel.unwrap();
            let send_conn_id = send_channel.conn_id();

            //
            if send_conn_id == 0 {
                //
                let verify_info = self.make_judge_conn_id_verify_info(send_channel_id);
                //
                let ret = event::publish_message_in(verify_info).await;
                //
                if let Err(err) = ret {
                    info!(
                        "channel {} 未设置 conn id 忽略其发送的消息:{:?}",
                        send_channel_id, err
                    );
                    return Err(Status::error(format!(
                        "channel {} 未设置 conn id 忽略其发送的消息:{:?}",
                        send_channel_id, err
                    )));
                }
            }
            Ok(send_conn_id)
        } else {
            Ok(SELF_CHANNEL)
        }
    }
}
