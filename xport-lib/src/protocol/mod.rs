use x_common_lib::{
    base::status::Status,
    protocol::{
        protocol_v1::{ProtocolV1Reader, ProtocolV1Writer},
        protocol_v2::{ProtocolV2Reader, ProtocolV2Writer},
        MsgType,
    },
    serial::request_message::RequestMessage,
    service::sys_service_api::ServiceKey,
};

use crate::application::Application;

pub mod protocol_handler;
pub mod protocol_hearbeat;
pub mod protocol_v1;
pub mod protocol_v2;
pub mod protocol_v3;

pub fn pack_status_message_from_v2(v2_reader: &ProtocolV2Reader<'_>, status: Status) -> Vec<u8> {
    let request_id = v2_reader.req_id();
    let receiver_service_key = v2_reader.sender();
    pack_status_message(receiver_service_key, request_id, status)
}

pub fn pack_status_message_from_v1(v1_reader: &ProtocolV1Reader<'_>, status: Status) -> Vec<u8> {
    let request_id = v1_reader.req_id();
    let receiver_service_key = v1_reader.sender();
    pack_status_message(receiver_service_key, request_id, status)
}

fn pack_status_message(
    receiver_service_key: Box<ServiceKey>,
    request_id: i64,
    status: Status,
) -> Vec<u8> {
    let signer = Application::get_signer();
    let default_sender_service_key = ServiceKey::default();
    let status_size = status.compute_size_with_tag_and_len();
    if signer.has_private_key() {
        let mut msg_body = Vec::with_capacity(status_size as usize + 4 + 64);
        {
            let mut os: protobuf::CodedOutputStream<'_> =
                protobuf::CodedOutputStream::vec(&mut msg_body);
            status.serial_with_tag_and_len(&mut os);
            os.flush().unwrap();
            drop(os);
        }
        let sign_message_buffer = signer.sign(msg_body.as_slice()).unwrap();
        let mut writer = ProtocolV2Writer::new(
            sign_message_buffer.len() as u32,
            request_id,
            &default_sender_service_key,
            &receiver_service_key,
        );
        writer.write_msg_type(MsgType::RspNormal);
        writer.write_conn_id(0);
        writer.write_msg_body(&sign_message_buffer);
        writer.msg_buffer
    } else {
        let mut writer = ProtocolV1Writer::new(
            status_size as u32,
            request_id,
            &default_sender_service_key,
            &receiver_service_key,
        );
        writer.write_msg_type(MsgType::RspNormal);
        writer.write_conn_id(0);
        let msg_body_buffer = writer.msg_body_buffer();
        let mut os = protobuf::CodedOutputStream::bytes(msg_body_buffer);
        status.serial_with_tag_and_len(&mut os);
        drop(os);
        writer.msg_buffer
    }
}

pub fn pack_message_in_local_service(
    request_id: i64,
    sender_service_key: &ServiceKey,
    receiver_service_key: &ServiceKey,
    msg_type: MsgType,
    message: &Vec<u8>,
) -> Vec<u8> {
    let signer = Application::get_signer();
    if signer.has_private_key() {
        let sign_message_buffer = signer.sign(message.as_slice()).unwrap();
        let mut v2_writer = ProtocolV2Writer::new(
            sign_message_buffer.len() as u32,
            request_id,
            sender_service_key,
            receiver_service_key,
        );
        // 
        v2_writer.write_conn_id(0);
        v2_writer.write_msg_type(msg_type);
        v2_writer.write_msg_body(&sign_message_buffer);
        v2_writer.msg_buffer
    } else {
        let mut v1_writer = ProtocolV1Writer::new(
            message.len() as u32,
            request_id,
            sender_service_key,
            receiver_service_key,
        );
        //
        v1_writer.write_conn_id(0);
        v1_writer.write_msg_type(msg_type);
        v1_writer.write_msg_body(&message);
        v1_writer.msg_buffer
    }
}

pub fn pack_message_in_ipc_service(
    request_id: i64,
    sender_service_key: &ServiceKey,
    receiver_service_key: &ServiceKey,
    msg_type: MsgType,
    msg_content: &[u8],
) -> Vec<u8> {
    let signer = Application::get_signer();
    if signer.has_private_key() {
        let sign_message_buffer = signer.sign(msg_content).unwrap();
        let mut v2_writer = ProtocolV2Writer::new(
            sign_message_buffer.len() as u32,
            request_id,
            sender_service_key,
            receiver_service_key,
        );
        //
        v2_writer.write_conn_id(0);
        v2_writer.write_msg_type(msg_type);
        // 跳过头部

        v2_writer.write_msg_body_by_arr(sign_message_buffer.as_slice());
        v2_writer.msg_buffer
    } else {
        let mut v1_writer = ProtocolV1Writer::new(
            msg_content.len() as u32,
            request_id,
            sender_service_key,
            receiver_service_key,
        );
        // 跳过头部
        let msg_content =
            unsafe { std::slice::from_raw_parts(msg_content.as_ptr(), msg_content.len()) };
        v1_writer.write_conn_id(0);
        v1_writer.write_msg_type(msg_type);
        v1_writer.write_msg_body_by_arr(msg_content);

        v1_writer.msg_buffer
    }
}
