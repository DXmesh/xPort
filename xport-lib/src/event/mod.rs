use protobuf::CodedInputStream;
use std::{
    collections::HashMap,
    sync::{Arc, Mutex},
};
use tokio::sync::oneshot::{self};
use x_common_lib::{
    base::{
        dll_api::{
            event::{
                CHANNEL_CONNECTED, CHANNEL_DISCONNECTED, IPC_SERVICE_DISCONNECT, LOCAL_SERVICE_OFF, LOCAL_SERVICE_ON, LOOK_FOR_DXC, MESSAGE_IN
            },
            SubscribeHandler,
        },
        id_generator::gen_id,
        status::Status,
    },
    serial::request_message::RequestMessage,
    service::sys_service_api::{ChannelEvent, ServiceInfo, ServiceKey, VerifyInfo},
};

use crate::application::Application;

pub(crate) struct Subscriber {
    id: i64,
    pri: u32, // 订阅优先级
    handler: SubscribeHandler,
}

impl Subscriber {
    pub(crate) fn new(pri: u32, handler: SubscribeHandler) -> Self {
        Subscriber {
            id: gen_id(),
            pri: pri,
            handler: handler,
        }
    }
}

pub struct Event {
    //
    channel_connected_subscribers: tokio::sync::RwLock<Vec<Arc<Subscriber>>>,
    channel_disconnected_subscribers: tokio::sync::RwLock<Vec<Arc<Subscriber>>>,
    local_service_on_subscribers: tokio::sync::RwLock<Vec<Arc<Subscriber>>>,
    local_service_off_subscribers: tokio::sync::RwLock<Vec<Arc<Subscriber>>>,
    //
    ipc_disconnect_subscribers: tokio::sync::RwLock<Vec<Arc<Subscriber>>>,
    //
    message_in_subscribers: tokio::sync::RwLock<Vec<Arc<Subscriber>>>,

    look_for_dxc_subscribers: tokio::sync::RwLock<Vec<Arc<Subscriber>>>,
    //
    resp_handler_map: Mutex<HashMap<i64, Box<dyn FnOnce(*const u8, u32) + Send + Sync>>>, //
}

impl Event {
    pub fn new() -> Self {
        Event {
            channel_connected_subscribers: tokio::sync::RwLock::new(Vec::new()),
            channel_disconnected_subscribers: tokio::sync::RwLock::new(Vec::new()),
            local_service_on_subscribers: tokio::sync::RwLock::new(Vec::new()),
            local_service_off_subscribers: tokio::sync::RwLock::new(Vec::new()),
            //
            ipc_disconnect_subscribers: tokio::sync::RwLock::new(Vec::new()),

            message_in_subscribers: tokio::sync::RwLock::new(Vec::new()),
            look_for_dxc_subscribers: tokio::sync::RwLock::new(Vec::new()),
            resp_handler_map: Mutex::new(HashMap::new()),
        }
    }

    pub fn add_response_handler(
        &self,
        publish_id: i64,
        handler: Box<dyn FnOnce(*const u8, u32) + Send + Sync>,
    ) {
        let mut handler_map = self.resp_handler_map.lock().unwrap();
        handler_map.insert(publish_id, handler);
    }

    pub fn on_resp(&self, publish_id: i64, msg: *const u8, len: u32) {
        //
        let handler = {
            let mut handler_map = self.resp_handler_map.lock().unwrap();
            handler_map.remove(&publish_id)
        };

        if handler.is_none() {
            // warn!("找不到请求id {} 的 handler", publish_id);
            return;
        }
        //

        let handler = handler.unwrap();

        handler(msg, len);
    }
}
pub async fn add_subscriber(event_id: u16, pri: u32, handler: SubscribeHandler) -> Option<i64> {
    let event = Application::get_event();
    let subscribers = match event_id {
        CHANNEL_CONNECTED => &event.channel_connected_subscribers,
        CHANNEL_DISCONNECTED => &event.channel_disconnected_subscribers,
        LOCAL_SERVICE_ON => &event.local_service_on_subscribers,
        LOCAL_SERVICE_OFF => &event.local_service_off_subscribers,
        MESSAGE_IN => &event.message_in_subscribers,
        LOOK_FOR_DXC => &event.look_for_dxc_subscribers,
        IPC_SERVICE_DISCONNECT=> &event.ipc_disconnect_subscribers,
        _ => return None,
    };

    let mut subscribers = subscribers.write().await;
    let subscriber = Subscriber::new(pri, handler);
    let subscriber_id = subscriber.id;
    let pos = subscribers
        .iter()
        .position(|s| s.pri < pri)
        .unwrap_or(subscribers.len());
    subscribers.insert(pos, Arc::new(subscriber));
    Some(subscriber_id)
}

pub async fn remove_subscriber(event_id: u16, subscriber_id: i64) {
    let event = Application::get_event();
    let target_list = match event_id {
        CHANNEL_CONNECTED => &event.channel_connected_subscribers,
        CHANNEL_DISCONNECTED => &event.channel_disconnected_subscribers,
        LOCAL_SERVICE_ON => &event.local_service_on_subscribers,
        LOCAL_SERVICE_OFF => &event.local_service_off_subscribers,
        MESSAGE_IN => &event.message_in_subscribers,
        LOOK_FOR_DXC => &event.look_for_dxc_subscribers,
        IPC_SERVICE_DISCONNECT=> &event.ipc_disconnect_subscribers,
        _ => return,
    };

    let mut subscribers = target_list.write().await;
    if let Some(pos) = subscribers.iter().position(|s| s.id == subscriber_id) {
        subscribers.remove(pos);
    }
}

pub async fn publish_channel_connected(conn_id: i64, channel_id: i64, is_active: bool) {
    let publish_id = gen_id();
    let event = Application::get_event();
    let mut channel_event = ChannelEvent::default();
    channel_event.channel_id = channel_id;
    channel_event.conn_id = conn_id;
    channel_event.is_active = is_active;
    //

    // get_all_conn_id(service_id, request_id)

    let buffer = channel_event.serial().unwrap();
    let subscribers = event.channel_connected_subscribers.read().await;
    for subscriber in subscribers.iter() {
        //     //
        // let (sender, receiver) = oneshot::channel::<()>();
        // event.add_response_handler(
        //     publish_id,
        //     Box::new(move |_buffer, _buffer_len| {
        //         let _ = sender.send(());
        //     }),
        // );
        // //

        (subscriber.handler)(
            CHANNEL_CONNECTED,
            publish_id,
            buffer.as_ptr(),
            buffer.len() as u32,
        );
        // let _ = receiver.await;
    }
}

pub async fn publish_channel_disconnected(conn_id: i64, channel_id: i64, is_active: bool) {
    let publish_id = gen_id();
    let event = Application::get_event();
    let mut channel_event = ChannelEvent::default();
    channel_event.channel_id = channel_id;
    channel_event.conn_id = conn_id;
    channel_event.is_active = is_active;
    let buffer = channel_event.serial().unwrap();
    let subscribers = event.channel_disconnected_subscribers.read().await;
    for subscriber in subscribers.iter() {
        // let (sender, receiver) = oneshot::channel::<()>();
        // event.add_response_handler(
        //     publish_id,
        //     Box::new(|_buffer, _buffer_len| {
        //         //
        //         let _ = sender.send(());
        //     }),
        // );
        (subscriber.handler)(
            CHANNEL_DISCONNECTED,
            publish_id,
            buffer.as_ptr(),
            buffer.len() as u32,
        );
        // let _ = receiver.await;
    }
}

pub async fn publish_local_service_on(service_info: Box<ServiceInfo>) {
    let publish_id = gen_id();
    let event = Application::get_event();
    let buffer = service_info.serial().unwrap();
    let subscribers = event.local_service_on_subscribers.read().await;
    for subscriber in subscribers.iter() {
        // 分发消息
        // let (sender, receiver) = oneshot::channel::<()>();
        // event.add_response_handler(
        //     publish_id,
        //     Box::new(|_buffer, _buffer_len| {
        //         //
        //         let _ = sender.send(());
        //     }),
        // );
        (subscriber.handler)(
            LOCAL_SERVICE_ON,
            publish_id,
            buffer.as_ptr(),
            buffer.len() as u32,
        );
        // let _ = receiver.await;
    }
}

pub async fn publish_local_service_off(service_info: Box<ServiceInfo>) {
    let publish_id = gen_id();
    let event = Application::get_event();
    let buffer = service_info.serial().unwrap();
    let subscribers = event.local_service_off_subscribers.read().await;
    for subscriber in subscribers.iter() {
        // let (sender, receiver) = oneshot::channel::<()>();
        // event.add_response_handler(
        //     publish_id,
        //     Box::new(|_buffer, _buffer_len| {
        //         //
        //         let _ = sender.send(());
        //     }),
        // );
        (subscriber.handler)(
            LOCAL_SERVICE_OFF,
            publish_id,
            buffer.as_ptr(),
            buffer.len() as u32,
        );
        // let _ = receiver.await;
    }
}

pub async fn publish_ipc_service_disconnect(service_info: Box<ServiceInfo>) {
  let publish_id = gen_id();
  let event = Application::get_event();
  let buffer = service_info.serial().unwrap();
  let subscribers = event.ipc_disconnect_subscribers.read().await;
  for subscriber in subscribers.iter() {
      (subscriber.handler)(
        IPC_SERVICE_DISCONNECT,
          publish_id,
          buffer.as_ptr(),
          buffer.len() as u32,
      );

  }
}

pub async fn publish_look_for_dxc(service_info: &ServiceKey) {
    let publish_id = gen_id();
    let event = Application::get_event();
    let buffer = service_info.serial().unwrap();
    let subscribers = event.look_for_dxc_subscribers.read().await;
    for subscriber in subscribers.iter() {

        // let (sender, receiver) = oneshot::channel::<()>();
        // event.add_response_handler(
        //     publish_id,
        //     Box::new(|_buffer, _buffer_len| {
        //         //
        //         let _ = sender.send(());
        //     }),
        // );
        (subscriber.handler)(
            LOOK_FOR_DXC,
            publish_id,
            buffer.as_ptr(),
            buffer.len() as u32,
        );
        // let _ = receiver.await;
    }
}

pub async fn publish_message_in(verify_info: VerifyInfo) -> Result<(Box<ServiceKey>, bool), Status> {
    let publish_id = gen_id();
    let event = Application::get_event();
    let buffer = verify_info.serial().unwrap();
    let subscribers = event.message_in_subscribers.read().await;
    let mut receiver_service_key = Box::new(ServiceKey::default());
    if subscribers.is_empty() {
        receiver_service_key.dxc_name = verify_info.dxc_name;
        receiver_service_key.dxc_version = verify_info.dxc_version;
        receiver_service_key.dxc_md5 = verify_info.dxc_md5;
        return Ok((receiver_service_key, true));
    }
    let mut is_first_subscriber = true;
    //
    // 只取优先级高的，也就是第一个 订阅者的结果
    // 但是有任一一个订阅者返回的拒绝的消息，都会返回 err
    for subscriber in subscribers.iter() {
        // 这里有订阅者
        let (sender, receiver) = oneshot::channel::<(Status, Option<Box<ServiceKey>>)>();
        event.add_response_handler(
            publish_id,
            Box::new(|buffer, len| {
                if len == 0 {
                    let _ = sender.send((Status::default(), None));
                    return;
                }
                //
                let slice = unsafe { std::slice::from_raw_parts(buffer, len as usize) };
                let mut is = CodedInputStream::from_bytes(slice);
                let mut status = Status::default();
                let _ = status.parse_from_input_stream_with_tag_and_len(&mut is);
                if status.is_erorr() {
                    let _ = sender.send((status, None));
                    return;
                }
                let mut service_key = Box::new(ServiceKey::default());
                let _ = service_key.parse_from_input_stream_with_tag_and_len(&mut is);
                let _ = sender.send((status, Some(service_key)));
            }),
        );
        (subscriber.handler)(MESSAGE_IN, publish_id, buffer.as_ptr(), buffer.len() as u32);
        // 插入进来了
        let result = receiver.await;
        match result {
            Err(err) => return Err(Status::error(err.to_string())),
            Ok((status, service_key)) => {
                if status.is_erorr() {
                    return Err(status);
                }
                //
                if is_first_subscriber {
                    //
                    let service_key = service_key.unwrap();
                    receiver_service_key = service_key;
                    is_first_subscriber = false;
                }
            }
        };
    }
    //
    if receiver_service_key.is_empty() {
        receiver_service_key.dxc_name = verify_info.dxc_name;
        receiver_service_key.dxc_version = verify_info.dxc_version;
        receiver_service_key.dxc_md5 = verify_info.dxc_md5;
        Ok((receiver_service_key, false))
    } else {
        Ok((receiver_service_key, true))
    }
}
