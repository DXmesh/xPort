use async_trait::async_trait;



// 未指定 channel id
pub static UNSET_CHANNEL: i64 = 0;
// 本节点
pub static SELF_CHANNEL: i64 = 1;

// 是否指定通道
pub fn is_fixed_channel(channel_id:i64)-> bool{
    return channel_id != UNSET_CHANNEL && channel_id != SELF_CHANNEL;
}

#[async_trait]
pub trait Channel: Send + Sync {
    fn init(&self);
    fn start(&self);
    fn finialize(&self);

    async fn on_close(&self);
    // 返回发送的消息id
    /**
     * 发送消息, 具体的实现由底层
     */
    async fn send_message(&self, message: &Vec<u8>) -> bool;

    /**
     *  关闭通道
     */
    async fn close_channel(&self);
    /**
     * 通道有消息过来
     */
    async fn message_in(&self, message: Vec<u8>);

    /**
     * 是否登录
     */
    fn is_login(&self) -> bool;
    /**
     * 设置通道的端口号
     */
    async fn set_channel_port(&self, port: u16);
    /**
     *
     */

    fn is_active(&self) -> bool;

    //=====================================

    fn id(&self) -> i64;

    fn register_time(&self) -> i64;

    fn conn_id(&self) -> i64;
    /*
    	* 是否已经设置端口
    	*/
    fn had_set_port(&self) -> bool;

    // fn add_normal_msg_and_wait_ack(&self, msg_id: i64);
    // /**
    //  *
    //  */
    // fn get_all_wait_for_send_msg_and_clear(& self) ->Vec<i64>;
}
