use std::{
    collections::HashMap,
    sync::{Arc, Mutex},
};
use tokio::sync::RwLock;
use x_common_lib::protocol::XID;

struct MessageHolder {
    // 消息从那条通道接收的
    channel_id: i64,
    // 来源地址
    from_addr: String,
    // 消息ID, CurReqId
    // pub msg_id: i64,
    // 消息
    data: Arc<Vec<u8>>,
}

struct MessageHolderWrapper {
    // key request_id
    normal_message_map: HashMap<i64, Box<MessageHolder>>,
    // key request_id
    resp_message_map: HashMap<i64, Box<MessageHolder>>,
}
//
struct StreamMessageHolder {
    // 消息从那条通道接收的
    channel_id: i64,
    // 来源地址
    from_addr: String,

    // 当前使用次数
    ref_count: u64,

    data: Arc<Vec<u8>>,
}

pub struct MessageManager {
    // key  conn_id
    message_wrapper_map: RwLock<HashMap<i64, MessageHolderWrapper>>,

    // 消息流
    stream_message_map: Mutex<HashMap<i64, HashMap<i64, StreamMessageHolder>>>,
}

impl MessageManager {
    pub fn new() -> MessageManager {
        MessageManager {
            message_wrapper_map: RwLock::new(HashMap::new()),
            //
            stream_message_map: Mutex::new(HashMap::new()),
        }
    }

    /**
     *
     */
    pub async fn add_message(
        &self,
        channel_id: i64,
        xid: XID,
        from_addr: String,
        is_normal: bool,
        data: Arc<Vec<u8>>,
    ) {
        //
        let mut map = self.message_wrapper_map.write().await;
        //
        let msg_wrapper_map = map.entry(xid.conn_id).or_insert(MessageHolderWrapper {
            normal_message_map: HashMap::new(),
            resp_message_map: HashMap::new(),
        });

        let msg_holder = Box::new(MessageHolder {
            channel_id,
            from_addr,
            data,
        });

        if is_normal {
            msg_wrapper_map
                .normal_message_map
                .insert(xid.request_id, msg_holder);
        } else {
            msg_wrapper_map
                .resp_message_map
                .insert(xid.request_id, msg_holder);
        }
    }

    pub fn add_stream_message(
        &self,
        channel_id: i64,
        xid: XID,
        from_addr: String,
        data: Arc<Vec<u8>>,
    ) {
        let mut stream_message_map = self.stream_message_map.lock().unwrap();
        //
        let stream_message_map = stream_message_map
            .entry(xid.conn_id)
            .or_insert(HashMap::new());

        stream_message_map.insert(
            xid.request_id,
            StreamMessageHolder {
                channel_id,
                from_addr,
                ref_count: 0,
                data,
            },
        );
    }

    /**
     *
     */
    pub async fn delete_normal_message(&self, xid: XID) {
        self.delete_message(xid, true).await
    }
    /**
     *
     */
    pub async fn delete_resp_message(&self, xid: XID) {
        self.delete_message(xid, false).await
    }

    async fn delete_message(&self, xid: XID, is_normal: bool) {
        let mut map = self.message_wrapper_map.write().await;
        let msg_wrapper = map.get_mut(&xid.conn_id);

        if msg_wrapper.is_none() {
            return;
        }

        let msg_wrapper = msg_wrapper.unwrap();

        if is_normal {
            msg_wrapper.normal_message_map.remove(&xid.request_id);
            // 当前节点没有消息要处理了，就删除对应的存储位
        } else {
            msg_wrapper.resp_message_map.remove(&xid.request_id);
        }

        if msg_wrapper.normal_message_map.len() == 0 && msg_wrapper.resp_message_map.len() == 0 {
            map.remove(&xid.conn_id);
        }
    }

    pub async fn get_channel_id_from_normal_msg(&self, xid: &XID) -> Option<i64> {
        let map = self.message_wrapper_map.read().await;

        let msg_wrapper = map.get(&xid.conn_id);

        if msg_wrapper.is_none() {
            return None;
        }

        let msg_wrapper = msg_wrapper.unwrap();

        let msg_holder = msg_wrapper.normal_message_map.get(&xid.request_id);
        if msg_holder.is_none() {
            return None;
        }
        let msg_holder = msg_holder.unwrap();
        //
        Some(msg_holder.channel_id)
    }

    /**
     * 返回消息，和 channel_id
     */
    pub async fn get_normal_msg(&self, xid: &XID) -> Option<(Arc<Vec<u8>>, i64, String)> {
        self.get_msg(xid, true).await
    }
    /**
     * 返回消息，和 channel_id
     */
    pub async fn get_resp_msg(&self, xid: &XID) -> Option<(Arc<Vec<u8>>, i64, String)> {
        self.get_msg(xid, false).await
    }
    /**
     * 返回消息，和 channel_id
     */
    async fn get_msg(&self, xid: &XID, is_normal: bool) -> Option<(Arc<Vec<u8>>, i64, String)> {
        
        let map = self.message_wrapper_map.read().await;

        let msg_wrapper = map.get(&xid.conn_id);
        if msg_wrapper.is_none() {
            return None;
        }
        let msg_wrapper = msg_wrapper.unwrap();

        let msg_holder = if is_normal {
            msg_wrapper.normal_message_map.get(&xid.request_id)
        } else {
            msg_wrapper.resp_message_map.get(&xid.request_id)
        };
        if msg_holder.is_none() {
            return None;
        }
        let msg_holder = msg_holder.unwrap();

        //
        Some((
            msg_holder.data.clone(),
            msg_holder.channel_id,
            msg_holder.from_addr.clone(),
        ))
    }
    /**
     *
     */
    pub fn ref_stream_message(&self, xid: XID) {
        let mut stream_message_map = self.stream_message_map.lock().unwrap();

        let stream_conn_id_message_map = stream_message_map.get_mut(&xid.conn_id);

        if stream_conn_id_message_map.is_none() {
            return;
        }
        let stream_conn_id_message_map = stream_conn_id_message_map.unwrap();

        let stream_message_holder = stream_conn_id_message_map.get_mut(&xid.request_id);

        if let Some(stream_message_holder) = stream_message_holder {
            stream_message_holder.ref_count += 1;
        }
    }
    /**
     * 解引用
     */
    pub fn deref_stream_message(&self, xid: &XID) {
        let mut stream_message_map = self.stream_message_map.lock().unwrap();

        let stream_conn_id_message_map = stream_message_map.get_mut(&xid.conn_id);

        if stream_conn_id_message_map.is_none() {
            return;
        }
        let stream_conn_id_message_map = stream_conn_id_message_map.unwrap();

        let stream_message_holder = stream_conn_id_message_map.get_mut(&xid.request_id);

        if let Some(stream_message_holder) = stream_message_holder {
            stream_message_holder.ref_count -= 1;

            if stream_message_holder.ref_count <= 0 {
                // 删除消息
                stream_conn_id_message_map.remove(&xid.request_id);
            }
            //
            if stream_conn_id_message_map.is_empty() {
                stream_message_map.remove(&xid.conn_id);
            }
        }
    }

    pub fn get_stream_message(&self, xid: &XID) -> Option<(Arc<Vec<u8>>, i64, String)> {
        let mut stream_message_map = self.stream_message_map.lock().unwrap();

        let stream_conn_id_message_map = stream_message_map.get_mut(&xid.conn_id);

        if stream_conn_id_message_map.is_none() {
            return None;
        }

        let stream_conn_id_message_map = stream_conn_id_message_map.unwrap();

        let stream_message_holder = stream_conn_id_message_map.get_mut(&xid.request_id);

        if let Some(stream_message_holder) = stream_message_holder {
            Some((
                stream_message_holder.data.clone(),
                stream_message_holder.channel_id,
                stream_message_holder.from_addr.clone(),
            ))
        } else {
            None
        }
    }
}
