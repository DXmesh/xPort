use semver::Version;

#[cfg(windows)]
use std::os::windows::process::CommandExt;

use std::{
    collections::HashMap,
    ffi::OsString,
    fs::File,
    path::Path,
    process::{Command, Stdio},
    sync::Arc,
};
use tokio::{sync::RwLock, task};
use tracing::info;
use x_common_lib::{
    base::{config::Config, dxc::Manifest, id_generator, status::Status},
    service::sys_service_api::ServiceInfos,
    utils::{env_utils, file_utils, string_utils},
};

use crate::{
    application::{get_runtime, Application},
    event::{publish_local_service_off, publish_local_service_on},
    log,
    service::{
        ipc_service::ipc_service::IPCService, loader::DllService, local_service::LocalService,
        outer_service::OuterService, remote_service::RemoteService, service::Service,
    },
};

use super::channel::{SELF_CHANNEL, UNSET_CHANNEL};

struct RemoteServiceHolder {
    // conn_id 对应的 remote_id
    service_conn_id_map: HashMap<i64, i64>,
    service_id_map: HashMap<i64, Arc<Box<RemoteService>>>,
}

struct PathSetter {
    original_paths: OsString,
}

impl PathSetter {
    pub fn set(dxc_path: &str) -> Self {
        let original_paths = std::env::var_os("PATH").unwrap_or_default();
        let abs_dxc_path = file_utils::get_absoule_path(dxc_path).expect("获取 dxc 绝对路径失败！");
        let mut new_paths = std::env::split_paths(&original_paths).collect::<Vec<_>>();
        new_paths.push(abs_dxc_path.into());
        let dll_path = std::env::join_paths(new_paths.iter()).unwrap();
        std::env::set_var("PATH", &dll_path);
        PathSetter { original_paths }
    }
}
impl Drop for PathSetter {
    fn drop(&mut self) {
        std::env::set_var("PATH", &self.original_paths)
    }
}

pub struct DXC {
    /**
     *
     */
    dxc_id: i64,
    /**
     *
     */
    dxc_name: String,
    /**
     *
     */
    local_service_holder: RwLock<Option<Arc<Box<LocalService>>>>,
    /**
     *
     */
    ipc_service_holder: RwLock<Option<Arc<Box<IPCService>>>>,
    /**
     *
     */
    outer_service_holder: RwLock<Option<Arc<Box<OuterService>>>>,

    /**
     *
     */
    remote_service_holder: RwLock<RemoteServiceHolder>,
    /**
     * 版本
     */
    version: Version,
}

impl DXC {
    pub fn new(dxc_name: String, version: Version) -> Self {
        DXC {
            dxc_name: dxc_name,
            local_service_holder: RwLock::new(None),
            ipc_service_holder: RwLock::new(None),
            outer_service_holder: RwLock::new(None),
            remote_service_holder: RwLock::new(RemoteServiceHolder {
                service_conn_id_map: HashMap::new(),
                service_id_map: HashMap::new(),
            }),
            version: version,
            dxc_id: id_generator::gen_dxc_id(),
        }
    }

    pub fn name(&self) -> String {
        self.dxc_name.clone()
    }

    /**
     *
     */
    pub fn verison(&self) -> Version {
        self.version.clone()
    }
    /**
     *
     */
    pub fn id(&self) -> i64 {
        self.dxc_id
    }

    pub async fn load_ipc_service(
        &self,
        dxc_name: String,
        dxc_version: Version,
        dxc_md5: String,
        language: String,
        only_build_ipc: bool,
        config: &str,
        compatible_versions: Vec<String>,
        only_in_node: bool,
        timeout: Option<i32>,
    ) -> Result<(), Status> {
        //
        let ipc_service = IPCService::new(
            dxc_name,
            dxc_version,
            self.dxc_id,
            dxc_md5,
            config,
            compatible_versions,
            only_in_node,
            only_in_node,
            timeout
        );

        let ipc_service = Arc::new(ipc_service);
        {
            let mut ipc_service_hoder = self.ipc_service_holder.write().await;
            *ipc_service_hoder = Some(ipc_service.clone());
        }

        // 启动 ipc 监服务
        ipc_service.start();
        //

        let airport = Application::get_airport();

        // 如果时调试模式，不启动 python 和 java 进程
        if !only_build_ipc {
            if language == "python" {
                let dxc_manager = airport.get_dxc_manager();
                // 用于生成 id 的分段id
                let segment_id = dxc_manager.allocate_sgement_id();
                let version = ipc_service.version().to_string();
                let dxc_name = ipc_service.dxc_name();

                let mut command = Command::new("python");

                command
                    .current_dir(format!("dxc/{}-{}-none-none", dxc_name, version))
                    .arg("src/lib.py")
                    .arg(format!("--dxc-name={}", ipc_service.dxc_name()))
                    .arg(format!("--dxc-version={}", version))
                    .arg(format!("--log-level={}", log::level_name()))
                    .arg(format!("--segment-id={}", segment_id));

                #[cfg(windows)]
                command.creation_flags(0x08000000);

                let process = command
                    .stderr(Stdio::null()) // 可选：隐藏标准错误
                    .stdout(Stdio::null()) // 可选：隐藏标准输出
                    .spawn()
                    .expect("启动python失败");

                ipc_service.set_process(process);
            } else if language == "java" {
                let dxc_manager = airport.get_dxc_manager();
                let segment_id = dxc_manager.allocate_sgement_id();
                let version = ipc_service.version().to_string();
                let dxc_name = ipc_service.dxc_name();
                let line_version = version.replace(".", "_");
                let jar_name = dxc_name.replace("Service", "");
                let jar_name = string_utils::convert_to_snake_case(&jar_name);
                let mut command = Command::new("java");

                command
                    .current_dir(format!("dxc/{}_{}", dxc_name, line_version))
                    .arg("-jar")
                    .arg(format!("{}-{}.jar", jar_name, version))
                    .arg(format!("--dxc-name={}", ipc_service.dxc_name()))
                    .arg(format!("--dxc-version={}", version))
                    .arg(format!("--log-level={}", log::level_name()))
                    .arg(format!("--segment-id={}", segment_id));

                #[cfg(windows)]
                command.creation_flags(0x08000000);

                let process = command
                    .stderr(Stdio::null()) // 可选：隐藏标准错误
                    .stdout(Stdio::null()) // 可选：隐藏标准输出
                    .spawn()
                    .expect("启动Java失败");
                ipc_service.set_process(process);
            } else {
                panic!("当前 xPort 版本，目前不支持运行 {} 编写的dxc", language);
            }
        }
        // 等待 ipc 服务初始化的回调
        let ret = ipc_service.wait_inited().await;
        if ret.is_erorr() {
            return Err(ret);
        }
        //
        let service_info = ipc_service.get_service_info();
        get_runtime().spawn(async move {
            publish_local_service_on(service_info).await;
        });

        Ok(())
    }

    pub async fn load_local_service(
        &self,
        dxc_path: String,
        dxc_name: String,
        dxc_version: Version,
        dxc_md5: String,
        config: &str,
        compatible_versions: Vec<String>,
        only_in_node: bool,
        timeout: Option<i32>,
    ) -> Result<(), Status> {
        let dll_service = DllService::new(&dxc_path);
        if let Err(err) = dll_service {
            return Err(Status::error(format!(
                "加载DXC:{}_{:?} 应用失败, 请检查dxc是否合法:{:?}",
                dxc_name, dxc_version, err
            )));
        }

        //
        let dll_service = dll_service.unwrap();
        let service_config = Config::load_from_string(config)?;
        let is_system = service_config.get_bool("", "system").unwrap_or(false);
        //
        let local_service = LocalService::new(
            dxc_name,
            dxc_version,
            self.dxc_id,
            dxc_md5,
            is_system,
            compatible_versions,
            dll_service,
            only_in_node,
            timeout
        );

        let local_service = Arc::new(local_service);
        {
            let mut local_service_hoder = self.local_service_holder.write().await;
            *local_service_hoder = Some(local_service.clone());
        }

        local_service.start();

        local_service.clone().init(config).await?;

        let service_info = local_service.get_service_info();
        get_runtime().spawn(async move {
            publish_local_service_on(service_info).await;
        });
        Ok(())
    }

    async fn unzip_dxc(
        &self,
        dxc_path: &str,
        dxc_unzip_path: &str,
        dxc_name_with_version: &str,
    ) -> Result<(), Status> {
        if file_utils::is_dir_exist(&dxc_unzip_path).await {
            return Ok(());
        }
        //
        file_utils::create_dir(&dxc_unzip_path).await;
        let input =
            File::open(&dxc_path).expect(&format!("打开文件失败，请检查文件{}是否存在", &dxc_path));

        //
        let zip = zip::ZipArchive::new(input);
        //
        if let Err(_) = zip {
            return Err(Status::error(format!(
                "打开文件失败，请检查文件:{} 是否是 DXC 文件",
                dxc_path
            )));
        }
        let mut zip = zip.unwrap();
        for i in 0..zip.len() {
            let file = zip.by_index(i); //.expect("读取文件失败！");
            if let Err(_) = file {
                return Err(Status::error(format!(
                    "读取DXC:{} 失败",
                    &dxc_name_with_version
                )));
            }
            let mut file = file.unwrap();
            let file_name = file.mangled_name();
            // 创建文件的完整路径
            let output_path = Path::new(&dxc_unzip_path).join(file_name);
            file_utils::create_file_path_sync(output_path.as_os_str().to_str().unwrap());
            let output_file = File::create(&output_path);
            if let Err(_) = output_file {
                return Err(Status::error(format!(
                    "创建 DXC: {} 文件 {} 失败",
                    &dxc_name_with_version,
                    &output_path.to_str().unwrap()
                )));
            }
            let mut output_file = output_file.unwrap();
            let ret = std::io::copy(&mut file, &mut output_file);

            if let Err(_) = ret {
                return Err(Status::error(format!(
                    "写入 DXC: {}  文件 {} 失败",
                    &dxc_name_with_version,
                    &output_path.to_str().unwrap()
                )));
            };
        }
        Ok(())
    }

    /**
     *
     */
    pub async fn load_local_or_ipc_service(
        &self,
        only_build_ipc: bool,
        config: &str,
        compatible_versions: Vec<String>,
        only_in_node: bool,
        sys_cpu_info: String,
        timeout: Option<i32>,
    ) -> Result<(), Status> {
        //
        let dxc_name = self.name();

        let dxc_version = self.verison().to_string();

        let dxc_name_with_version = format!("{}-{}-{}", dxc_name, dxc_version, sys_cpu_info);

        // dxc 加压后的路径
        let dxc_unzip_path = format!("./dxc/{}", dxc_name_with_version);
        //
        let dxc_path: String = format!("./dxc/{}.dxc", dxc_name_with_version);

        if !file_utils::is_file_exist(&dxc_path) {
            return Err(Status::error(format!("加载失败: dxc :{} 不存在", dxc_path)));
        }
        // 获取 dxc 的md5，用于获取 dxc 在web3，注册的信息
        let dxc_md5 = file_utils::get_md5(&dxc_path);

        if let Err(err) = dxc_md5 {
            return Err(Status::error(format!(
                "加载失败: dxc: {:?} 不存在",
                err.to_string()
            )));
        }

        let dxc_md5 = dxc_md5.unwrap();
        // 解压 dxc
        self.unzip_dxc(&dxc_path, &dxc_unzip_path, &dxc_name_with_version)
            .await?;
        // 读取 dxc 元信息文件
        let manifest_path = format!("{}/manifest.json", &dxc_unzip_path);
        //
        let manifest = file_utils::read_file_as_string(&manifest_path).await;

        if let Err(_) = manifest {
            return Err(Status::error(format!(
                "加载DXC:{} manifest.json文件失败，请检查dxc是否合法",
                &dxc_name_with_version
            )));
        }

        let manifest = manifest.unwrap();
        //
        let manifest = serde_json::from_str::<Manifest>(&manifest);

        if let Err(_) = manifest {
            return Err(Status::error(format!(
                "读取DXC:{} manifest.json文件失败, 请检查dxc是否合法",
                &dxc_name_with_version
            )));
        }

        let manifest = manifest.unwrap();
        // rust 编写的 dxc 要判断系统

        let system = env_utils::get_system()?;

        if manifest.language == "rust" && manifest.system != system {
            return Err(Status::error(format!(
                "加载DXC:{} 失败,当前系统为：{}, 不能加载 {} 的DXC",
                &dxc_name_with_version,
                std::env::consts::OS,
                &manifest.system
            )));
        }

        if manifest.language == "rust" {
            let snake_dxc_name = string_utils::convert_to_snake_case(&dxc_name);
            #[cfg(unix)]
            let dxc_path = format!("{}/lib{}.so", &dxc_unzip_path, &snake_dxc_name);
            #[cfg(windows)]
            let dxc_path = format!("{}/{}.dll", &dxc_unzip_path, &snake_dxc_name);
            // 设置 dxc 加载时的环境变量
            let path_setter = PathSetter::set(&dxc_unzip_path);
            let result = self
                .load_local_service(
                    dxc_path,
                    dxc_name,
                    self.verison(),
                    dxc_md5,
                    config,
                    compatible_versions,
                    only_in_node,
                    timeout,
                )
                .await;
            drop(path_setter);
            result
        } else {
            self.load_ipc_service(
                dxc_name,
                self.verison(),
                dxc_md5,
                manifest.language,
                only_build_ipc,
                config,
                compatible_versions,
                only_in_node,
                timeout,
            )
            .await
        }
    }

    pub async fn add_outer_service(
        &self,
        outer_service: Arc<Box<OuterService>>,
    ) -> Result<(), Status> {
        let mut outer_service_holder = self.outer_service_holder.write().await;

        if outer_service_holder.is_some() {
            return Err(Status::error(format!(
                "添加 outer service 失败： {} {:?} 已经存在！",
                self.dxc_name,
                self.verison()
            )));
        }

        *outer_service_holder = Some(outer_service);

        Ok(())
    }

    //
    pub async fn remove_local_or_ipc_service(&self) {
        //
        // 获取到元素
        let local_service = {
            let mut local_service_holder = self.local_service_holder.write().await;

            let local_service = local_service_holder.take();

            if let Some(local_service) = local_service {
                Some(local_service)
            } else {
                None
            }
        };
        //
        if let Some(local_service) = local_service {
            info!("移除 {}  {} local 服务", self.dxc_name, self.verison());
            let clone_local_service = local_service.clone();
            let _ = task::spawn_blocking(move || {
                clone_local_service.finalize();
            })
            .await;

            local_service.stop().await;
            let service_info = local_service.get_service_info();
            get_runtime().spawn(async move {
                publish_local_service_off(service_info).await;
            });
            return;
        }

        // let ipc_service = {
        //     let  ipc_service_holder = self.ipc_service_holder.read().await;
        //     // let ipc_service = ipc_service_holder.service.take();
        //     if let Some(ipc_service) = ipc_service_holder.service {
        //         Some(ipc_service.clone())
        //     } else {
        //         None
        //     }
        // };

        let ipc_service = self.get_ipc_service().await;
        //
        if let Some(ipc_service) = ipc_service {
            info!("移除 ipc {}  {}  服务", self.dxc_name, self.verison());
            ipc_service.async_finalize().await;
            ipc_service.stop().await;
            let service_info = ipc_service.get_service_info();

            // 删除
            {
                let mut ipc_service_holder = self.ipc_service_holder.write().await;
                ipc_service_holder.take();
            }

            get_runtime().spawn(async move {
                publish_local_service_off(service_info).await;
            });
        }

        //
    }
    /**
     *
     */
    pub async fn get_local_service(&self) -> Option<Arc<Box<LocalService>>> {
        let local_service_holder = self.local_service_holder.read().await;
        local_service_holder.clone()
        // let local_service = local_service_holder.clone();
        // return if let Some(local_service) = local_service {
        //     Some(local_service)
        // } else {
        //     None
        // };
    }
    /**
     *
     */
    pub async fn get_ipc_service(&self) -> Option<Arc<Box<IPCService>>> {
        let ipc_service_holder = self.ipc_service_holder.read().await;

        ipc_service_holder.clone()

        // let ipc_service = ipc_service_holder.clone();

        // return if let Some(ipc_service) = ipc_service {
        //     Some(ipc_service)
        // } else {
        //     None
        // };
    }

    pub async fn get_outer_service(&self) -> Option<Arc<Box<OuterService>>> {
        let outer_service_holder = self.outer_service_holder.read().await;

        outer_service_holder.clone()

        // return if let Some(outer_service) = outer_service {
        //     Some(outer_service)
        // } else {
        //     None
        // };
    }
    /**
     *
     */
    pub async fn get_remote_service_by_conn_id(
        &self,
        conn_id: i64,
    ) -> Option<Arc<Box<RemoteService>>> {
        let remote_service_holder = self.remote_service_holder.write().await;
        let remote_service_id = remote_service_holder.service_conn_id_map.get(&conn_id);

        if let Some(remote_service_id) = remote_service_id {
            let remote_service = remote_service_holder.service_id_map.get(remote_service_id);

            if let Some(remote_service) = remote_service {
                return Some(remote_service.clone());
            }
        }
        None
    }

    pub async fn add_remote_service_and_return_ins(
        &self,
        md5: String,
        service_id: i64,
        compatible_versions: Vec<String>,
    ) -> Arc<Box<RemoteService>> {
        let mut remote_service_holder = self.remote_service_holder.write().await;

        let remote_service = remote_service_holder.service_id_map.get(&service_id);

        if let Some(remote_service) = remote_service {
            return remote_service.clone();
        }

        let remote_service = RemoteService::new(
            service_id,
            self.name(),
            self.verison(),
            md5,
            compatible_versions,
        );

        let remote_service = Arc::new(remote_service);

        remote_service_holder
            .service_id_map
            .insert(service_id, remote_service.clone());

        let clone_remote_service = remote_service.clone();

        // 启动接收消息

        clone_remote_service.start();

        remote_service
    }

    async fn get_one_has_channel_remote_service(&self) -> Option<Arc<Box<RemoteService>>> {
        let remote_service_holder = self.remote_service_holder.read().await;
        for (_key, value) in remote_service_holder.service_id_map.iter() {
            if value.has_channel() {
                return Some(value.clone());
            }
        }
        None
    }

    pub async fn add_channel_to_remote_service_by_conn_id(&self, conn_id: i64, channel_id: i64) {
        let remote_service_holder = self.remote_service_holder.write().await;

        let remote_service_id = remote_service_holder.service_conn_id_map.get(&conn_id);

        if remote_service_id.is_none() {
            return;
        }
        let remote_service_id = remote_service_id.unwrap();

        let remote_service = remote_service_holder.service_id_map.get(&remote_service_id);

        if remote_service.is_none() {
            return;
        }
        remote_service.unwrap().add_channel_id(channel_id);
    }

    pub async fn remove_channel_in_remote_service_by_conn_id(&self, conn_id: i64, channel_id: i64) {
        let remote_service_holder = self.remote_service_holder.write().await;

        let remote_service_id = remote_service_holder.service_conn_id_map.get(&conn_id);

        if remote_service_id.is_none() {
            return;
        }
        let remote_service_id = remote_service_id.unwrap();

        let remote_service = remote_service_holder.service_id_map.get(&remote_service_id);

        if remote_service.is_none() {
            return;
        }
        remote_service.unwrap().remove_channel_id(channel_id);
    }

    /**
     *
     */
    pub async fn add_channel_to_remote_service(
        &self,
        channel_id: i64,
        md5: String,
        service_id: i64,
        compatible_versions: Vec<String>,
    ) {
        let remote_service = self
            .add_remote_service_and_return_ins(md5, service_id, compatible_versions)
            .await;

        remote_service.add_channel_id(channel_id);

        // 添加进去
        self.add_remote_service_id_to_conn_map(channel_id, remote_service.id())
            .await;
    }
    /**
     *
     */
    async fn add_remote_service_id_to_conn_map(&self, channel_id: i64, service_id: i64) {
        let airport = Application::get_airport();

        let channel = airport.get_channel_manager().get_channel(channel_id).await;

        if channel.is_none() {
            return;
        }
        let channel = channel.unwrap();

        let mut remote_service_holder = self.remote_service_holder.write().await;

        //
        remote_service_holder
            .service_conn_id_map
            .insert(channel.conn_id(), service_id);
    }

    /**
     *  移除远程Service的通道信息
     */
    pub async fn remove_all_remote_service_channel_id(&self, channel_id: i64) {
        //
        let remote_service_holder = self.remote_service_holder.read().await;

        for (_key, value) in remote_service_holder.service_id_map.iter() {
            value.remove_channel_id(channel_id);
        }
    }

    /**
     *
     */
    pub async fn message_in(&self, message: &Vec<u8>) -> bool {
        let local_service = self.get_local_service().await;

        if let Some(local_service) = local_service {
            local_service.message_in(message);
            return true;
        }

        let ipc_service = self.get_ipc_service().await;

        if let Some(ipc_service) = ipc_service {
            ipc_service.message_in(message);
            return true;
        }

        let outer_service: Option<Arc<Box<OuterService>>> = self.get_outer_service().await;

        if let Some(outer_service) = outer_service {
            outer_service.message_in(message);

            return true;
        }

        false
    }
    //
    pub async fn is_compatible_version(&self, channel_id: i64, version: &String) -> bool {
        if channel_id == SELF_CHANNEL || channel_id == UNSET_CHANNEL {
            let local_service = self.get_local_service().await;
            if let Some(local_service) = local_service {
                if local_service.is_compatible_version(version) {
                    return true;
                }
            }
            //
            let ipc_service = self.get_ipc_service().await;
            if let Some(ipc_service) = ipc_service {
                if ipc_service.is_compatible_version(version) {
                    return true;
                }
            }
            // 如果指定未本节点 的服务，找不到时，返回 None
            if channel_id == SELF_CHANNEL {
                return false;
            }
        }

        // 有指定的节点，寻找指定节点
        if channel_id != UNSET_CHANNEL {
            let airport = Application::get_airport();
            let channel = airport.get_channel_manager().get_channel(channel_id).await;
            if let Some(channel) = channel {
                let remote_service = self.get_remote_service_by_conn_id(channel.conn_id()).await;

                if let Some(remote_service) = remote_service {
                    if remote_service.is_compatible_version(version) {
                        return true;
                    }
                }
            }
        } else {
            let remote_service = self.get_one_has_channel_remote_service().await;
            if let Some(remote_service) = remote_service {
                if remote_service.is_compatible_version(version) {
                    return true;
                }
            }
        }
        false
    }
    //
    pub async fn get_local_hide_service_md5_by_compatible_version(
        &self,
        version: &String,
    ) -> Option<String> {
        let local_service = self.get_local_service().await;
        if let Some(local_service) = local_service {
            if local_service.is_only_in_node() && local_service.is_compatible_version(version) {
                return Some(local_service.md5());
            }
        }
        //
        let ipc_service = self.get_ipc_service().await;
        if let Some(ipc_service) = ipc_service {
            if ipc_service.is_only_in_node() && ipc_service.is_compatible_version(version) {
                return Some(ipc_service.md5());
            }
        }

        None
    }
    //
    pub async fn get_service_md5_and_channel_id_by_compatible_version(
        &self,
        channel_id: i64,
        version: &String,
    ) -> Option<(String, i64)> {
        if channel_id == SELF_CHANNEL || channel_id == UNSET_CHANNEL {
            let local_service = self.get_local_service().await;
            if let Some(local_service) = local_service {
                if local_service.is_compatible_version(version) {
                    return Some((local_service.md5(), SELF_CHANNEL));
                }
            }
            //
            let ipc_service = self.get_ipc_service().await;
            if let Some(ipc_service) = ipc_service {
                if ipc_service.is_compatible_version(version) {
                    return Some((ipc_service.md5(), SELF_CHANNEL));
                }
            }
            // 如果指定未本节点 的服务，找不到时，返回 None
            if channel_id == SELF_CHANNEL {
                return None;
            }
        }

        // 有指定的节点，寻找指定节点
        if channel_id != UNSET_CHANNEL {
            let airport = Application::get_airport();
            let channel = airport.get_channel_manager().get_channel(channel_id).await;
            if let Some(channel) = channel {
                let remote_service = self.get_remote_service_by_conn_id(channel.conn_id()).await;

                if let Some(remote_service) = remote_service {
                    if remote_service.is_compatible_version(version) {
                        return Some((remote_service.md5(), channel_id));
                    }
                }
            }
        } else {
            let remote_service = self.get_one_has_channel_remote_service().await;
            if let Some(remote_service) = remote_service {
                if remote_service.is_compatible_version(version) {
                    if let Some(channel_id) = remote_service.get_one_channel_id() {
                        return Some((remote_service.md5(), channel_id));
                    }
                }
            }
        }
        None
    }
    //
    pub async fn get_local_hide_service_md5(&self) -> Option<String> {
        let local_service = self.get_local_service().await;
        if let Some(local_service) = local_service {
            if local_service.is_only_in_node() {
                return Some(local_service.md5());
            }
        }
        let ipc_service = self.get_ipc_service().await;

        if let Some(ipc_service) = ipc_service {
            if ipc_service.is_only_in_node() {
                return Some(ipc_service.md5());
            }
        }
        None
    }
    //
    pub async fn get_service_md5_and_channel_id(&self, channel_id: i64) -> Option<(String, i64)> {
        // 当 channel 未自身或者未指定节点时，优先搜寻本节点的服务
        if channel_id == SELF_CHANNEL || channel_id == UNSET_CHANNEL {
            let local_service = self.get_local_service().await;
            if let Some(local_service) = local_service {
                return Some((local_service.md5(), SELF_CHANNEL));
            }
            let ipc_service = self.get_ipc_service().await;
            if let Some(ipc_service) = ipc_service {
                return Some((ipc_service.md5(), SELF_CHANNEL));
            }
            // 如果指定未本节点 的服务，找不到时，返回 None
            if channel_id == SELF_CHANNEL {
                return None;
            }
        }
        // 有指定的节点，寻找指定节点
        if channel_id != UNSET_CHANNEL {
            let airport = Application::get_airport();
            let channel = airport.get_channel_manager().get_channel(channel_id).await;
            if let Some(channel) = channel {
                let remote_service = self.get_remote_service_by_conn_id(channel.conn_id()).await;
                if let Some(remote_service) = remote_service {
                    return Some((remote_service.md5(), channel_id));
                }
            }
        } else {
            let remote_service = self.get_one_has_channel_remote_service().await;
            if let Some(remote_service) = remote_service {
                let channel_id = remote_service.get_one_channel_id();
                if let Some(channel_id) = channel_id {
                    return Some((remote_service.md5(), channel_id));
                }
            }
        }
        None
    }
    //
    pub async fn get_all_local_service_info(&self, filter_hide_node: bool) -> ServiceInfos {
        let mut service_infos = ServiceInfos::default();

        service_infos.service_infos = Vec::with_capacity(1);

        let local_service = self.get_local_service().await;
        if let Some(local_service) = local_service {
            if filter_hide_node {
                if !local_service.is_only_in_node() {
                    service_infos
                        .service_infos
                        .push(local_service.get_service_info());
                }
            } else {
                service_infos
                    .service_infos
                    .push(local_service.get_service_info());
            }
        }

        let ipc_service = self.get_ipc_service().await;

        if let Some(ipc_service) = ipc_service {
            if filter_hide_node {
                if !ipc_service.is_only_in_node() {
                    service_infos
                        .service_infos
                        .push(ipc_service.get_service_info());
                }
            } else {
                service_infos
                    .service_infos
                    .push(ipc_service.get_service_info());
            }
        }

        service_infos
    }
}
