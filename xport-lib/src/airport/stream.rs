use std::{
    collections::{HashMap, HashSet, LinkedList},
    sync::{
        atomic::{AtomicBool, AtomicU64},
        Arc,
    },
    time::Duration,
};

use crate::{
    application::{get_runtime, Application},
    service::service::Service as _,
};
use tokio::{
    sync::mpsc::{self, UnboundedReceiver, UnboundedSender},
    time::sleep,
};
use tracing::info;
use x_common_lib::{
    base::id_generator,
    protocol::protocol_v3::ProtocolV3Writer,
    service::sys_service_api::{PublishInfo, TopicKey},
    utils::time_utils,
};

use super::channel::SELF_CHANNEL;

#[derive(Clone, Copy)]
enum MessageSignal {
    Normal,     // 处理信息
    Handle,     // 单纯通知处理信息，无消息ID
    CheckAlive, // 检查是否还有订阅
    Exit,       // 退出
}

pub struct Stream {
    // 增加一个 filter
    // 订阅的
    // topic => conn_Id， 订阅消息的节点数量
    //  xport_subscribe_topic_map
    subscribe_xport_map: std::sync::RwLock<(
        HashMap<TopicKey, HashSet<i64>>,
        HashMap<i64, HashSet<TopicKey>>,
    )>,

    sender: UnboundedSender<(PublishInfo, bool, Vec<u8>)>,

    receiver: tokio::sync::Mutex<UnboundedReceiver<(PublishInfo, bool, Vec<u8>)>>,

    // topic 规则 = 唯一标识 + 消息名称
    // 订阅者，推送 的 conn_id => topic => dxc_id
    subscriber_map: std::sync::RwLock<HashMap<i64, HashMap<TopicKey, HashSet<i64>>>>,
    // 订阅版本，防止并发情况下删除错误
    xport_subscribe_version: AtomicU64,
    // 是否已经启动队列长度检测
    launched_queue_size_checker: AtomicBool,
    //  data, req_id, subsceibe_version, cur_version
    topic_sender_map: std::sync::RwLock<
        HashMap<
            i64,
            (
                // Arc<Vec<u8>> 数据，MessageSignal 信号，u64 当前接受的版本号
                Arc<UnboundedSender<(Arc<Vec<u8>>, MessageSignal, u64)>>,
                // 当前接收者的版本号
                u64,
            ),
        >,
    >,
    //
}

impl Stream {
    //
    pub fn new() -> Self {
        let (sender, receiver) = mpsc::unbounded_channel::<(PublishInfo, bool, Vec<u8>)>();

        // get_runtime().spawn(Stream::receive_publish_message(receiver));
        Stream {
            sender,
            receiver: tokio::sync::Mutex::new(receiver),
            subscribe_xport_map: std::sync::RwLock::new((HashMap::new(), HashMap::new())),

            launched_queue_size_checker: AtomicBool::new(false),

            xport_subscribe_version: AtomicU64::new(1),

            subscriber_map: std::sync::RwLock::new(HashMap::new()),

            topic_sender_map: std::sync::RwLock::new(HashMap::new()),
        }
    }

    pub async fn receive_publish_message(&self) {
        let mut receiver = self.receiver.lock().await;

        let airport = Application::get_airport();
        let dxc_manager = airport.get_dxc_manager();
        let signer = Application::get_signer();
        let has_private_key = signer.has_private_key();

        while let Some((publish_info, is_ipc, message)) = receiver.recv().await {
            let sender_service_key = if is_ipc {
                let ipc_service = dxc_manager
                    .get_ipc_service_by_id(publish_info.service_id)
                    .await;
                //
                if ipc_service.is_none() {
                    continue;
                }
                ipc_service.unwrap().get_service_key()
            } else {
                let local_service = dxc_manager
                    .get_local_service_by_id(publish_info.service_id)
                    .await;
                //
                if local_service.is_none() {
                    continue;
                }
                local_service.unwrap().get_service_key()
            };
            //
            let req_id = id_generator::gen_id();
            let mut v3_writer =
                ProtocolV3Writer::new(message.len() as u32, req_id, &sender_service_key);
            v3_writer.write_msg_body(&message, has_private_key);
            let v3_msg = Arc::new(v3_writer.msg_buffer);

            let mut topic_key = TopicKey::default();
            topic_key.topic = publish_info.topic;
            topic_key.tag = publish_info.tag;
            self.publish_message_inner(publish_info.fix_conn_id, &topic_key, v3_msg);
        }
    }

    // 检查队列的长度的
    fn check_alive() {
        // 检查队列长度
        let airport = Application::get_airport();
        let stream = airport.get_stream();
        //
        let ret = stream.launched_queue_size_checker.compare_exchange(
            false,
            true,
            std::sync::atomic::Ordering::SeqCst,
            std::sync::atomic::Ordering::SeqCst,
        );
        if ret.is_err() {
            return;
        }
        //
        get_runtime().spawn(async {
            loop {
                sleep(Duration::from_millis(1000)).await;

                let airport = Application::get_airport();
                let stream = airport.get_stream();
                let topic_sender_map = stream.topic_sender_map.read().unwrap();
                for (_, topic_sender) in topic_sender_map.iter() {
                    let _ = topic_sender.0.send((
                        Arc::new(Vec::default()),
                        MessageSignal::CheckAlive,
                        topic_sender.1,
                    ));
                }
            }
        });
    }

    /**
     * 删除 conn_id 所有订阅
     */
    fn remove_xport(&self, xport_conn_id: i64) {
        //
        {
            // 删除对应所有的 topic
            let mut subscribe_xport_map = self.subscribe_xport_map.write().unwrap();

            let topic_key_set = subscribe_xport_map.1.remove(&xport_conn_id);

            if let Some(topic_key_set) = topic_key_set {
                for topic_key in topic_key_set.into_iter() {
                    let xport_id_set = subscribe_xport_map.0.get_mut(&topic_key);
                    if let Some(xport_id_set) = xport_id_set {
                        xport_id_set.remove(&xport_conn_id);
                    }
                }
            }
        }
        //
        self.remove_xport_queue(xport_conn_id);
    }
    /**
     * 绑定监听队列
     */
    fn build_xport_queue(&self, xport_conn_id: i64) {
        //
        Self::check_alive();
        let mut receiver = {
            let mut topic_sender_map = self.topic_sender_map.write().unwrap();
            let topic_sender = topic_sender_map.get_mut(&xport_conn_id);
            let new_version = self
                .xport_subscribe_version
                .fetch_add(1, std::sync::atomic::Ordering::SeqCst);
            // 已经存在的话，增加版本号即可
            if let Some(topic_sender) = topic_sender {
                topic_sender.1 = new_version;
                return;
            }
            let (sender, receiver) =
                mpsc::unbounded_channel::<(Arc<Vec<u8>>, MessageSignal, u64)>();
            topic_sender_map.insert(xport_conn_id, (Arc::new(sender), new_version));
            receiver
        };
        let runtime = get_runtime();
        runtime.spawn(async move {
            let airport = Application::get_airport();

            let channel_manager = airport.get_channel_manager();
            // 接收所有信息，解决消息乱序的问题
            let mut msg_list = LinkedList::new();

            let config = Application::get_config();
            // 毫秒
            let queue_keep_duration = config
                .get_i64("xport", "queue-keep-duration")
                .unwrap_or(1000);

            let mut last_check_time: i64 = 0;

            while let Some(msg) = receiver.recv().await {
                //
                match msg.1 {
                    MessageSignal::Normal => msg_list.push_back(msg.0),
                    MessageSignal::Handle => {
                        // 用于唤醒队列
                    }
                    MessageSignal::CheckAlive => {
                        if xport_conn_id == SELF_CHANNEL {
                            continue;
                        }
                        //
                        if last_check_time == 0 {
                            // 第一次判断时间
                            last_check_time = time_utils::cur_timestamp();
                            continue;
                        }
                        let channel_manager = airport.get_channel_manager();
                        let stream = airport.get_stream();
                        if channel_manager.conn_id_have_channel(xport_conn_id).await {
                            last_check_time = 0;
                            continue;
                        }
                        if (time_utils::cur_timestamp() - last_check_time) > queue_keep_duration {
                            stream.remove_xport(xport_conn_id);
                        }
                        continue;
                    }
                    MessageSignal::Exit => {
                        let stream = airport.get_stream();
                        //
                        let mut topic_sender_map = stream.topic_sender_map.write().unwrap();
                        //
                        let topic_sender = topic_sender_map.get(&xport_conn_id);
                        // 判断版本是否一致，一致的话，则删除
                        if let Some(topic_sender) = topic_sender {
                            if topic_sender.1 == msg.2 {
                                topic_sender_map.remove(&xport_conn_id);
                                break;
                            }
                        }
                        continue;
                    }
                }
                //
                loop {
                    let data = msg_list.pop_front();
                    if data.is_none() {
                        break;
                    }
                    let data = data.unwrap();

                    if xport_conn_id == SELF_CHANNEL || xport_conn_id == airport.xrpc_net.conn_id()
                    {
                        let data = data.to_vec();
                        airport.message_in(SELF_CHANNEL, SELF_CHANNEL, data).await;
                    } else {
                        let channel = channel_manager.get_channel_by_conn_id(xport_conn_id).await;
                        //
                        if channel.is_none() {
                            info!("消息分发 xport_conn_id = {:?} 不存在连接", xport_conn_id);
                            msg_list.push_front(data);
                            break;
                        }
                        // 重试

                        let channel = channel.unwrap();
                        let is_succeed = channel.send_message(&data).await;
                        if !is_succeed {
                            msg_list.push_front(data);
                            channel_manager.remove_channel(channel.id()).await;
                            continue;
                        }
                    }
                    //
                    if msg_list.is_empty() {
                        break;
                    }
                }
            }
            // todo 增加退出通知事件
        });
    }

    fn remove_xport_queue(&self, xport_conn_id: i64) {
        let topic_sender = self.get_xport_sender(xport_conn_id);

        if let Some((sender, cur_version)) = topic_sender {
            let _ = sender.send((Arc::new(Vec::default()), MessageSignal::Exit, cur_version));
        }
    }

    fn get_xport_sender(
        &self,
        xport_conn_id: i64,
    ) -> Option<(
        Arc<UnboundedSender<(Arc<Vec<u8>>, MessageSignal, u64)>>,
        u64,
    )> {
        let topic_sender_map = self.topic_sender_map.read().unwrap();

        let topic_sender = topic_sender_map.get(&xport_conn_id);

        if let Some(topic_sender) = topic_sender {
            Some((topic_sender.0.clone(), topic_sender.1))
        } else {
            None
        }
    }

    fn get_real_xport_conn_id(&self, conn_id: i64) -> i64 {
        let airport = Application::get_airport();

        if airport.is_self_conn_id(conn_id) {
            SELF_CHANNEL
        } else {
            conn_id
        }
    }

    pub fn publish_message(&self, publish_info: PublishInfo, is_ipc: bool, msg: Vec<u8>) {
        self.sender.send((publish_info, is_ipc, msg)).unwrap()
        // let xport_conn_ids = self.get_subscribe_xport(fix_conn_id, topic_key);

        // for xport_conn_id in xport_conn_ids.into_iter() {
        //     let topic_sender = self.get_xport_sender(xport_conn_id);
        //     if topic_sender.is_none() {
        //         info!("{} sender 不存在", xport_conn_id);
        //         continue;
        //     }
        //     let topic_sender = topic_sender.unwrap();
        //     let _ = topic_sender
        //         .0
        //         .send((msg.clone(), MessageSignal::Normal, topic_sender.1));
        // }
    }

    fn publish_message_inner(&self, fix_conn_id: i64, topic_key: &TopicKey, msg: Arc<Vec<u8>>) {
        let xport_conn_ids = self.get_subscribe_xport(fix_conn_id, topic_key);

        for xport_conn_id in xport_conn_ids.into_iter() {
            let topic_sender = self.get_xport_sender(xport_conn_id);
            if topic_sender.is_none() {
                info!("{} sender 不存在", xport_conn_id);
                continue;
            }
            let topic_sender = topic_sender.unwrap();
            let _ = topic_sender
                .0
                .send((msg.clone(), MessageSignal::Normal, topic_sender.1));
        }
    }

    pub fn xport_add_channel(&self, xport_conn_id: i64) {
        let topic_sender = self.get_xport_sender(xport_conn_id);
        if topic_sender.is_none() {
            return;
        }
        let topic_sender = topic_sender.unwrap();
        let _ = topic_sender.0.send((
            Arc::new(Vec::default()),
            MessageSignal::Handle,
            topic_sender.1,
        ));
    }
    /**
     * 添加 xport 订阅的连接 id
     */
    pub fn xport_subscribe_topic(&self, topic_key: &TopicKey, xport_conn_id: i64) {
        let xport_conn_id = self.get_real_xport_conn_id(xport_conn_id);
        {
            let mut subscribe_xport_map = self.subscribe_xport_map.write().unwrap();
            //
            let subscribe_xport_set = subscribe_xport_map
                .0
                .entry(topic_key.to_owned())
                .or_insert(HashSet::new());
            //
            subscribe_xport_set.insert(xport_conn_id);

            let subscribe_topic_set = subscribe_xport_map
                .1
                .entry(xport_conn_id)
                .or_insert(HashSet::new());
            //
            subscribe_topic_set.insert(topic_key.to_owned());
        }

        self.build_xport_queue(xport_conn_id);
        //
    }
    /**
     * 添加 xport 订阅的连接 id
     */
    pub fn xport_cancel_subscribe_topic(&self, topic_key: &TopicKey, xport_conn_id: i64) {
        let xport_conn_id = self.get_real_xport_conn_id(xport_conn_id);

        let mut is_xport_subscribe_topic_empty = true;
        {
            let mut subscribe_xport_map = self.subscribe_xport_map.write().unwrap();
            //
            let subscribe_xport_set = subscribe_xport_map.0.get_mut(topic_key);
            //
            if let Some(subscribe_xport_set) = subscribe_xport_set {
                subscribe_xport_set.remove(&xport_conn_id);
                if subscribe_xport_set.is_empty() {
                    subscribe_xport_map.0.remove(topic_key);
                }
            }
            let subscribe_topic_set = subscribe_xport_map.1.get_mut(&xport_conn_id);
            if let Some(subscribe_topic_set) = subscribe_topic_set {
                subscribe_topic_set.remove(topic_key);
                is_xport_subscribe_topic_empty = subscribe_topic_set.is_empty();
                if is_xport_subscribe_topic_empty {
                    // 删除 channel的信息
                    subscribe_xport_map.1.remove(&xport_conn_id);
                }
            }
        }

        if is_xport_subscribe_topic_empty {
            self.remove_xport_queue(xport_conn_id);
        }

        //
    }

    /**
     * 获取订阅的 xport conn_id
     */

    pub fn get_subscribe_xport(&self, fix_conn_id: i64, topic_key: &TopicKey) -> Vec<i64> {
        //
        let subscribe_xport_map = self.subscribe_xport_map.read().unwrap();
        //
        let subscribe_xport_set = subscribe_xport_map.0.get(topic_key);
        //
        if let Some(subscribe_xport_set) = subscribe_xport_set {
            if fix_conn_id == 0 {
                return subscribe_xport_set.iter().cloned().collect();
            } else {
                if subscribe_xport_set.contains(&fix_conn_id) {
                    return vec![fix_conn_id];
                }
            }
        }
        //
        Vec::default()
    }

    /**
     * 订阅主题
     */
    pub fn dxc_subscribe_topic(
        &self,
        publish_conn_id: i64,
        topic_key: &TopicKey,
        subscriber_id: i64,
    ) {
        let publish_conn_id = self.get_real_xport_conn_id(publish_conn_id);

        let mut subscriber_map = self.subscriber_map.write().unwrap();

        let subscriber_map = subscriber_map
            .entry(publish_conn_id)
            .or_insert(HashMap::new());

        let subscriber_set = subscriber_map
            .entry(topic_key.to_owned())
            .or_insert(HashSet::new());
        //
        subscriber_set.insert(subscriber_id);
    }
    /**
     * 取消订阅主题
     */
    pub fn dxc_cancel_subscribe_topic(
        &self,
        publish_conn_id: i64,
        topic_key: &TopicKey,
        subscriber_id: i64,
    ) {
        let publish_conn_id = self.get_real_xport_conn_id(publish_conn_id);
        //
        let mut subscriber_map = self.subscriber_map.write().unwrap();

        let subscriber_map = subscriber_map.get_mut(&publish_conn_id);

        if subscriber_map.is_none() {
            return;
        }
        let subscriber_map = subscriber_map.unwrap();

        let subscriber_set = subscriber_map.get_mut(topic_key);
        if subscriber_set.is_none() {
            return;
        }
        let subscriber_set = subscriber_set.unwrap();

        subscriber_set.remove(&subscriber_id);
    }
    /**
     * 获取订阅列表
     */
    pub fn get_subscribers(&self, publish_conn_id: i64, topic_key: &TopicKey) -> Vec<i64> {
        //
        let subscriber_map = self.subscriber_map.read().unwrap();
        // 群发消息
        let subscriber_map = subscriber_map.get(&publish_conn_id);
        //
        if subscriber_map.is_none() {
            return Vec::default();
        }
        //
        let subscriber_map = subscriber_map.unwrap();

        let subscriber_set = subscriber_map.get(topic_key);

        if let Some(subscriber_set) = subscriber_set {
            return subscriber_set.iter().cloned().collect();
        }

        Vec::default()
    }
}
