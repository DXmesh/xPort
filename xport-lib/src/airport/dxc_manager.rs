use std::{
    collections::{BTreeMap, HashMap},
    str::FromStr,
    sync::{
        atomic::{AtomicU32, Ordering},
        Arc,
    },
    time::Duration,
};

use crate::service::service::Service;

use tracing::info;
use x_common_lib::{
    base::{id_generator, status::Status},
    service::sys_service_api::{DXCDetailInfo, ServiceInfo, ServiceInfos, ServiceKey},
    utils::{dxc_utils, env_utils, file_utils, string_utils},
};

use super::{channel::UNSET_CHANNEL, dxc::DXC};
use crate::{
    event::publish_look_for_dxc,
    service::{
        ipc_service::ipc_service::IPCService, local_service::LocalService,
        outer_service::OuterService,
    },
};
use semver::Version;
use tokio::{
    sync::{Mutex, RwLock},
    time::sleep,
};

pub struct DXCManager {
    //
    segment_id: AtomicU32,
    //
    load_dxc_mutex: tokio::sync::Mutex<()>,

    // 使用 BTreeMap, dxc 的版本号有序排列
    dxc_map: RwLock<HashMap<String, BTreeMap<Version, Arc<Box<DXC>>>>>,
}

#[allow(dead_code)]
impl DXCManager {
    pub fn new() -> DXCManager {
        DXCManager {
            load_dxc_mutex: Mutex::new(()),
            segment_id: AtomicU32::new(1),
            dxc_map: RwLock::new(HashMap::new()),
        }
    }

    pub fn allocate_sgement_id(&self) -> u32 {
        self.segment_id.fetch_add(1, Ordering::AcqRel)
    }

    pub async fn has_dxc(&self, dxc_name: &str, version: &Version) -> bool {
        let dxc_map = self.dxc_map.read().await;

        if let Some(version_map) = dxc_map.get(dxc_name) {
            if let Some(_) = version_map.get(version) {
                true
            } else {
                false
            }
        } else {
            false
        }
    }

    pub async fn add_dxc_and_return(&self, dxc_name: &str, version: &Version) -> Arc<Box<DXC>> {
        let mut dxc_map = self.dxc_map.write().await;

        let version_map = dxc_map
            .entry(String::from(dxc_name))
            .or_insert(BTreeMap::new());

        let dxc = version_map.get(version);

        if let Some(dxc) = dxc {
            return dxc.clone();
        }

        let dxc = Arc::new(Box::new(DXC::new(dxc_name.into(), version.clone())));

        version_map.insert(version.clone(), dxc.clone());

        dxc
    }
    /**
     *
     */
    pub async fn remove_dxc(&self, dxc_name: &str, version: &Version) {
        let mut dxc_map = self.dxc_map.write().await;
        let version_map = dxc_map.get_mut(dxc_name);
        if version_map.is_none() {
            return;
        }
        version_map.unwrap().remove(version);
    }

    pub async fn has_local_service_by_id(&self, service_id: i64) -> bool {
        let local_servcice = self.get_local_service_by_id(service_id).await;
        return local_servcice.is_some();
    }

    pub async fn get_local_service_by_id(&self, service_id: i64) -> Option<Arc<Box<LocalService>>> {
        let dxc_id = id_generator::get_dxc_id_by_service_id(service_id);
        let dxc = self.get_dxc_by_id(dxc_id).await;
        if dxc.is_none() {
            return None;
        }
        dxc.unwrap().get_local_service().await
    }

    pub async fn get_ipc_service_by_id(&self, service_id: i64) -> Option<Arc<Box<IPCService>>> {
        let dxc_id = id_generator::get_dxc_id_by_service_id(service_id);
        let dxc = self.get_dxc_by_id(dxc_id).await;
        if dxc.is_none() {
            return None;
        }
        dxc.unwrap().get_ipc_service().await
    }

    /**
     *
     */
    pub async fn remove_dxc_channel_id(&self, channel_id: i64) {
        let dxc_map = self.dxc_map.read().await;
        for (_, version_map) in dxc_map.iter() {
            for (_, dxc) in version_map.iter() {
                dxc.remove_all_remote_service_channel_id(channel_id).await;
            }
        }
    }
    /**
     *
     */
    pub async fn get_dxc_by_id(&self, dxc_id: i64) -> Option<Arc<Box<DXC>>> {
        let dxc_map = self.dxc_map.read().await;
        for (_, version_map) in dxc_map.iter() {
            for (_, dxc) in version_map.iter() {
                if dxc.id() == dxc_id {
                    return Some(dxc.clone());
                }
            }
        }
        None
    }

    /**
     * 获取指定节点的服务的 md5
     */
    pub async fn get_service_md5_and_channel_id(
        &self,
        channel_id: i64,
        service_key: &ServiceKey,
    ) -> Result<(String, i64), Status> {
        //
        let dxc_version = Version::from_str(&service_key.dxc_version)
            .map_err(|_| Status::error("DXC 版本号格式错误, 请使用合法的 DXC".into()))?;
        // 尝试 10 次
        for _ in 0..3 {
            let dxc = self
                .get_dxc_by_name_and_version(&service_key.dxc_name, &dxc_version)
                .await;

            if let Some(ref dxc) = dxc {
                let ret = dxc.get_service_md5_and_channel_id(channel_id).await;
                if let Some(value) = ret {
                    return Ok(value);
                }
            }
            let md5 = self
                .get_dxc_md5_and_channel_id_by_name_and_compatible_version(
                    &service_key.dxc_name,
                    &service_key.dxc_version,
                    channel_id,
                )
                .await;
            if let Some(md5) = md5 {
                return Ok(md5);
            }
            // 同一个组件，如果是 hide 的话，拿本地的组件
            if let Some(dxc) = dxc {
                let md5 = dxc.get_local_hide_service_md5().await;
                if let Some(md5) = md5 {
                    return Ok((md5, channel_id));
                }
            }
            let md5 = self
                .get_local_hide_dxc_md5_by_name_and_compatible_version(
                    &service_key.dxc_name,
                    &service_key.dxc_version,
                )
                .await;
            if let Some(md5) = md5 {
                return Ok((md5, channel_id));
            }

            // 如果已经指定了 channel, 找不到 md5 的情况下，发送到这个 channel
            if channel_id != UNSET_CHANNEL {
                return Ok((String::default(), channel_id));
            }

            // 群发消息
            publish_look_for_dxc(&service_key).await;

            // 10ms 检查 一次
            sleep(Duration::from_millis(10)).await;
        }

        Err(Status::error(format!(
            "调用失败: {:?} 没有可用的服务实例, channel_id = {}",
            service_key, channel_id
        )))
    }
    /**
     *
     */
    pub async fn get_dxc_by_name_and_version(
        &self,
        dxc_name: &str,
        version: &Version,
    ) -> Option<Arc<Box<DXC>>> {
        let dxc_map = self.dxc_map.read().await;

        let version_map = dxc_map.get(dxc_name);

        if version_map.is_none() {
            return None;
        }

        let dxc = version_map.unwrap().get(version);
        if dxc.is_some() {
            return Some(dxc.unwrap().clone());
        }

        None
    }

    pub async fn get_local_hide_dxc_md5_by_name_and_compatible_version(
        &self,
        dxc_name: &str,
        version: &String,
    ) -> Option<String> {
        //
        //
        let dxc_map = self.dxc_map.read().await;
        let version_map = dxc_map.get(dxc_name);
        if version_map.is_none() {
            return None;
        }
        let version_map = version_map.unwrap();
        for (_, dxc) in version_map.iter() {
            let md5 = dxc
                .get_local_hide_service_md5_by_compatible_version(version)
                .await;

            if let Some(md5) = md5 {
                return Some(md5);
            }
        }
        None
    }

    pub async fn get_dxc_md5_and_channel_id_by_name_and_compatible_version(
        &self,
        dxc_name: &str,
        version: &String,
        channel_id: i64,
    ) -> Option<(String, i64)> {
        //
        //
        let dxc_map = self.dxc_map.read().await;
        let version_map = dxc_map.get(dxc_name);
        if version_map.is_none() {
            return None;
        }
        let version_map = version_map.unwrap();
        for (_, dxc) in version_map.iter() {
            let ret = dxc
                .get_service_md5_and_channel_id_by_compatible_version(channel_id, version)
                .await;

            if let Some(ret) = ret {
                return Some(ret);
            }
        }
        None
    }

    pub async fn get_dxc_by_name_compatible_version(
        &self,
        dxc_name: &str,
        version: &String,
        channel_id: i64,
    ) -> Option<Arc<Box<DXC>>> {
        let dxc_map = self.dxc_map.read().await;

        let version_map = dxc_map.get(dxc_name);
        if version_map.is_none() {
            return None;
        }

        let version_map = version_map.unwrap();

        for (_, dxc) in version_map.iter() {
            if dxc.is_compatible_version(channel_id, version).await {
                return Some(dxc.clone());
            }
        }
        None
    }

    pub async fn load_local_or_ipc_service(
        &self,
        dxc_name: &str,
        dxc_version: &Version,
        only_build_ipc: bool,
        config: &str,
        compatible_versions: Vec<String>,
        only_in_node: bool,
        timeout: Option<i32>,
    ) -> Result<(), Status> {
        // dxc 串行加载
        let _ = self.load_dxc_mutex.lock().await;
        let mut sys_cpu_info = env_utils::get_system_cpu_info()?;
        // if !only_build_ipc {
        // 如果是 ipc 的调试模式，不判断 dxc 是否存在
        let dxc_path = format!("./dxc/{}-{}-{}.dxc", dxc_name, dxc_version, sys_cpu_info);
        let is_dxc_exist = file_utils::is_file_exist(&dxc_path);

        if !is_dxc_exist {
            //
            let dxc_path = format!("./dxc/{}-{}-none-none.dxc", dxc_name, dxc_version);
            sys_cpu_info = String::from("none-none");

            let is_dxc_exist = file_utils::is_file_exist(&dxc_path);
            if !is_dxc_exist {
                info!("{}-{} 不存在", dxc_name, dxc_version);
                return Err(Status::error(format!("DXC:{} 不存在!", dxc_path)));
            }
        }
        // }
        // 要移除
        // 每一个 dxc 只允许一个实例存在
        let has_dxc = self.has_dxc(dxc_name, &dxc_version).await;
        if has_dxc {
            return Err(Status::error(format!(
                "加载 dxc {} version {} 失败, 服务已经存在!",
                dxc_name,
                dxc_version.to_string()
            )));
        }
        let dxc = self.add_dxc_and_return(dxc_name, &dxc_version).await;
        dxc.load_local_or_ipc_service(
            only_build_ipc,
            config,
            compatible_versions,
            only_in_node,
            sys_cpu_info,
            timeout,
        )
        .await
    }

    pub async fn add_outer_service(
        &self,
        outer_service: Arc<Box<OuterService>>,
    ) -> Result<(), Status> {
        let dxc_name = outer_service.dxc_name();
        let dxc_version = outer_service.version();
        if self.has_dxc(&dxc_name, &dxc_version).await {
            return Err(Status::error(format!(
                "添加 outer service 失败： {} {:?} 已经存在！",
                dxc_name, dxc_version
            )));
        }
        let dxc = self.add_dxc_and_return(&dxc_name, &dxc_version).await;
        outer_service.set_dxc_id(dxc.id());
        dxc.add_outer_service(outer_service).await
    }

    pub async fn get_dxc_id_by_name_and_version(
        &self,
        dxc_name: &str,
        version: &Version,
    ) -> Option<i64> {
        let dxc = self.get_dxc_by_name_and_version(dxc_name, version).await;
        return if let Some(dxc) = dxc {
            Some(dxc.id())
        } else {
            None
        };
    }
    pub async fn get_all_local_service_info(&self, filter_hide_node: bool) -> ServiceInfos {
        let mut local_service_infos = ServiceInfos::default();
        {
            let dxc_map = self.dxc_map.read().await;
            for (_, version_map) in dxc_map.iter() {
                for (_, dxc) in version_map.iter() {
                    local_service_infos.service_infos.append(
                        &mut dxc
                            .get_all_local_service_info(filter_hide_node)
                            .await
                            .service_infos,
                    );
                }
            }
        }
        local_service_infos
    }
    /**
     *
     */
    pub async fn add_channel_id_to_remote_dxc(&self, conn_id: i64, channel_id: i64) {
        let dxc_map = self.dxc_map.read().await;
        for (_, version_map) in dxc_map.iter() {
            for (_, dxc) in version_map.iter() {
                dxc.add_channel_to_remote_service_by_conn_id(conn_id, channel_id)
                    .await;
            }
        }
    }

    /**
     *
     */
    pub async fn remove_channel_id_in_remote_dxc(&self, conn_id: i64, channel_id: i64) {
        let dxc_map = self.dxc_map.read().await;
        for (_, version_map) in dxc_map.iter() {
            for (_, dxc) in version_map.iter() {
                dxc.remove_channel_in_remote_service_by_conn_id(conn_id, channel_id)
                    .await;
            }
        }
    }
    /**
     *
     */
    // pub async fn send_message_to_one_remote_dxc(&self, conn_id: i64, message: &Vec<u8>) {
    //     let dxc_map = self.dxc_map.read().await;
    //     for (_, version_map) in dxc_map.iter() {
    //         for (_, dxc) in version_map.iter() {
    //             let remote_service = dxc.get_remote_service_by_conn_id(conn_id).await;
    //             if remote_service.is_none() {
    //                 continue;
    //             }
    //             let remote_service = remote_service.unwrap();
    //             remote_service.message_in(message);
    //             break;
    //         }
    //     }
    // }

    pub async fn get_all_installed_service(&self) -> Result<ServiceInfos, Status> {
        let dxc_file_list = file_utils::get_file_list("./dxc").await;
        if let Err(err) = dxc_file_list {
            return Err(Status::error(err.to_string()));
        }
        let mut local_service_infos = ServiceInfos::default();

        let dxc_file_list = dxc_file_list.unwrap();

        for dxc_file in dxc_file_list.iter() {
            if !dxc_file.ends_with(".dxc") {
                continue;
            }
            let result = string_utils::extract_dxc_name_and_version_from_dxc_file_name(&dxc_file);
            if result.is_none() {
                continue;
            }
            //  安装的版本列表
            let mut service_info = Box::new(ServiceInfo::default());
            let (dxc_name, dxc_version) = result.unwrap();
            service_info.dxc_name = dxc_name;

            service_info.md5 =
                file_utils::get_md5(format!("./dxc/{}", dxc_file)).unwrap_or(String::default());

            service_info.dxc_version = dxc_version;
            local_service_infos.service_infos.push(service_info);
        }
        Ok(local_service_infos)
    }

    pub async fn get_service_detail(
        &self,
        service_key: &ServiceKey,
    ) -> Result<DXCDetailInfo, Status> {
        let sys_cpu_info = env_utils::get_system_cpu_info()?;

        let dxc_file_name_version = format!(
            "{}-{}-{}",
            service_key.dxc_name, service_key.dxc_version, sys_cpu_info
        );

        let dxc_file_path = format!("./dxc/{}.dxc", dxc_file_name_version);

        let dxc_file_path = if file_utils::is_file_exist(&dxc_file_path) {
            dxc_file_path
        } else {
            format!(
                "./dxc/{}-{}-none-none.dxc",
                service_key.dxc_name, service_key.dxc_version
            )
        };
        dxc_utils::read_dxc_meta_info(&dxc_file_path).await
    }
}
