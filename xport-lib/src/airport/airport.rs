use std::{mem::MaybeUninit, sync::Arc};

use tracing::{debug, info, warn};
use x_common_lib::protocol::{get_version, XID};

use crate::{
    airport::channel::SELF_CHANNEL,
    net::{ipc_net::IPCNet, tcp_net::TCPNet},
    protocol::protocol_handler::ProtocolHandler,
};

use super::{
    channel::is_fixed_channel, channel_manager::ChannelManager, dxc_manager::DXCManager,
    message_manager::MessageManager, stream::Stream,
};

pub trait AirportEventHandler {
    fn init(&self);

    fn start_airport(&self);

    fn start_from_watcher(&self);

    fn end_airport(&self);
}

pub struct Airport {
    // xrpc
    pub xrpc_net: Arc<Box<TCPNet>>,
    // ipc
    pub ipc_net: Arc<Box<IPCNet>>,
    //
    stream: Arc<Box<Stream>>,
    // 协议处理器
    protocol_handlers: [MaybeUninit<Box<dyn ProtocolHandler>>; 256],
    //
    protocol_handlers_init_array: [bool; 256],
    //
    // 消息管理器
    msg_manager: Arc<Box<MessageManager>>,
    //
    channel_manager: Arc<Box<ChannelManager>>,

    dxc_manager: Arc<Box<DXCManager>>,

    // 自身的节点id
    conn_ids: Vec<i64>,
}

unsafe impl Send for Airport {}
unsafe impl Sync for Airport {}

impl Airport {
    pub fn new(
        xrpc_net: Arc<Box<TCPNet>>,
        ipc_net: Arc<Box<IPCNet>>,
        conn_ids: Vec<i64>,
    ) -> Box<Airport> {
        Box::new(Airport {
            xrpc_net,
            ipc_net,
            stream: Arc::new(Box::new(Stream::new())),
            protocol_handlers_init_array: [false; 256],
            protocol_handlers: unsafe { MaybeUninit::uninit().assume_init() },
            msg_manager: Arc::new(Box::new(MessageManager::new())),
            channel_manager: Arc::new(Box::new(ChannelManager::new())),
            dxc_manager: Arc::new(Box::new(DXCManager::new())),
            conn_ids,
        })
    }

    // pub fn

    pub fn get_message_manager(&self) -> Arc<Box<MessageManager>> {
        self.msg_manager.clone()
    }

    pub fn get_channel_manager(&self) -> Arc<Box<ChannelManager>> {
        self.channel_manager.clone()
    }

    pub fn get_stream(&self) -> Arc<Box<Stream>> {
        self.stream.clone()
    }



    pub fn get_dxc_manager(&self) -> Arc<Box<DXCManager>> {
        self.dxc_manager.clone()
    }
    // 判断是否是自身节点
    pub fn is_self_conn_id(&self, conn_id: i64) -> bool {
        self.conn_ids.contains(&conn_id)
    }

    pub async fn response_message(&self, xid: XID, resp_message: Vec<u8>) -> bool {
        // 1 为本 xPort节点的连接, 0 表示连接未声明节点
        if xid.conn_id == SELF_CHANNEL {
            self.message_in(SELF_CHANNEL, SELF_CHANNEL, resp_message)
                .await;
            // 本节点的信息，不删除，在 resp 中删除
            return false;
        }
        //
        let channel_id = self.msg_manager.get_channel_id_from_normal_msg(&xid).await;
        if channel_id.is_none() {
            debug!("返回信息 {} 的通道已经关闭", xid.request_id);
            return true;
        }
        let mut channel_id = channel_id.unwrap();
        // 尝试三次发送消息
        for _ in 0..3 {
            if self
                .send_message_to_channel(channel_id, &resp_message)
                .await
            {
                return true;
            }
            let new_channel_id = self
                .get_channel_manager()
                .get_channel_id_by_conn_id(xid.conn_id)
                .await;

            if new_channel_id.is_none() {
                break;
            }
            channel_id = new_channel_id.unwrap();
        }

        info!(
            "回复消息 conn_id:{} {} 失败...",
            xid.conn_id, xid.request_id
        );
        true
    }

    pub async fn message_in(
        &self,
        send_channel_id: i64,
        receive_channel_id: i64,
        message: Vec<u8>,
    ) {
        // 指定节点
        if is_fixed_channel(receive_channel_id) {
            self.send_message_to_channel(receive_channel_id, &message)
                .await;
            return;
        }

        let version = get_version(&message);
        let version = version & 255;

        if !self.protocol_handlers_init_array[version as usize] {
            warn!("获取不到版本为{}的协议处理器,丢弃消息...", version);
            return;
        }
        //
        let protocol_handler: &MaybeUninit<Box<dyn ProtocolHandler>> =
            &self.protocol_handlers[version as usize];

        unsafe {
            protocol_handler
                .assume_init_ref()
                .handle_msg(self, send_channel_id, receive_channel_id, message)
                .await
        }
    }

    pub fn add_protocol_handler(&mut self, protocol_handler: Box<dyn ProtocolHandler>) -> bool {
        let version: u16 = protocol_handler.get_version();

        if self.protocol_handlers_init_array[version as usize] {
            warn!("添加版本{}处理器失败, 已经存在...", version);
            return false;
        }

        unsafe {
            self.protocol_handlers[version as usize]
                .as_mut_ptr()
                .write(protocol_handler);
        }

        self.protocol_handlers_init_array[version as usize] = true;

        info!("添加版本 {} 处理器成功...", version);
        true
    }

    /**
     * 发送给通道的，忽略即可
     */
    pub async fn send_message_to_channel(&self, channel_id: i64, message: &Vec<u8>) -> bool {
        let channel = self.channel_manager.get_channel(channel_id).await;
        if channel.is_none() {
            return false;
        }
        channel.unwrap().send_message(message).await
    }

    // 和其他 xPort 建立连接
    pub async fn build_xrpc_channel(&self, conn_id: i64) -> Option<i64> {
        self.xrpc_net.connect_to(conn_id).await
    }
}
