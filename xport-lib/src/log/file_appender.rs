use std::{
    fs::{self, File, OpenOptions},
    io,
    path::Path,
};

use x_common_lib::utils::{file_utils, time_utils};

use zip::{write::SimpleFileOptions, CompressionMethod, ZipWriter};
pub struct FileAppender {
    cur_timestamp: i64,
    dir: String,
    writer: File,
}

impl FileAppender {
    //
    fn create_writer(dir: &str, file_name: &str) -> io::Result<File> {
        let path = Path::new(dir).join(file_name);
        let mut open_options = OpenOptions::new();
        open_options.append(true).create(true);

        let new_file = open_options.open(path.as_path());
        if new_file.is_err() {
            if let Some(parent) = path.parent() {
                fs::create_dir_all(parent)?;
                return open_options.open(path);
            }
        }
        new_file
    }

    fn cur_begin_timestamp() -> i64 {
        let cur_timestamp = time_utils::cur_timestamp();

        // 这里已经加了时间偏移
        let local_timestamp = time_utils::timestamp_to_local(cur_timestamp);

        time_utils::begin_timestamp_from(local_timestamp)
    }

    pub fn new(dir: String) -> Self {
        let writer = Self::create_writer(&dir, "xport.log").expect("创建日志文件失败");
        FileAppender {
            dir,
            writer,
            cur_timestamp: Self::cur_begin_timestamp(),
        }
    }

    fn zip_file(zip_writer: &mut ZipWriter<File>, file_path: &str, file_name: String) {
        let mut file = File::open(file_path).expect(&format!("读取文件 {} 失败", file_path));
        let options = SimpleFileOptions::default()
            .compression_method(CompressionMethod::Deflated)
            .unix_permissions(0o755);

        let file_name = if file_name.is_empty() {
            file_utils::get_file_name_from_path(file_path).unwrap()
        } else {
            file_name
        };

        zip_writer
            .start_file(file_name, options)
            .expect(&format!("压缩文件 {} 失败", file_path));

        std::io::copy(&mut file, zip_writer).expect(&format!("压缩文件 {} 失败", file_path));
    }

    fn refresh_writer(&mut self) -> io::Result<()> {
        let begin_timestamp = Self::cur_begin_timestamp();
        if self.cur_timestamp == begin_timestamp {
            return Ok(());
        }

        {
            let temp_writer = Self::create_writer(&self.dir, "xport_temp.log")?;
            let _ = std::mem::replace(&mut self.writer, temp_writer);
        }

        // self.cur_timestamp 这里要减去

        let real_timestamp = self.cur_timestamp - (time_utils::get_offset_whole_seconds()) as i64 * 1000;
        let last_time_file_name = time_utils::timestamp_to_string("yyyy-MM-dd", real_timestamp);
        let src_file_path = format!("{}/xport.log", self.dir);
        let to_file_path = format!("{}/xport.log.{}.gz", self.dir, last_time_file_name);
        //  写入压缩文件
        let output_file = File::create(to_file_path)?;
        let mut zip_writer = ZipWriter::new(output_file);
        Self::zip_file(
            &mut zip_writer,
            &src_file_path,
            format!("xport.log.{}", last_time_file_name),
        );
        zip_writer.finish()?;
        // 删除之前的文件
        file_utils::remove_file_sync(format!("{}/xport.log", self.dir))?;

        {
            let new_writer = Self::create_writer(&self.dir, "xport.log")?;

            let _ = std::mem::replace(&mut self.writer, new_writer);
        }
        file_utils::remove_file_sync(format!("{}/xport_temp.log", self.dir))?;

        // 移动文件指针到文件开头
        self.cur_timestamp = begin_timestamp;

        Ok(())
    }
}

impl io::Write for FileAppender {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        // 这里

        // 判断是否要刷新
        self.refresh_writer()?;

        self.writer.write(buf)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.writer.flush()
    }
}
