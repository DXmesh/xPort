use lazy_static::lazy_static;
use parity_crypto::Keccak256;

use secp256k1::{
    ecdsa::{self, RecoveryId},
    All, Message, PublicKey, SecretKey,
};
use tracing::info;
use x_common_lib::base::status::Status;

lazy_static! {
    static ref SECP256K1: secp256k1::Secp256k1<All> = secp256k1::Secp256k1::new();
}

pub struct Signer {
    private_key: Option<SecretKey>,
    pub str_private_key: String,
    pub address: String,
}

impl Signer {
    pub fn new(str_private_key: String) -> Self {
        let mut address = String::default();
        let private_key = if str_private_key.is_empty() {
            None
        } else {
            let private_key = Self::make_private_key(&str_private_key);
            let pk = private_key.public_key(&SECP256K1);
            address = Self::public_key_to_address(&pk);

            info!("节点地址:{}", address);
            Some(private_key)
        };
        Signer {
            private_key: private_key,
            address: address,
            str_private_key: str_private_key,
        }
    }

    pub fn has_private_key(&self) -> bool {

        self.private_key.is_some()
    }

    pub fn get_private_key(&self) -> String {

        self.str_private_key.clone()
    }
    //
    pub fn sign(&self, buffer: &[u8]) -> Option<Vec<u8>> {
 
        match &self.private_key {
            Some(private_key) => {
                let hash_value = buffer.keccak256();
                let message = Message::from_digest_slice(&hash_value).unwrap();
                let signature = SECP256K1.sign_ecdsa_recoverable(&message, private_key);
                let (recovery_id, compact_signature) = signature.serialize_compact();
                
                
                let v = i32::from(recovery_id).to_le_bytes();
                let mut buffer_with_vrs: Vec<u8> = Vec::with_capacity(buffer.len() + 4 + 64);
                buffer_with_vrs.extend_from_slice(buffer);
                buffer_with_vrs.extend_from_slice(&v);
                buffer_with_vrs.extend_from_slice(&compact_signature);
                Some(buffer_with_vrs)
            }
            None => None,
        }
    }
    #[allow(dead_code)]
    pub fn sign_with_buffer(&self, buffer: &[u8], receive_buffer: &mut [u8]) -> bool {

        match &self.private_key {
            Some(private_key) => {
                let hash_value = buffer.keccak256();
                let message = Message::from_digest_slice(&hash_value).unwrap();
                let signature = SECP256K1.sign_ecdsa_recoverable(&message, private_key);
                let (recovery_id, compact_signature) = signature.serialize_compact();

                let v = i32::from(recovery_id).to_le_bytes();
                receive_buffer[..buffer.len()].copy_from_slice(buffer);
                receive_buffer[buffer.len()..(buffer.len() + v.len())].copy_from_slice(&v);
                receive_buffer[(buffer.len() + v.len())..].copy_from_slice(&compact_signature);

                true
            }
            None => false,
        }
    }
    #[allow(dead_code)]
    pub fn get_from_address(&self, buffer_with_vrs: &[u8]) -> Result<String, Status> {
        let real_message_len = buffer_with_vrs.len() - 68;
        let v = &buffer_with_vrs[real_message_len..real_message_len + 4];
        let r_s = &buffer_with_vrs[real_message_len + 4..real_message_len + 68];

        let buffer = &buffer_with_vrs[0..real_message_len];

        let hash_value = buffer.keccak256();
        let message = Message::from_digest_slice(&hash_value).unwrap();

        let recovery_id = RecoveryId::try_from(i32::from_le_bytes(v.try_into().unwrap())).unwrap();

        let recoverable_signature = ecdsa::RecoverableSignature::from_compact(r_s, recovery_id);

        if let Err(err) = recoverable_signature {
            return Err(Status::error(err.to_string()));
        }

        let recoverable_signature = recoverable_signature.unwrap();
        let public_key = SECP256K1.recover_ecdsa(&message, &recoverable_signature);
        if let Err(err) = public_key {
            return Err(Status::error(err.to_string()));
        }
        let public_key = public_key.unwrap();
        Ok(Self::public_key_to_address(&public_key))
    }
    //
    pub fn verify(&self, buffer_with_vrs: &[u8]) -> Result<String, Status> {
        let real_message_len = buffer_with_vrs.len() - 68;
        let v = &buffer_with_vrs[real_message_len..real_message_len + 4];
        let r_s = &buffer_with_vrs[real_message_len + 4..real_message_len + 68];

        let buffer = &buffer_with_vrs[0..real_message_len];

        let hash_value = buffer.keccak256();

        let message = Message::from_digest_slice(&hash_value).unwrap();

        let recovery_id = RecoveryId::try_from(i32::from_le_bytes(v.try_into().unwrap())).unwrap();

        let recoverable_signature = ecdsa::RecoverableSignature::from_compact(r_s, recovery_id);

        if let Err(err) = recoverable_signature {
            return Err(Status::error(err.to_string()));
        }

        let recoverable_signature = recoverable_signature.unwrap();

        let public_key = SECP256K1.recover_ecdsa(&message, &recoverable_signature);

        if let Err(err) = public_key {
            return Err(Status::error(err.to_string()));
        }

        let public_key = public_key.unwrap();

        let standard_signature = recoverable_signature.to_standard();

        let result = SECP256K1.verify_ecdsa(&message, &standard_signature, &public_key);

        if let Err(err) = result {
            Err(Status::error(err.to_string()))
        } else {
            Ok(Self::public_key_to_address(&public_key))
        }
    }

    fn public_key_to_address(public_key: &PublicKey) -> String {
        let public_key_bytes = public_key.serialize_uncompressed();
        let public_key_bytes = &public_key_bytes[1..];
        let hash = public_key_bytes.keccak256();
        let hash_hex_strng = hex::encode(hash);
        format!("0x{}", &hash_hex_strng[hash_hex_strng.len() - 40..])
    }

    //
    fn make_private_key(private_key: &str) -> SecretKey {
        let private_key2 = if private_key.starts_with("0x") || private_key.starts_with("0X") {
            &private_key[2..]
        } else {
            &private_key
        };
        let private_key_data = hex::decode(private_key2).unwrap();

        let private_key = SecretKey::from_slice(private_key_data.as_slice()).unwrap();
        private_key
    }
}
