use std::{
    collections::{HashMap, LinkedList},
    sync::{atomic::AtomicI64, Arc, Mutex},
};

use async_trait::async_trait;
use semver::Version;
use tokio::{
    sync::{
        mpsc::{self, UnboundedReceiver, UnboundedSender},
        oneshot,
    },
    task,
};
use tracing::{info, warn};
use x_common_lib::{
    base::{id_generator, status::Status},
    protocol::{to_dxc_protocol_msg, MsgType, XID},
    serial::request_message::RequestMessage,
    service::sys_service_api::{ServiceInfo, ServiceKey},
    utils::time_utils,
};

use crate::{
    application::{get_runtime, Application},
    log,
    protocol::pack_message_in_local_service,
};

use super::{loader::ServiceIns, service::Service, timeout_checker::TimeoutChecker};

pub struct LocalService {
    /**
     *
     */
    dxc_name: String,
    /**
     *
     */
    dxc_version: Version,
    /**
     *
     */
    service_id: i64,
    /**
     *
     */
    md5: String,
    /**
     * 是否是系统级别的 dxc
     */
    is_system: bool,
    /**
     * 只在本节点内可见
     */
    only_in_node: bool,
    /**
     * 上线时间
     */
    online_time: AtomicI64,

    compatible_versions: Vec<String>,
    /**
     *  加载的 service dll 的实例
     */
    service_ins: Box<dyn ServiceIns>,
    /**
     * 接收消息的 receiver
     */
    message_receiver: tokio::sync::Mutex<UnboundedReceiver<bool>>,
    /**
     *
     */
    message_sender: UnboundedSender<bool>,
    /**
     *
     */
    normal_message_list: Mutex<LinkedList<XID>>,
    /**
     *
     */
    resp_message_list: Mutex<LinkedList<XID>>,
    /**
     *
     */
    system_message_list: Mutex<LinkedList<XID>>,
    /**
     *
     */
    stream_message_list: Mutex<LinkedList<XID>>,
    /**
     * 超时检测器
     */
    timeout_checker: Arc<Box<TimeoutChecker>>,
    /**
     * 消息返回值的处理函数
     */
    return_message_sender_map: Mutex<HashMap<i64, tokio::sync::oneshot::Sender<Vec<u8>>>>,
}

impl LocalService {
    pub fn new(
        dxc_name: String,
        dxc_version: Version,
        dxc_id: i64,
        md5: String,
        is_system: bool,
        compatible_versions: Vec<String>,
        service_ins: Box<dyn ServiceIns>,
        only_in_node: bool,
        timeout: Option<i32>,
    ) -> Box<LocalService> {
        let (sender, receiver) = mpsc::unbounded_channel::<bool>();

      //  let mut  timeout_checker = Arc::new(Box::new(TimeoutChecker::new())),

        Box::new(LocalService {
            dxc_name,
            service_id: id_generator::gen_service_id(dxc_id),
            md5,
            online_time: AtomicI64::new(0),
            dxc_version,
            is_system,
            only_in_node,
            service_ins,
            compatible_versions,
            message_receiver: tokio::sync::Mutex::new(receiver),
            message_sender: sender,
            system_message_list: Mutex::new(LinkedList::new()),
            resp_message_list: Mutex::new(LinkedList::new()),
            normal_message_list: Mutex::new(LinkedList::new()),
            stream_message_list: Mutex::new(LinkedList::new()),
            timeout_checker: Arc::new(Box::new(TimeoutChecker::new(timeout))),
            return_message_sender_map: Mutex::new(HashMap::new()),
        })
    }

    pub fn dxc_name(&self) -> String {
        self.dxc_name.clone()
    }

    pub fn md5(&self) -> String {
        self.md5.clone()
    }

    pub fn is_compatible_version(&self, version: &String) -> bool {
        self.compatible_versions.contains(version)
    }
    //
    fn get_xid(&self) -> Option<(XID, MsgType)> {
        let xid = {
            let mut message_list = self.system_message_list.lock().unwrap();
            message_list.pop_front()
        };

        if let Some(xid) = xid {
            return Some((xid, MsgType::System));
        }

        let xid = {
            let mut message_list = self.resp_message_list.lock().unwrap();
            message_list.pop_front()
        };

        if let Some(xid) = xid {
            return Some((xid, MsgType::RspNormal));
        }

        let xid = {
            let mut message_list = self.normal_message_list.lock().unwrap();
            message_list.pop_front()
        };
        if let Some(xid) = xid {
            return Some((xid, MsgType::Normal));
        }

        let xid = {
            let mut message_list = self.stream_message_list.lock().unwrap();
            message_list.pop_front()
        };

        if let Some(xid) = xid {
            return Some((xid, MsgType::Stream));
        }

        //
        None
    }

    pub fn is_system(&self) -> bool {
        self.is_system
    }
    pub fn is_only_in_node(&self) -> bool {
        self.only_in_node
    }
    /**
     * 添加返回消息的者
     */
    fn add_return_handler(&self, request_id: i64, sender: tokio::sync::oneshot::Sender<Vec<u8>>) {
        let mut return_message_sender_map = self.return_message_sender_map.lock().unwrap();

        return_message_sender_map.insert(request_id, sender);
    }
    //
    pub fn hanlde_return_message(&self, request_id: i64, buffer: Vec<u8>) {
        let sender = {
            let mut return_message_sender_map = self.return_message_sender_map.lock().unwrap();

            return_message_sender_map.remove(&request_id)
        };

        if sender.is_none() {
            return;
        }

        let sender = sender.unwrap();

        let result = sender.send(buffer);

        if let Err(err) = result {
            warn!("通知消息：{} 返回失败:{:?}", request_id, err)
        }
    }

    async fn handle_message(self: Arc<Box<Self>>) {
        let mut message_receiver = self.message_receiver.lock().await;

        while let Some(is_handle) = message_receiver.recv().await {
            if !is_handle {
                break;
            }
            let xid = self.get_xid();
            // 处理消息
            if xid.is_none() {
                continue;
            }
            let (xid, msg_type) = xid.unwrap();

            let airport = Application::get_airport();

            let message_manager = airport.get_message_manager();

            let message = if msg_type == MsgType::System || msg_type == MsgType::Normal {
                message_manager.get_normal_msg(&xid).await
            } else if msg_type == MsgType::RspNormal {
                message_manager.get_resp_msg(&xid).await
            } else if msg_type == MsgType::Stream {
                message_manager.get_stream_message(&xid)
            } else {
                None
            };

            if message.is_none() {
                info!("获取不到消息：{:?} 。。。", xid);
                continue;
            }
            if msg_type == MsgType::RspNormal {
                message_manager.delete_resp_message(xid).await;
            }
            let (message, channel_id, from_addr) = message.unwrap();
            //
            let dxc_msg = to_dxc_protocol_msg(channel_id, from_addr, message.as_slice());
            //
            if dxc_msg.is_none() {
                warn!("无法识别协议！");
                return;
            }
            let (dxc_msg, sender_service_key) = dxc_msg.unwrap();
            //
            if msg_type == MsgType::Stream {
                //
                self.service_ins.dispatch_message(&dxc_msg);
                message_manager.deref_stream_message(&xid);
            } else {
                let clone_self = self.clone();
                get_runtime().spawn(async move {
                    if msg_type == MsgType::RspNormal {
                        // 考虑到反序列化需要的事件过长，造成堵塞
                        clone_self.service_ins.result_in(xid.request_id, &dxc_msg);
                        //
                    } else {
                        let (sender, receiver) = oneshot::channel::<Vec<u8>>();
                        //
                        clone_self.add_return_handler(xid.request_id, sender);
                        //
                        clone_self.service_ins.dispatch_message(&dxc_msg);
                        //
                        let rsp_message = receiver.await;
                        //
                        if let Err(err) = rsp_message {
                            warn!("{:?}", err);
                            return;
                        }
                        // 返回结果
                        let rsp_message = rsp_message.unwrap();
                        let self_service_key = clone_self.get_service_key();
                        let resp_msg_buffer = pack_message_in_local_service(
                            xid.request_id,
                            &self_service_key,
                            &sender_service_key,
                            MsgType::RspNormal,
                            &rsp_message,
                        );
                        //
                        airport.response_message(xid, resp_msg_buffer).await;
                        message_manager.delete_normal_message(xid).await;
                    }
                });
            }
        }
        message_receiver.close();
        //
    }

    /**
     * 添加等待回复的请求,用于判断是否超时
     */
    pub fn add_waitting_request_id(&self, request_id: i64) {
        self.timeout_checker.add_waitting_request_id(request_id);
    }

    pub fn start(self: &Arc<Box<Self>>) {
        self.online_time.store(
            time_utils::cur_timestamp(),
            std::sync::atomic::Ordering::Relaxed,
        );

        let clone_self = self.clone();

        get_runtime().spawn(async move {
            clone_self.handle_message().await;
        });

        let timeout_checker = self.timeout_checker.clone();
        let service_key = self.get_service_key();

        get_runtime().spawn(async move {
            timeout_checker
                .check_timeout_message(service_key, false)
                .await;
        });
    }

    pub fn get_service_info(&self) -> Box<ServiceInfo> {
        let mut service_info = Box::new(ServiceInfo::default());
        service_info.dxc_name = self.dxc_name();
        service_info.dxc_version = self.dxc_version.to_string();
        service_info.service_id = self.service_id;
        service_info.md5 = self.md5();
        service_info.online_time = self.oline_time();
        service_info.compatible_versions = self.compatible_versions.clone();
        service_info.only_in_node = self.only_in_node;
        service_info
    }
}

impl Drop for LocalService {
    fn drop(&mut self) {
        {
            let mut stream_message_list = self.stream_message_list.lock().unwrap();
            let airport = Application::get_airport();
            let message_manager = airport.get_message_manager();
            for xid in stream_message_list.iter() {
                message_manager.deref_stream_message(xid);
            }
            stream_message_list.clear();
        }

        //
        info!("卸载local_service :{:?}", self.get_service_key());
    }
}

#[async_trait]
impl Service for LocalService {
    fn id(&self) -> i64 {
        self.service_id
    }

    fn version(&self) -> Version {
        self.dxc_version.clone()
    }

    async fn init(self: Arc<Box<Self>>, config: &str) -> Result<(), Status> {
        let log_level = log::level();
        // 就是在这里
        let (sender, receiver) = oneshot::channel::<Vec<u8>>();
        self.add_return_handler(0, sender);
        let config = config.to_string();
        let _ = task::spawn_blocking(move || {
            self.service_ins
                .init(self.service_id, config.as_str(), log_level);
            //
        })
        .await;

        let rsp_message = receiver.await;
        if let Err(err) = rsp_message {
            return Err(Status::error(err.to_string()));
        }
        let rsp_message = rsp_message.unwrap();
        //
        let mut input_stream = protobuf::CodedInputStream::from_bytes(&rsp_message);

        let mut status = Status::default();
        let _ = status.parse_from_input_stream_with_tag_and_len(&mut input_stream);

        let _tag = input_stream.read_raw_tag_or_eof();
        if let Err(_) = _tag {
            return Err(Status::error("返回结果字节码出错！".into()));
        }
        let message = input_stream.read_string();
        if let Err(_) = message {
            return Err(Status::error("返回结果字节码出错！".into()));
        }
        //
        if status.is_erorr() {
            Err(status)
        } else {
            Ok(())
        }
    }

    async fn stop(&self) {
        //
        self.timeout_checker.stop();
        let _ = self.message_sender.send(false);
        self.message_sender.closed().await;
    }

    fn dispatch_message_with_type(&self, msg_type: MsgType, xid: XID) {
        match msg_type {
            MsgType::System => {
                let mut msg_list = self.system_message_list.lock().unwrap();
                msg_list.push_back(xid)
            }
            MsgType::RspNormal => {
                self.timeout_checker.remove_message(xid.request_id);

                let mut msg_list = self.resp_message_list.lock().unwrap();
                msg_list.push_back(xid)
            }
            MsgType::Normal => {
                let mut msg_list = self.normal_message_list.lock().unwrap();

                msg_list.push_back(xid)
            }
            MsgType::Stream => {
                let mut msg_list = self.stream_message_list.lock().unwrap();

                msg_list.push_back(xid)
            }
            _ => {}
        };

        let result = self.message_sender.send(true);

        if let Err(_err) = result {
            warn!(
                "dxc:{}  version:{} 接收消息出错！",
                self.dxc_name, self.dxc_version
            )
        }
    }

    fn finalize(&self) {
        self.service_ins.finalize();
    }

    fn get_service_key(&self) -> ServiceKey {
        ServiceKey::new(
            self.dxc_name.clone(),
            self.dxc_version.to_string(),
            self.md5(),
        )
    }

    fn oline_time(&self) -> i64 {
        self.online_time.load(std::sync::atomic::Ordering::Relaxed)
    }
}
