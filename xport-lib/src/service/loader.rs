use async_trait::async_trait;
use x_common_lib::base::{
    dll_api::{
        AddChannelIdToRemoteServicesApi, BuildChannelApi, CloseChannelApi, DXCCancelSubscribeTopicApi, DXCSubscribeTopicApi, EventResponseApi, GenIDApi, GetAllConnIdApi, GetAllInstalledServiceApi, GetAllLocalServiceApi, GetChannelIdByConnIdApi, GetNodePrivateKeyApi, GetSelfConnIdApi, GetServiceDetailApi, GetServiceIdApi, HasServiceApi, IsSelfNodeApi, LoadServiceApi, OffsetWholeSecondsApi, OutputLogApi, PulishMessageApi, RemoveRemoteServiceAllChannelIdApi, ResponseApi, SendMessageApi, SetChannelXRPCPortApi, SubscribeApi, UnLoadServiceApi, UnSubscribeApi, XportCancelSubscribeTopicApi, XportSubscribeTopicApi
    },
    status::Status,
};

#[cfg(unix)]
use libloading::os::unix::Symbol;

#[cfg(windows)]
use libloading::os::windows::Symbol;

use super::inject_api;

type InitApi = unsafe extern "C" fn(i64, *const u8, u32, i32);

type FinalizeApi = unsafe extern "C" fn();

type DispatchMessageApi = unsafe extern "C" fn(*const u8, u32);

type ResultInApi = unsafe extern "C" fn(i64, *const u8, u32);

type InjectSendMessageApi = unsafe extern "C" fn(SendMessageApi);

type InjectResponseApi = unsafe extern "C" fn(ResponseApi);

type InjectEventResponseApi = unsafe extern "C" fn(EventResponseApi);

type InjectGenIdApi = unsafe extern "C" fn(GenIDApi);

type InjectOffsetWholeSecondsApi = unsafe extern "C" fn(OffsetWholeSecondsApi);

type InjectLogOutputApi = unsafe extern "C" fn(OutputLogApi);
//
type InjectLoadServiceApi = unsafe extern "C" fn(LoadServiceApi);
//
type InjectHasServiceApi = unsafe extern "C" fn(HasServiceApi);

type InjectGetServiceIdApi = unsafe extern "C" fn(GetServiceIdApi);

type InjectUnLoadServiceApi = unsafe extern "C" fn(UnLoadServiceApi);

type InjectBuildChannelApi = unsafe extern "C" fn(BuildChannelApi);

type InjectCloseChannelApi = unsafe extern "C" fn(CloseChannelApi);

type InjectGetAllLocalServiceApi = unsafe extern "C" fn(GetAllLocalServiceApi);

type InjectAdChannelIdToRemoteServicesApi = unsafe extern "C" fn(AddChannelIdToRemoteServicesApi);

type InjectRemoveRemoteServiceAllChannelIdApi =
    unsafe extern "C" fn(RemoveRemoteServiceAllChannelIdApi);

type InjectSetChannelXRPCPortApi = unsafe extern "C" fn(SetChannelXRPCPortApi);

type InjectGetSelfConnIdApi = unsafe extern "C" fn(GetSelfConnIdApi);

type InjectGetAllConnIdApi = unsafe extern "C" fn(GetAllConnIdApi);

type InjectGetChannelIdByConnIdApi = unsafe extern "C" fn(GetChannelIdByConnIdApi);

type InjectSubscribeApi = unsafe extern "C" fn(SubscribeApi);

type InjectUnSubscribeApi = unsafe extern "C" fn(UnSubscribeApi);

type InjectGetNodePrivateKeyApi = unsafe extern "C" fn(GetNodePrivateKeyApi);

type InjectGetAllInstalledServiceApi = unsafe extern "C" fn(GetAllInstalledServiceApi);

type InjectGetServiceDetailApi = unsafe extern "C" fn(GetServiceDetailApi);
// ================ stream 注入的 api =====================

type InjectPulishMessageApi = unsafe extern "C" fn(PulishMessageApi);

type InjectXportSubscribeTopicApi = unsafe extern "C" fn(XportSubscribeTopicApi);

type InjectXportCancelSubscribeTopicApi = unsafe extern "C" fn(XportCancelSubscribeTopicApi);

type InjectDXCSubscribeTopicApi = unsafe extern "C" fn(DXCSubscribeTopicApi);

type InjectDXCCancelSubscribeTopicApi = unsafe extern "C" fn(DXCCancelSubscribeTopicApi);

type InjectIsSelfNodeApi = unsafe extern "C" fn(IsSelfNodeApi);

// type InjectGetConfigApi = unsafe extern "C" fn(GetConfigApi);

/**
 * service 实例
*/

pub trait ServiceIns: Send + Sync {
    fn init(&self, service_id: i64, config: &str, log_level: i32);
    fn dispatch_message(&self, msg: &Vec<u8>);
    fn result_in(&self, request_id: i64, msg: &Vec<u8>);
    fn finalize(&self);
}

pub struct DllService {
    _lib: libloading::Library,
    init_api: Symbol<InitApi>,
    finalize_api: Symbol<FinalizeApi>,
    dispatch_message_api: Symbol<DispatchMessageApi>,
    result_in_api: Symbol<ResultInApi>,
}

//

macro_rules! get_func {
    ($func_name:expr, $lib:expr) => {
        match $lib.get($func_name) {
            Ok(func) => Ok(func),
            Err(err) => Err(Status::error(Self::get_load_dll_err_msg(&err))),
        }
    };
}

macro_rules! get_and_into_raw {
    ($name:expr, $lib:expr, $fn:ty) => {{
        let symbol: Result<libloading::Symbol<'_, $fn>, Status> = get_func!($name, $lib);
        if let Ok(symbol) = symbol {
            let symbol: Symbol<$fn> = symbol.into_raw();
            Some(symbol)
        } else {
            None
        }
    }};
}

unsafe impl Send for DllService {}

unsafe impl Sync for DllService {}

impl DllService {
    fn get_load_dll_err_msg(err: &libloading::Error) -> String {
        match err {
            libloading::Error::DlOpen { desc } => format!("{:?}", desc),
            libloading::Error::DlOpenUnknown => "加载失败，未知错误！".to_string(),
            libloading::Error::DlSym { desc } => format!("{:?}", desc),
            libloading::Error::DlSymUnknown => "获取函数失败，未知错误".to_string(),
            libloading::Error::DlClose { desc } => format!("{:?}", desc),
            libloading::Error::DlCloseUnknown => "卸载错误，未知错误".to_string(),
            libloading::Error::LoadLibraryExW { source } => format!("{:?}", source),
            libloading::Error::LoadLibraryExWUnknown => "加载失败，未知错误！".to_string(),
            libloading::Error::GetModuleHandleExW { source } => format!("{:?}", source),
            libloading::Error::GetModuleHandleExWUnknown => "获取句柄错误，未知错误！".to_string(),
            libloading::Error::GetProcAddress { source } => format!("{:?}", source),
            libloading::Error::GetProcAddressUnknown => "获取函数失败，未知错误".to_string(),
            libloading::Error::FreeLibrary { source } => format!("{:?}", source),
            libloading::Error::FreeLibraryUnknown => "卸载错误，未知错误".to_string(),
            libloading::Error::CreateCString { source } => format!("{:?}", source),
            libloading::Error::CreateCStringWithTrailing { source } => format!("{:?}", source),
            _ => "未知错误".to_string(),
        }
    }
    //
    pub fn new(dxc_path: &str) -> Result<Box<Self>, Status> {
        unsafe {
            let lib = match libloading::Library::new(dxc_path) {
                Ok(lib) => lib,
                Err(err) => return Err(Status::error(Self::get_load_dll_err_msg(&err))),
            };

            let init_api = get_and_into_raw!(b"init", lib, InitApi);

            let finalize_api = get_and_into_raw!(b"finalize", lib, FinalizeApi);

            let dispatch_message_api =
                get_and_into_raw!(b"dispatch_message", lib, DispatchMessageApi);

            let result_in_api = get_and_into_raw!(b"result_in", lib, ResultInApi);

            let inject_send_message_api =
                get_and_into_raw!(b"inject_send_message", lib, InjectSendMessageApi);

            let inject_has_service_api =
                get_and_into_raw!(b"inject_has_service", lib, InjectHasServiceApi);

            let inject_get_service_id_api =
                get_and_into_raw!(b"inject_get_service_id", lib, InjectGetServiceIdApi);

            // let inject_sleep_api = get_and_into_raw!(b"inject_sleep", lib, InjectSleepApi);

            let inject_response_api =
                get_and_into_raw!(b"inject_response_func", lib, InjectResponseApi);

            let inject_event_response_api =
                get_and_into_raw!(b"inject_event_response_func", lib, InjectEventResponseApi);

            let inject_gen_id_api = get_and_into_raw!(b"inject_gen_id", lib, InjectGenIdApi);

            let inject_offset_whole_seconds_api = get_and_into_raw!(
                b"inject_offset_whole_seconds",
                lib,
                InjectOffsetWholeSecondsApi
            );

            let inject_log_output_api =
                get_and_into_raw!(b"inject_log_output", lib, InjectLogOutputApi);

            //
            let inject_load_service_api =
                get_and_into_raw!(b"inject_load_service", lib, InjectLoadServiceApi);

            let inject_set_channel_xrpc_port_api = get_and_into_raw!(
                b"inject_set_channel_xrpc_port",
                lib,
                InjectSetChannelXRPCPortApi
            );

            let inject_unload_service_api =
                get_and_into_raw!(b"inject_unload_service", lib, InjectUnLoadServiceApi);

            let inject_build_channel_api =
                get_and_into_raw!(b"inject_build_channel", lib, InjectBuildChannelApi);

            let inject_close_channel_api =
                get_and_into_raw!(b"inject_close_channel", lib, InjectCloseChannelApi);

            let inject_get_all_local_service_api = get_and_into_raw!(
                b"inject_get_all_local_service",
                lib,
                InjectGetAllLocalServiceApi
            );

            let inject_add_channel_id_to_remote_services_api = get_and_into_raw!(
                b"inject_add_channel_id_to_remote_services",
                lib,
                InjectAdChannelIdToRemoteServicesApi
            );

            let inject_remove_remote_services_all_channel_id_api = get_and_into_raw!(
                b"inject_remove_remote_services_all_channel_id",
                lib,
                InjectRemoveRemoteServiceAllChannelIdApi
            );

            let inject_get_self_conn_id_api =
                get_and_into_raw!(b"inject_get_self_conn_id", lib, InjectGetSelfConnIdApi);

            let inject_get_all_conn_id_api =
                get_and_into_raw!(b"inject_get_all_conn_id", lib, InjectGetAllConnIdApi);

            let inject_get_channel_id_by_conn_id_api = get_and_into_raw!(
                b"inject_get_channel_id_by_conn_id",
                lib,
                InjectGetChannelIdByConnIdApi
            );

            //
            let inject_subscribe_api =
                get_and_into_raw!(b"inject_subscribe", lib, InjectSubscribeApi);
            //
            let inject_unsubscribe_api =
                get_and_into_raw!(b"inject_unsubscribe", lib, InjectUnSubscribeApi);

            let inject_get_node_private_key_api = get_and_into_raw!(
                b"inject_get_node_private_key",
                lib,
                InjectGetNodePrivateKeyApi
            );

            //
            let inject_get_all_installed_service_api = get_and_into_raw!(
                b"inject_get_all_installed_service",
                lib,
                InjectGetAllInstalledServiceApi
            );

            let inject_get_service_detail_api =
                get_and_into_raw!(b"inject_get_service_detail", lib, InjectGetServiceDetailApi);

            // === stream ===

            let inject_pulish_message_api =
                get_and_into_raw!(b"inject_pulish_message", lib, InjectPulishMessageApi);

            let inject_xport_subscribe_topic_api = get_and_into_raw!(
                b"inject_xport_subscribe_topic",
                lib,
                InjectXportSubscribeTopicApi
            );

            let inject_xport_cancel_subscribe_topic_api = get_and_into_raw!(
                b"inject_xport_cancel_subscribe_topic",
                lib,
                InjectXportCancelSubscribeTopicApi
            );

            let inject_dxc_subscribe_topic_api = get_and_into_raw!(
                b"inject_dxc_subscribe_topic",
                lib,
                InjectDXCSubscribeTopicApi
            );

            let inject_dxc_cancel_subscribe_topic_api = get_and_into_raw!(
                b"inject_dxc_cancel_subscribe_topic",
                lib,
                InjectDXCCancelSubscribeTopicApi
            );

            let inject_is_self_node_api =
                get_and_into_raw!(b"inject_is_self_node", lib, InjectIsSelfNodeApi);

            // let inject_get_config_api =
            //     get_and_into_raw!(b"inject_get_config", lib, InjectGetConfigApi);
            // //

            // 注入 api

            if let Some(inject_send_message_api) = inject_send_message_api {
                inject_send_message_api(inject_api::xport_api::send_message);
            }

            if let Some(inject_has_service_api) = inject_has_service_api {
                inject_has_service_api(inject_api::xport_api::has_service);
            }
            //
            if let Some(inject_get_service_id_api) = inject_get_service_id_api {
                inject_get_service_id_api(inject_api::xport_api::get_service_id);
            }

            //
            if let Some(inject_gen_id_api) = inject_gen_id_api {
                inject_gen_id_api(inject_api::xport_api::gen_id);
            }

            if let Some(inject_offset_whole_seconds_api) = inject_offset_whole_seconds_api {
                inject_offset_whole_seconds_api(inject_api::xport_api::offset_whole_seconds);
            }

            if let Some(inject_log_output_api) = inject_log_output_api {
                inject_log_output_api(inject_api::xport_api::log_output);
            }

            if let Some(inject_response_api) = inject_response_api {
                inject_response_api(inject_api::xport_api::response);
            }

            if let Some(inject_event_response_api) = inject_event_response_api {
                inject_event_response_api(inject_api::event_api::event_response);
            }

            if let Some(inject_load_service_api) = inject_load_service_api {
                inject_load_service_api(inject_api::xport_api::load_service);
            }

            if let Some(inject_unload_service_api) = inject_unload_service_api {
                inject_unload_service_api(inject_api::xport_api::unload_service);
            }

            if let Some(inject_build_channel_api) = inject_build_channel_api {
                inject_build_channel_api(inject_api::xport_api::build_channel);
            }

            if let Some(inject_close_channel_api) = inject_close_channel_api {
                inject_close_channel_api(inject_api::xport_api::close_channel);
            }

            if let Some(inject_get_all_local_service_api) = inject_get_all_local_service_api {
                inject_get_all_local_service_api(inject_api::xport_api::get_all_local_service);
            }

            if let Some(inject_add_channel_id_to_remote_services_api) =
                inject_add_channel_id_to_remote_services_api
            {
                inject_add_channel_id_to_remote_services_api(
                    inject_api::xport_api::add_channel_id_to_remote_services,
                );
            }

            if let Some(inject_remove_remote_services_all_channel_id_api) =
                inject_remove_remote_services_all_channel_id_api
            {
                inject_remove_remote_services_all_channel_id_api(
                    inject_api::xport_api::remove_remote_services_all_channel_id,
                );
            }
            if let Some(inject_get_self_conn_id_api) = inject_get_self_conn_id_api {
                inject_get_self_conn_id_api(inject_api::xport_api::get_self_conn_id);
            }

            if let Some(inject_set_channel_xrpc_port_api) = inject_set_channel_xrpc_port_api {
                inject_set_channel_xrpc_port_api(inject_api::xport_api::set_channel_xrpc_port);
            }

            if let Some(inject_get_all_conn_id_api) = inject_get_all_conn_id_api {
                inject_get_all_conn_id_api(inject_api::xport_api::get_all_conn_id);
            }

            if let Some(inject_get_channel_id_by_conn_id_api) = inject_get_channel_id_by_conn_id_api
            {
                inject_get_channel_id_by_conn_id_api(
                    inject_api::xport_api::get_channel_id_by_conn_id,
                );
            }

            if let Some(inject_subscribe_api) = inject_subscribe_api {
                inject_subscribe_api(inject_api::event_api::subscribe);
            }

            if let Some(inject_unsubscribe_api) = inject_unsubscribe_api {
                inject_unsubscribe_api(inject_api::event_api::unsubscribe);
            }

            if let Some(inject_get_node_private_key_api) = inject_get_node_private_key_api {
                inject_get_node_private_key_api(inject_api::xport_api::get_node_private_key);
            }

            if let Some(inject_get_all_installed_service_api) = inject_get_all_installed_service_api
            {
                inject_get_all_installed_service_api(
                    inject_api::xport_api::get_all_installed_service,
                );
            }

            if let Some(inject_get_service_detail_api) = inject_get_service_detail_api {
                inject_get_service_detail_api(inject_api::xport_api::get_service_detail);
            }

            if let Some(inject_is_self_node_api) = inject_is_self_node_api {
                inject_is_self_node_api(inject_api::xport_api::is_self_node);
            }

            // if let Some(inject_get_config_api) = inject_get_config_api {
            //     inject_get_config_api(inject_api::xport_api::get_config);
            // }
            // === stream ===
            if let Some(inject_pulish_message_api) = inject_pulish_message_api {
                inject_pulish_message_api(inject_api::stream_api::publish_message);
            }
            if let Some(inject_xport_subscribe_topic_api) = inject_xport_subscribe_topic_api {
                inject_xport_subscribe_topic_api(inject_api::stream_api::xport_subscribe_topic);
            }
            if let Some(inject_xport_cancel_subscribe_topic_api) =
                inject_xport_cancel_subscribe_topic_api
            {
                inject_xport_cancel_subscribe_topic_api(
                    inject_api::stream_api::xport_cancel_subscribe_topic,
                );
            }
            if let Some(inject_dxc_subscribe_topic_api) = inject_dxc_subscribe_topic_api {
                inject_dxc_subscribe_topic_api(inject_api::stream_api::dxc_subscribe_topic);
            }
            if let Some(inject_dxc_cancel_subscribe_topic_api) =
                inject_dxc_cancel_subscribe_topic_api
            {
                inject_dxc_cancel_subscribe_topic_api(
                    inject_api::stream_api::dxc_cancel_subscribe_topic,
                );
            }

            //
            let init_api = init_api.unwrap();
            let finalize_api = finalize_api.unwrap();
            let dispatch_message_api = dispatch_message_api.unwrap();
            let result_in_api = result_in_api.unwrap();

            let api = Box::new(DllService {
                _lib: lib,
                init_api,
                finalize_api,
                dispatch_message_api,
                result_in_api,
            });
            Ok(api)
        }
    }
}

#[async_trait]
impl ServiceIns for DllService {
    // 初始化
    fn init(&self, service_id: i64, config: &str, log_level: i32) {
        unsafe {
            (self.init_api)(service_id, config.as_ptr(), config.len() as u32, log_level);
        }
    }

    // 分发消息
    fn dispatch_message(&self, msg: &Vec<u8>) {
        unsafe {
            (self.dispatch_message_api)(msg.as_ptr(), msg.len() as u32);
        }
    }
    // 消息进来
    fn result_in(&self, request_id: i64, msg: &Vec<u8>) {
        // 要判断是否有等待的消息
        unsafe {
            (self.result_in_api)(request_id, msg.as_ptr(), msg.len() as u32);
        }
    }
    //

    /**
     * 关闭后就不能用了
     */
    fn finalize(&self) {
        unsafe {
            (self.finalize_api)();
        }
    }
}
