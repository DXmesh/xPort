use std::{
    collections::{HashMap, LinkedList},
    sync::{atomic::AtomicI64, Arc, Mutex},
};

use super::{service::Service, timeout_checker::TimeoutChecker};
use crate::{
    airport::channel::SELF_CHANNEL,
    application::{get_runtime, Application},
    protocol::pack_message_in_local_service,
};
use async_trait::async_trait;
use semver::Version;
use tokio::sync::mpsc::{self, UnboundedReceiver, UnboundedSender};
use tracing::{info, warn};
use x_common_lib::{
    base::{id_generator, status::Status},
    protocol::{to_dxc_protocol_msg, MsgType, XID},
    service::sys_service_api::ServiceKey,
    utils::time_utils,
};

pub struct OuterService {
    /**
     *
     */
    dxc_name: String,
    /**
     *
     */
    dxc_version: Version,
    /**
     *
     */
    service_id: AtomicI64,
    /**
     * 上线时间
     */
    online_time: i64,
    /**
     * 接收消息的 receiver
     */
    message_receiver: tokio::sync::Mutex<UnboundedReceiver<bool>>,
    /**
     *
     */
    message_sender: UnboundedSender<bool>,
    /**
     *
     */
    resp_message_list: Mutex<LinkedList<XID>>,
    /**
     *
     */
    stream_message_list: Mutex<LinkedList<XID>>,
    /**
     * 超时检测器
     */
    timeout_checker: Arc<Box<TimeoutChecker>>,

    request_handle_map: Mutex<HashMap<i64, Box<dyn FnOnce(Vec<u8>) + Send + Sync>>>,
    /**
     *
     */
    message_handler: Box<dyn Fn(Vec<u8>) + Send + Sync>,
    /**
     * 消息返回值的处理函数
     */
    return_message_sender_map: Mutex<HashMap<i64, tokio::sync::oneshot::Sender<Vec<u8>>>>,
}

impl OuterService {
    pub fn new(
        dxc_name: String,
        dxc_version: Version,
        message_handler: Box<dyn Fn(Vec<u8>) + Send + Sync>,
    ) -> Box<OuterService> {
        let (sender, receiver) = mpsc::unbounded_channel::<bool>();
        Box::new(OuterService {
            dxc_name,
            service_id: AtomicI64::new(0),
            online_time: time_utils::cur_timestamp(),
            dxc_version,
            message_receiver: tokio::sync::Mutex::new(receiver),
            message_sender: sender,
            resp_message_list: Mutex::new(LinkedList::new()),
            stream_message_list: Mutex::new(LinkedList::new()),
            timeout_checker: Arc::new(Box::new(TimeoutChecker::new(None))),
            return_message_sender_map: Mutex::new(HashMap::new()),
            message_handler,
            request_handle_map: Mutex::new(HashMap::new()),
        })
    }

    pub fn set_dxc_id(&self, dxc_id: i64) {
        self.service_id.store(
            id_generator::gen_service_id(dxc_id),
            std::sync::atomic::Ordering::SeqCst,
        );
    }

    pub fn dxc_name(&self) -> String {
        self.dxc_name.clone()
    }

    fn get_xid(&self) -> Option<(XID, MsgType)> {
        let xid = {
            let mut message_list = self.resp_message_list.lock().unwrap();
            message_list.pop_front()
        };

        if let Some(xid) = xid {
            return Some((xid, MsgType::RspNormal));
        }

        let xid = {
            let mut message_list = self.stream_message_list.lock().unwrap();
            message_list.pop_front()
        };

        if let Some(xid) = xid {
            return Some((xid, MsgType::Stream));
        }
        //
        None
    }

    /**
     * 添加返回消息的者
     */

    #[allow(dead_code)]
    fn add_return_handler(&self, request_id: i64, sender: tokio::sync::oneshot::Sender<Vec<u8>>) {
        let mut return_message_sender_map = self.return_message_sender_map.lock().unwrap();

        return_message_sender_map.insert(request_id, sender);
    }
    //
    pub fn hanlde_return_message(&self, request_id: i64, buffer: Vec<u8>) {
        let sender = {
            let mut return_message_sender_map = self.return_message_sender_map.lock().unwrap();

            return_message_sender_map.remove(&request_id)
        };
        if sender.is_none() {
            return;
        }

        let sender = sender.unwrap();

        let result = sender.send(buffer);

        if let Err(err) = result {
            warn!("通知消息：{} 返回失败:{:?}", request_id, err)
        }
    }

    async fn handle_message(self: Arc<Box<Self>>) {
        let mut message_receiver = self.message_receiver.lock().await;

        while let Some(is_handle) = message_receiver.recv().await {
            if !is_handle {
                break;
            }
            let xid = self.get_xid();

            // 处理消息
            if xid.is_none() {
                continue;
            }
            let (xid, msg_type) = xid.unwrap();

            let airport = Application::get_airport();

            let message_manager = airport.get_message_manager();

            let message = if msg_type == MsgType::RspNormal {
                message_manager.get_resp_msg(&xid).await
            } else if msg_type == MsgType::Stream {
                message_manager.get_stream_message(&xid)
            } else {
                None
            };

            if message.is_none() {
                info!("获取不到消息：{:?} 。。。", xid);
                continue;
            }
            if msg_type == MsgType::RspNormal {
                message_manager.delete_resp_message(xid).await;
            }

            let clone_self = self.clone();

            get_runtime().spawn(async move {
                let (message, channel_id, from_addr) = message.unwrap();
                //
                let dxc_msg = to_dxc_protocol_msg(channel_id, from_addr, message.as_slice());
                //
                if dxc_msg.is_none() {
                    warn!("无法识别协议！");
                    return;
                }
                let (dxc_msg, _) = dxc_msg.unwrap();
                //
                if msg_type == MsgType::RspNormal {
                    let request_handler = {
                        let mut request_handle_map = clone_self.request_handle_map.lock().unwrap();
                        request_handle_map.remove(&xid.request_id)
                    };

                    if let Some(request_handler) = request_handler {
                        clone_self.timeout_checker.remove_message(xid.request_id);
                        (request_handler)(dxc_msg);
                    }
                } else if msg_type == MsgType::Stream {
                    //
                    (clone_self.message_handler)(dxc_msg);
                    message_manager.deref_stream_message(&xid);
                }
            });
        }
        message_receiver.close();
        //
    }

    /**
     * 添加等待回复的请求,用于判断是否超时
     */
    pub fn add_waitting_request_id(&self, request_id: i64) {
        self.timeout_checker.add_waitting_request_id(request_id);
    }

    pub fn start(self: &Arc<Box<Self>>) {
        let clone_self = self.clone();

        get_runtime().spawn(async move {
            clone_self.handle_message().await;
        });

        let timeout_checker = self.timeout_checker.clone();
        let service_key = self.get_service_key();

        get_runtime().spawn(async move {
            timeout_checker
                .check_timeout_message(service_key, false)
                .await;
        });
    }

    pub fn add_request_handler(
        &self,
        request_id: i64,
        handler: Box<dyn FnOnce(Vec<u8>) + Send + Sync>,
    ) {
        {
            let mut handler_map = self.request_handle_map.lock().unwrap();
            handler_map.insert(request_id, handler);
        }
        //
        self.timeout_checker.add_waitting_request_id(request_id);
    }

    pub async fn send_message(
        &self,
        mut receiver_service_key: ServiceKey,
        receive_channel_id: i64,
        buffer: Vec<u8>,
    ) -> Result<Vec<u8>, Status> {
        //
        let request_id = id_generator::gen_id();
        let airport = Application::get_airport();
        //
        let dxc_manager = airport.get_dxc_manager();
        //
        let (md5, receive_channel_id) = dxc_manager
            .get_service_md5_and_channel_id(receive_channel_id, &receiver_service_key)
            .await?;
        receiver_service_key.dxc_md5 = md5;
        let sender_service_key = self.get_service_key();
        let msg_buffer = pack_message_in_local_service(
            request_id,
            &sender_service_key,
            &receiver_service_key,
            MsgType::Normal,
            &buffer,
        );

        let (sender, receiver) = tokio::sync::oneshot::channel::<Vec<u8>>();

        self.add_request_handler(
            request_id,
            Box::new(move |message| {
                let _ = sender.send(message);
            }),
        );
        airport
            .message_in(SELF_CHANNEL, receive_channel_id, msg_buffer)
            .await;

        let resp_msg = receiver.await;

        match resp_msg {
            Ok(resp_msg) => Ok(resp_msg),
            Err(err) => Err(Status::error(err.to_string())),
        }
    }
}

impl Drop for OuterService {
    fn drop(&mut self) {
        {
            let mut stream_message_list = self.stream_message_list.lock().unwrap();
            let airport = Application::get_airport();
            let message_manager = airport.get_message_manager();
            for xid in stream_message_list.iter() {
                message_manager.deref_stream_message(xid);
            }
            stream_message_list.clear();
        }

        //
        info!("卸载 outer_service :{:?}", self.get_service_key());
    }
}

#[async_trait]
impl Service for OuterService {
    fn id(&self) -> i64 {
        self.service_id.load(std::sync::atomic::Ordering::Relaxed)
    }

    fn version(&self) -> Version {
        self.dxc_version.clone()
    }

    async fn init(self: Arc<Box<Self>>, _config: &str) -> Result<(), Status> {
        Ok(())
    }

    async fn stop(&self) {
        //
        self.timeout_checker.stop();
        let _ = self.message_sender.send(false);
        self.message_sender.closed().await;
    }

    fn dispatch_message_with_type(&self, msg_type: MsgType, xid: XID) {
        match msg_type {
            MsgType::RspNormal => {
                self.timeout_checker.remove_message(xid.request_id);
                let mut msg_list = self.resp_message_list.lock().unwrap();
                msg_list.push_back(xid)
            }
            MsgType::Stream => {
                let mut msg_list = self.stream_message_list.lock().unwrap();
                msg_list.push_back(xid)
            }
            _ => {}
        };

        let result = self.message_sender.send(true);

        if let Err(_err) = result {
            warn!(
                "dxc:{}  version:{} 接收消息出错！",
                self.dxc_name, self.dxc_version
            )
        }
    }

    fn finalize(&self) {
        //
    }

    fn get_service_key(&self) -> ServiceKey {
        ServiceKey::new(
            self.dxc_name.clone(),
            self.dxc_version.to_string(),
            String::default(),
        )
    }

    fn oline_time(&self) -> i64 {
        self.online_time
    }
}
