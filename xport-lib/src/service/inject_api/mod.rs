use std::sync::Arc;

use crate::{
    airport::channel::SELF_CHANNEL,
    application::{get_runtime, Application},
};
use bytes::Bytes;
use lazy_static::lazy_static;
use tracing::error;
use x_common_lib::{
    base::status::Status,
    protocol::{protocol_v1::ProtocolV1Writer, MsgType, XID},
    serial::request_message::RequestMessage,
    service::sys_service_api::ServiceKey,
};

use super::{local_service::LocalService, service::Service};

pub mod event_api;
pub mod stream_api;
pub mod xport_api;

lazy_static! {
    static ref XPORT_SERVICE_KEY: ServiceKey =
        ServiceKey::new("xPort".into(), "0.0.1".into(), String::default());
}

async fn get_local_service(
    service_id: i64,
    request_id: i64,
    want_system: bool,
) -> Option<Arc<Box<LocalService>>> {
    let airport = Application::get_airport();
    let dxc_manager = airport.get_dxc_manager();
    let local_service = dxc_manager.get_local_service_by_id(service_id).await;
    if local_service.is_none() {
        error!("调用的服务已经下线!");
        return None;
    }
    let local_service = local_service.unwrap();
    if want_system {
        if !local_service.is_system() {
            pack_and_return_error(
                request_id,
                local_service.get_service_key(),
                Status::error("无权限调用!".into()),
            );
            return None;
        }
    }
    Some(local_service)
}

pub fn pack_and_return_message<T>(request_id: i64, receiver_service_key: ServiceKey, message: T)
where
    T: RequestMessage,
{
    let status = Status::default();

    let mut message_size = status.compute_size_with_tag_and_len() as u32;

    message_size += message.compute_size_with_tag_and_len() as u32;

    let mut writer = ProtocolV1Writer::new(
        message_size,
        request_id,
        &XPORT_SERVICE_KEY,
        &receiver_service_key,
    );

    writer.write_msg_type(MsgType::RspNormal);
    writer.write_conn_id(0);
    let msg_body_buffer = writer.msg_body_buffer();
    let mut os = protobuf::CodedOutputStream::bytes(msg_body_buffer);
    status.serial_with_tag_and_len(&mut os);
    message.serial_with_tag_and_len(&mut os);
    drop(os);
    get_runtime().spawn(async move {
        let airport = Application::get_airport();
        airport
            .message_in(SELF_CHANNEL, SELF_CHANNEL, writer.msg_buffer)
            .await;
    });
}

pub fn pack_and_return_bool(request_id: i64, receiver_service_key: ServiceKey, value: bool) {
    let bool_size = 1 + 1;
    let mut writer = ProtocolV1Writer::new(
        bool_size,
        request_id,
        &XPORT_SERVICE_KEY,
        &receiver_service_key,
    );
    //
    writer.write_msg_type(MsgType::RspNormal);
    //
    writer.write_conn_id(0);
    //
    let msg_body_buffer = writer.msg_body_buffer();
    //
    let mut os = protobuf::CodedOutputStream::bytes(msg_body_buffer);
    //
    os.write_bool(1, value).unwrap();
    //
    drop(os);
    //
    get_runtime().spawn(async move {
        let airport = Application::get_airport();
        airport
            .message_in(SELF_CHANNEL, SELF_CHANNEL, writer.msg_buffer)
            .await;
    });
}

pub fn pack_and_return_error(request_id: i64, receiver_service_key: ServiceKey, status: Status) {
    pack_and_return_status(request_id, receiver_service_key, status);
}

pub fn pack_and_return_success(request_id: i64, receiver_service_key: ServiceKey) {
    pack_and_return_status(request_id, receiver_service_key, Status::default());
}

pub fn pack_and_return_status(request_id: i64, receiver_service_key: ServiceKey, status: Status) {
    let status_size = status.compute_size_with_tag_and_len() as u32;
    let mut writer = ProtocolV1Writer::new(
        status_size,
        request_id,
        &XPORT_SERVICE_KEY,
        &receiver_service_key,
    );
    writer.write_msg_type(MsgType::RspNormal);
    writer.write_conn_id(0);

    let msg_body_buffer = writer.msg_body_buffer();
    let mut os = protobuf::CodedOutputStream::bytes(msg_body_buffer);
    status.serial_with_tag_and_len(&mut os);
    drop(os);
    get_runtime().spawn(async move {
        let airport = Application::get_airport();
        airport
            .message_in(SELF_CHANNEL, SELF_CHANNEL, writer.msg_buffer)
            .await;
    });
}

pub fn pack_and_return_status_to_airport(
    request_id: i64,
    receiver_service_key:ServiceKey,
    status: Status,
) {

    let status_size = status.compute_size_with_tag_and_len() as u32;
    let mut writer = ProtocolV1Writer::new(
        status_size,
        request_id,
        &XPORT_SERVICE_KEY,
        &receiver_service_key,
    );
    writer.write_msg_type(MsgType::RspNormal);
    writer.write_conn_id(0);

    let msg_body_buffer = writer.msg_body_buffer();
    let mut os = protobuf::CodedOutputStream::bytes(msg_body_buffer);
    status.serial_with_tag_and_len(&mut os);
    drop(os);

    get_runtime().spawn(async move {
        let xid = XID::new(SELF_CHANNEL, request_id);
        let airport = Application::get_airport();
        airport.response_message(xid, writer.msg_buffer).await;
    });
}

#[allow(dead_code)]
fn pack_and_return_empty(request_id: i64, receiver_service_key: ServiceKey) {
    let mut writer =
        ProtocolV1Writer::new(0, request_id, &XPORT_SERVICE_KEY, &receiver_service_key);
    writer.write_msg_type(MsgType::RspNormal);
    writer.write_conn_id(0);

    get_runtime().spawn(async move {
        let airport = Application::get_airport();
        airport
            .message_in(SELF_CHANNEL, SELF_CHANNEL, writer.msg_buffer)
            .await;
    });
}

#[allow(dead_code)]
fn pack_and_return_status_and_bytes(
    request_id: i64,
    receiver_service_key: ServiceKey,
    status: Status,
    bytes: Bytes,
) {
    let status_size = status.compute_size_with_tag_and_len() as u32 + bytes.len() as u32;
    let mut writer = ProtocolV1Writer::new(
        status_size,
        request_id,
        &XPORT_SERVICE_KEY,
        &receiver_service_key,
    );
    writer.write_msg_type(MsgType::RspNormal);

    writer.write_conn_id(0);

    let msg_body_buffer = writer.msg_body_buffer();
    let mut os = protobuf::CodedOutputStream::bytes(msg_body_buffer);
    status.serial_with_tag_and_len(&mut os);

    let slice_bytes = bytes.as_ref();
    os.write_raw_bytes(slice_bytes).unwrap();

    drop(os);
    let runtime = get_runtime();

    runtime.spawn(async move {
        let airport = Application::get_airport();
        airport
            .message_in(SELF_CHANNEL, SELF_CHANNEL, writer.msg_buffer)
            .await;
    });
}

//
