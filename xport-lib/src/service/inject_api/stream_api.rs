use crate::application::get_runtime;
use crate::{application::Application, service::service::Service};
use protobuf::CodedInputStream;
use tracing::info;
use x_common_lib::protocol::protocol_ipc::ProtocolIPCReader;
use x_common_lib::service::sys_service_api::{PublishInfo, ServiceKey, TopicKey};
use x_common_lib::{
    base::status::Status, serial::request_message::RequestMessage,
    service::sys_service_api::SubscribeTopicInfo,
};

use super::{get_local_service, pack_and_return_status};

// 发布消息
pub extern "C" fn publish_message(info: *const u8, info_len: u32, msg: *const u8, msg_len: u32) {
    //
    let slice = unsafe { std::slice::from_raw_parts(info, info_len as usize) };
    let mut publish_info = PublishInfo::default();
    //
    publish_info.parse_from_bytes(slice).unwrap();
    //
    let signer = Application::get_signer();

    let message = if signer.has_private_key() {
        let message = unsafe { std::slice::from_raw_parts(msg, msg_len as usize) };
        signer.sign(message).unwrap()
    } else {
        unsafe { std::slice::from_raw_parts(msg, msg_len as usize).to_owned() }
    };
    let airport = Application::get_airport();
    airport
        .get_stream()
        .publish_message(publish_info, false, message);
}
//
pub fn ipc_publish_message(service_id: i64, message: Vec<u8>) {
    let reader = ProtocolIPCReader::new(&message);
    let msg_body = reader.msg_body();
    let mut publish_info = PublishInfo::default();
    let mut is = CodedInputStream::from_bytes(msg_body);
    let ret = publish_info.parse_from_input_stream_with_tag_and_len(&mut is);
    if let Err(err) = ret {
        info!("推送消息失败：{}", err.err_msg);
        return;
    }
    publish_info.service_id = service_id;
    let publish_msg = &msg_body[is.pos() as usize..];
    let signer = Application::get_signer();

    let message = if signer.has_private_key() {
        signer.sign(publish_msg).unwrap()
    } else {
        publish_msg.to_owned()
    };
    let airport = Application::get_airport();
    airport
        .get_stream()
        .publish_message(publish_info, true, message);
    // publish_message_inner(publish_info, true, message);
}

// fn publish_message_inner(publish_info: PublishInfo, is_ipc: bool, message: Vec<u8>) {
//     //
//     let req_id = id_generator::gen_id();
//     //
//     let airport = Application::get_airport();
//     //
//     let dxc_manager = airport.get_dxc_manager();

//     let sender_service_key = get_runtime().block_on(async {
//         if is_ipc {
//             let ipc_service = dxc_manager
//                 .get_ipc_service_by_id(publish_info.service_id)
//                 .await;
//             //
//             if ipc_service.is_none() {
//                 return None;
//             }
//             Some(ipc_service.unwrap().get_service_key())
//         } else {
//             let local_service = dxc_manager
//                 .get_local_service_by_id(publish_info.service_id)
//                 .await;
//             //
//             if local_service.is_none() {
//                 return None;
//             }
//             Some(local_service.unwrap().get_service_key())
//         }
//     });
//     if sender_service_key.is_none() {
//         error!("调用的服务已经下线!");
//         return;
//     }

//     let sender_service_key = sender_service_key.unwrap();

//     let signer = Application::get_signer();

//     let has_private_key = signer.has_private_key();
//     //

//     let mut v3_writer = ProtocolV3Writer::new(message.len() as u32, req_id, &sender_service_key);
//     v3_writer.write_msg_body(&message, has_private_key);
//     let v3_msg = Arc::new(v3_writer.msg_buffer);
//     let stream = airport.get_stream();
//     let mut topic_key = TopicKey::default();
//     topic_key.topic = publish_info.topic;
//     topic_key.tag = publish_info.tag;
//     stream.publish_message(publish_info.fix_conn_id, &topic_key, v3_msg);
// }

/**
 *
 */
pub extern "C" fn xport_subscribe_topic(
    service_id: i64,
    request_id: i64,
    param: *const u8,
    len: u32,
) {
    let slice = unsafe { std::slice::from_raw_parts(param, len as usize) };
    let mut subscribe_info = SubscribeTopicInfo::default();
    subscribe_info.parse_from_bytes(slice).unwrap();
    let runtime = get_runtime();
    runtime.spawn(async move {
        //
        let local_service = get_local_service(service_id, request_id, false).await;
        if local_service.is_none() {
            return;
        }
        let local_service = local_service.unwrap();
        async_xport_subscribe_topic(local_service.get_service_key(), subscribe_info, request_id)
            .await;
    });
}

pub async fn async_xport_subscribe_topic(
    service_key: ServiceKey,
    subscribe_info: SubscribeTopicInfo,
    request_id: i64,
) {
    let airport = Application::get_airport();
    let stream = airport.get_stream();
    if subscribe_info.tag.is_empty() {
        let mut topic_key = TopicKey::default();
        topic_key.topic = subscribe_info.topic.clone();
        stream.xport_subscribe_topic(&topic_key, subscribe_info.conn_id);
    } else {
        for tag in subscribe_info.tag.into_iter() {
            let mut topic_key = TopicKey::default();
            topic_key.topic = subscribe_info.topic.clone();
            topic_key.tag = tag;
            stream.xport_subscribe_topic(&topic_key, subscribe_info.conn_id);
        }
    }
    //
    pack_and_return_status(request_id, service_key, Status::default())
}

/**
 *
 */
pub extern "C" fn xport_cancel_subscribe_topic(
    service_id: i64,
    request_id: i64,
    param: *const u8,
    len: u32,
) {
    let slice = unsafe { std::slice::from_raw_parts(param, len as usize) };
    let mut subscribe_info = SubscribeTopicInfo::default();
    subscribe_info.parse_from_bytes(slice).unwrap();
    let runtime = get_runtime();
    runtime.spawn(async move {
        //
        let local_service = get_local_service(service_id, request_id, false).await;
        if local_service.is_none() {
            return;
        }
        let local_service = local_service.unwrap();

        async_xport_cancel_subscribe_topic(
            local_service.get_service_key(),
            subscribe_info,
            request_id,
        )
        .await;
    });
}

pub async fn async_xport_cancel_subscribe_topic(
    service_key: ServiceKey,
    subscribe_info: SubscribeTopicInfo,
    request_id: i64,
) {
    let airport = Application::get_airport();
    let stream = airport.get_stream();
    for tag in subscribe_info.tag.into_iter() {
        let mut topic_key = TopicKey::default();
        topic_key.topic = subscribe_info.topic.clone();
        topic_key.tag = tag;

        stream.xport_cancel_subscribe_topic(&topic_key, subscribe_info.conn_id);
    }
    pack_and_return_status(request_id, service_key, Status::default())
}

/**
 * 订阅消息
 */
pub extern "C" fn dxc_subscribe_topic(
    service_id: i64,
    request_id: i64,
    param: *const u8,
    len: u32,
) {
    //
    let slice = unsafe { std::slice::from_raw_parts(param, len as usize) };
    let mut subscribe_info = SubscribeTopicInfo::default();
    subscribe_info.parse_from_bytes(slice).unwrap();
    let runtime = get_runtime();
    runtime.spawn(async move {
        //
        let local_service = get_local_service(service_id, request_id, false).await;
        if local_service.is_none() {
            return;
        }
        let local_service = local_service.unwrap();
        //
        async_dxc_subscribe_topic(local_service.get_service_key(), subscribe_info, request_id)
            .await;
    });
}

pub async fn async_dxc_subscribe_topic(
    service_key: ServiceKey,
    subscribe_info: SubscribeTopicInfo,
    request_id: i64,
) {
    let airport = Application::get_airport();
    let stream = airport.get_stream();
    if subscribe_info.tag.is_empty() {
        let mut topic_key = TopicKey::default();
        topic_key.topic = subscribe_info.topic.clone();
        stream.dxc_subscribe_topic(
            subscribe_info.conn_id,
            &topic_key,
            subscribe_info.service_id,
        );
    } else {
        for tag in subscribe_info.tag.into_iter() {
            let mut topic_key = TopicKey::default();

            topic_key.topic = subscribe_info.topic.clone();
            topic_key.tag = tag;

            stream.dxc_subscribe_topic(
                subscribe_info.conn_id,
                &topic_key,
                subscribe_info.service_id,
            );
        }
    }
    pack_and_return_status(request_id, service_key, Status::default())
}
/**
 * 去掉订阅消息
 */
pub extern "C" fn dxc_cancel_subscribe_topic(
    service_id: i64,
    request_id: i64,
    param: *const u8,
    len: u32,
) {
    //
    let slice = unsafe { std::slice::from_raw_parts(param, len as usize) };
    let mut subscribe_info = SubscribeTopicInfo::default();
    subscribe_info.parse_from_bytes(slice).unwrap();
    let runtime = get_runtime();
    runtime.spawn(async move {
        //
        let local_service = get_local_service(service_id, request_id, false).await;
        if local_service.is_none() {
            return;
        }
        let local_service = local_service.unwrap();
        //
        async_dxc_cancel_subscribe_topic(
            local_service.get_service_key(),
            subscribe_info,
            request_id,
        )
        .await;
    });
}

pub async fn async_dxc_cancel_subscribe_topic(
    service_key: ServiceKey,
    subscribe_info: SubscribeTopicInfo,
    request_id: i64,
) {
    let airport = Application::get_airport();
    let stream = airport.get_stream();
    for tag in subscribe_info.tag.into_iter() {
        let mut topic_key = TopicKey::default();
        topic_key.topic = subscribe_info.topic.clone();
        topic_key.tag = tag;

        stream.dxc_cancel_subscribe_topic(
            subscribe_info.conn_id,
            &topic_key,
            subscribe_info.service_id,
        );
    }
    pack_and_return_status(request_id, service_key, Status::default())
}
