use std::slice;

use crate::{
    airport::channel::SELF_CHANNEL,
    application::{get_runtime, Application},
    log,
    protocol::pack_message_in_local_service,
    service::service::Service,
};

use x_common_lib::{
    base::{id_generator, status::Status},
    protocol::MsgType,
    serial::request_message::RequestMessage,
    service::sys_service_api::{
        BoolResponse, ChannelId, ConnIds, I64Response, LoadServiceRequest, ServiceInfo,
        ServiceInfos, ServiceKey, StringResponse, UnloadServiceRequest,
    }, utils::time_utils,
};

use semver::Version;
use tracing::{error, info};

use super::{
    get_local_service, pack_and_return_bool, pack_and_return_error, pack_and_return_message,
    pack_and_return_status, pack_and_return_success,
};

pub extern "C" fn get_node_private_key(service_id: i64, request_id: i64) {
    get_runtime().spawn(async move {
        let local_service = get_local_service(service_id, request_id, true).await;
        if local_service.is_none() {
            return;
        }
        let local_service = local_service.unwrap();
        //
        let signer = Application::get_signer();

        let private_key = signer.get_private_key();

        let mut resp = StringResponse::default();

        resp.value = private_key;

        pack_and_return_message(request_id, local_service.get_service_key(), resp);
    });
}

pub extern "C" fn is_self_node(service_id: i64, request_id: i64, conn_id: i64) {
    get_runtime().spawn(async move {
        let local_service = get_local_service(service_id, request_id, true).await;
        if local_service.is_none() {
            return;
        }
        let local_service = local_service.unwrap();

        let airport = Application::get_airport();

        let mut resp = BoolResponse::default();

        resp.value = airport.is_self_conn_id(conn_id);

        pack_and_return_message(request_id, local_service.get_service_key(), resp);
    });
}
/**
 * 加载服务
 */
pub extern "C" fn load_service(service_id: i64, request_id: i64, param: *const u8, len: u32) {
    let slice = unsafe { std::slice::from_raw_parts(param, len as usize) };
    let mut load_service_request = LoadServiceRequest::default();
    load_service_request.parse_from_bytes(slice).unwrap();
    get_runtime().spawn(async move {
        //
        let local_service = get_local_service(service_id, request_id, true).await;
        //
        if local_service.is_none() {
            return;
        }
        //
        let local_service = local_service.unwrap();
        //
        let dxc_version = Version::parse(&load_service_request.dxc_version);
        if let Err(_) = dxc_version {
            pack_and_return_error(
                request_id,
                local_service.get_service_key(),
                Status::error("版本号错误".into()),
            );
            return;
        }
        // 卸载
        //
        let airport = Application::get_airport();
        let dxc_manager = airport.get_dxc_manager();
        let dxc_version = dxc_version.unwrap();
        //
        if dxc_manager
            .has_dxc(&load_service_request.dxc_name, &dxc_version)
            .await
        {
            pack_and_return_status(
                request_id,
                local_service.get_service_key(),
                Status::error(format!(
                    "加载 dxc {} version {} 失败, 服务已经存在!",
                    load_service_request.dxc_name,
                    dxc_version.to_string()
                )),
            );
            return;
        }
        //
        let ret = dxc_manager
            .load_local_or_ipc_service(
                &load_service_request.dxc_name,
                &dxc_version,
                load_service_request.only_build_ipc,
                &load_service_request.config,
                load_service_request.compatible_versions,
                load_service_request.only_in_node,
                load_service_request.timeout
            )
            .await;
        //
        let status = if let Err(status) = ret {
            // 加载失败，则卸载 dxc
            let local_dxc = dxc_manager
                .get_dxc_by_name_and_version(&load_service_request.dxc_name, &dxc_version)
                .await;

            if let Some(local_dxc) = local_dxc {
                local_dxc.remove_local_or_ipc_service().await;
                dxc_manager
                    .remove_dxc(&load_service_request.dxc_name, &dxc_version)
                    .await;
            }

            status
        } else {
            Status::new(0, String::default())
        };
        pack_and_return_status(request_id, local_service.get_service_key(), status);
    });
}
/**
 * 卸载服务
 */
pub extern "C" fn unload_service(service_id: i64, request_id: i64, param: *const u8, len: u32) {
    let slice = unsafe { std::slice::from_raw_parts(param, len as usize) };

    let mut unload_service_request = UnloadServiceRequest::default();

    unload_service_request.parse_from_bytes(slice).unwrap();

    let runtime = get_runtime();

    runtime.spawn(async move {
        let local_service = get_local_service(service_id, request_id, true).await;
        if local_service.is_none() {
            return;
        }

        let local_service = local_service.unwrap();
        let airport = Application::get_airport();
        let dxc_manager = airport.get_dxc_manager();
        let dxc_version = Version::parse(&unload_service_request.dxc_version);
        if dxc_version.is_err() {
            pack_and_return_error(
                request_id,
                local_service.get_service_key(),
                Status::error("版本解析出错!".into()),
            );
            return;
        }
        let dxc_version = dxc_version.unwrap();
        let local_dxc = dxc_manager
            .get_dxc_by_name_and_version(&unload_service_request.dxc_name, &dxc_version)
            .await;

        if local_dxc.is_none() {
            pack_and_return_error(
                request_id,
                local_service.get_service_key(),
                Status::error("服务不存在！".into()),
            );
            return;
        }
        //
        local_dxc.unwrap().remove_local_or_ipc_service().await;
        // 删除 dxc
        dxc_manager
            .remove_dxc(&unload_service_request.dxc_name, &dxc_version)
            .await;

        pack_and_return_success(request_id, local_service.get_service_key());
    });
}

/**
 * 获取本地安装的 服务列表
 */
pub extern "C" fn get_all_installed_service(service_id: i64, request_id: i64) {
    let runtime = get_runtime();
    runtime.spawn(async move {
        let local_service = get_local_service(service_id, request_id, true).await;
        if local_service.is_none() {
            return;
        }
        //
        let local_service = local_service.unwrap();
        //
        let airport = Application::get_airport();
        let dxc_manager = airport.get_dxc_manager();
        //
        let service_infos = dxc_manager.get_all_installed_service().await;
        // 返回结果
        match service_infos {
            Ok(value) => {
                pack_and_return_message(request_id, local_service.get_service_key(), value)
            }
            Err(status) => {
                pack_and_return_status(request_id, local_service.get_service_key(), status)
            }
        }
    });
}

/**
 * 获取本地安装的 服务列表
 */
pub extern "C" fn get_service_detail(service_id: i64, request_id: i64, param: *const u8, len: u32) {
    let slice = unsafe { std::slice::from_raw_parts(param, len as usize) };
    let mut service_key: ServiceKey = ServiceKey::default();
    service_key.parse_from_bytes(slice).unwrap();
    let runtime = get_runtime();
    runtime.spawn(async move {
        let local_service = get_local_service(service_id, request_id, true).await;
        if local_service.is_none() {
            return;
        }
        let local_service = local_service.unwrap();
        //
        let airport = Application::get_airport();

        let dxc_manager = airport.get_dxc_manager();
        let detail_info = dxc_manager.get_service_detail(&service_key).await;
        match detail_info {
            Ok(value) => {
                pack_and_return_message(request_id, local_service.get_service_key(), value)
            }
            Err(status) => {
                pack_and_return_status(request_id, local_service.get_service_key(), status)
            }
        }
    });
}

/**
 * 判断是否有服务
 */
pub extern "C" fn has_service(service_id: i64, request_id: i64, param: *const u8, len: u32) {
    let slice = unsafe { std::slice::from_raw_parts(param, len as usize) };
    let mut target_service_key: ServiceKey = ServiceKey::default();
    target_service_key.parse_from_bytes(slice).unwrap();
    let runtime = get_runtime();
    runtime.spawn(async move {
        let local_service = get_local_service(service_id, request_id, true).await;
        if local_service.is_none() {
            return;
        }
        let local_service = local_service.unwrap();
        //
        let dxc_version = Version::parse(&target_service_key.dxc_version);
        if dxc_version.is_err() {
            pack_and_return_error(
                request_id,
                local_service.get_service_key(),
                Status::error("版本解析出错!".into()),
            );
            return;
        }

        let dxc_version = dxc_version.unwrap();
        let dxc_manager = Application::get_airport().get_dxc_manager();

        let exist = dxc_manager
            .has_dxc(&target_service_key.dxc_name, &dxc_version)
            .await;
        pack_and_return_bool(request_id, local_service.get_service_key(), exist);
    });
}

/**
 * 获取 dxc 的 service_id
 */
pub extern "C" fn get_service_id(service_id: i64, request_id: i64, param: *const u8, len: u32) {
    let slice = unsafe { std::slice::from_raw_parts(param, len as usize) };
    let mut target_service_key: ServiceKey = ServiceKey::default();
    target_service_key.parse_from_bytes(slice).unwrap();
    let runtime = get_runtime();
    runtime.spawn(async move {
        let local_service = get_local_service(service_id, request_id, false).await;
        if local_service.is_none() {
            return;
        }
        let local_service = local_service.unwrap();
        //
        let dxc_version = Version::parse(&target_service_key.dxc_version);
        if let Err(_) = dxc_version {
            pack_and_return_error(
                request_id,
                local_service.get_service_key(),
                Status::error("版本解析出错!".into()),
            );
            return;
        }
        let mut resp = I64Response::default();
        resp.value = -1;

        let airport = Application::get_airport();
        let dxc_version = dxc_version.unwrap();
        let dxc = airport
            .get_dxc_manager()
            .get_dxc_by_name_and_version(&target_service_key.dxc_name, &dxc_version)
            .await;

        if dxc.is_none() {
            pack_and_return_message(request_id, local_service.get_service_key(), resp);
            return;
        }
        //
        let target_local_service = dxc.as_ref().unwrap().get_local_service().await;
        if let Some(target_local_service) = target_local_service {
            resp.value = target_local_service.id();
            pack_and_return_message(request_id, local_service.get_service_key(), resp);
            return;
        }
        let target_ipc_service = dxc.unwrap().get_ipc_service().await;
        if let Some(target_ipc_service) = target_ipc_service {
            let mut resp = I64Response::default();
            resp.value = target_ipc_service.id();
            pack_and_return_message(request_id, local_service.get_service_key(), resp);
            return;
        }
        pack_and_return_message(request_id, local_service.get_service_key(), resp);
    });
}

/**
 * channel_id  指定接收消息 channel
 */
pub extern "C" fn send_message(
    service_id: i64,
    request_id: i64,
    receiver_service_key_ptr: *const u8,
    receiver_service_key_len: u32,
    receive_channel_id: i64,
    buffer: *const u8,
    buffer_size: u32,
) {
    let receiver_service_slice = unsafe {
        slice::from_raw_parts(receiver_service_key_ptr, receiver_service_key_len as usize)
    };
    //
    let mut receiver_service_key = ServiceKey::default();
    receiver_service_key
        .parse_from_bytes(receiver_service_slice)
        .unwrap();

    let message = unsafe { std::slice::from_raw_parts(buffer, buffer_size as usize).to_owned() };
    //
    get_runtime().spawn(async move {
        //
        let airport = Application::get_airport();
        //
        let dxc_manager = airport.get_dxc_manager();
        // 切换了协程，需要再次判断 dxc 是否还存在
        let local_service = dxc_manager.get_local_service_by_id(service_id).await;
        //
        if local_service.is_none() {
            error!("调用的服务已经下线!");
            return;
        }
        let local_service = local_service.unwrap();
        //
        let sender_service_key = local_service.get_service_key();
        // 获取指定节点的服务的 md5
        // 要解决 XComService 找不到的问题
        let ret = dxc_manager
            .get_service_md5_and_channel_id(receive_channel_id, &receiver_service_key)
            .await;
        // 配置了这个参数，没有任何参数的时候，会把所有的消息喜欢发到这个节点

        if let Err(err) = ret {
            return pack_and_return_error(request_id, local_service.get_service_key(), err);
        }

        let (md5, receive_channel_id) = ret.unwrap();
        receiver_service_key.dxc_md5 = md5;

        let msg_buffer = pack_message_in_local_service(
            request_id,
            &sender_service_key,
            &receiver_service_key,
            MsgType::Normal,
            &message,
        );
        // 增加超时等待
        local_service.add_waitting_request_id(request_id);
        // 未指定 channel，或者为本节点
        airport
            .message_in(SELF_CHANNEL, receive_channel_id, msg_buffer)
            .await;
    });
}

/**
 * 创建链接
 */
pub extern "C" fn build_channel(service_id: i64, request_id: i64, conn_id: i64) {
    get_runtime().spawn(async move {
        let local_service = get_local_service(service_id, request_id, true).await;
        if local_service.is_none() {
            return;
        }
        let airport = Application::get_airport();

        let local_service = local_service.unwrap();
        //  执行任务
        let channel_manager = airport.get_channel_manager();
        let session_id = channel_manager.build_xrpc_channel(conn_id).await;
        if session_id.is_none() {
            pack_and_return_error(
                request_id,
                local_service.get_service_key(),
                Status::error("连接失败!".into()),
            );
        } else {
            pack_and_return_success(request_id, local_service.get_service_key());
        }
    });
}

pub extern "C" fn close_channel(service_id: i64, request_id: i64, channel_id: i64) {
    get_runtime().spawn(async move {
        let local_service = get_local_service(service_id, request_id, true).await;
        if local_service.is_none() {
            return;
        }
        let airport = Application::get_airport();

        let local_service = local_service.unwrap();
        //  执行任务
        let channel_manager = airport.get_channel_manager();

        info!("关闭通道...");
        channel_manager.remove_channel(channel_id).await;

        pack_and_return_success(request_id, local_service.get_service_key());
    });
}

/**
 *
 */
pub extern "C" fn get_all_local_service(service_id: i64, request_id: i64, filter_hide_node: bool) {
    get_runtime().spawn(async move {
        let local_service = get_local_service(service_id, request_id, false).await;
        if local_service.is_none() {
            return;
        }
        let local_service = local_service.unwrap();
        let airport = Application::get_airport();
        let dxc_manager = airport.get_dxc_manager();

        let service_infos = dxc_manager
            .get_all_local_service_info(filter_hide_node)
            .await;
        pack_and_return_message(request_id, local_service.get_service_key(), service_infos);
    });
}

pub extern "C" fn add_channel_id_to_remote_services(
    service_id: i64,
    request_id: i64,
    channel_id: i64,
    buffer: *const u8,
    buffer_len: u32,
) {
    let slice = unsafe { std::slice::from_raw_parts(buffer, buffer_len as usize) };
    let mut service_infos = ServiceInfos::default();
    service_infos.parse_from_bytes(slice).unwrap();

    get_runtime().spawn(async move {
        let local_service = get_local_service(service_id, request_id, true).await;
        if local_service.is_none() {
            return;
        }
        //
        let local_service = local_service.unwrap();
        let airport = Application::get_airport();
        let dxc_manager = airport.get_dxc_manager();

        for service_info in service_infos.service_infos {
            let dxc_version = Version::parse(&service_info.dxc_version);

            if let Err(_) = dxc_version {
                pack_and_return_error(
                    request_id,
                    local_service.get_service_key(),
                    Status::error("版本号格式错误！".into()),
                );
                return;
            }
            let dxc_version = dxc_version.unwrap();
            let dxc = dxc_manager
                .add_dxc_and_return(&service_info.dxc_name, &dxc_version)
                .await;

            dxc.add_channel_to_remote_service(
                channel_id,
                service_info.md5,
                service_info.service_id,
                service_info.compatible_versions,
            )
            .await;
        }
        pack_and_return_success(request_id, local_service.get_service_key());
    });
    //
}

pub extern "C" fn set_channel_xrpc_port(
    service_id: i64,
    request_id: i64,
    channel_id: i64,
    port: u32,
) {
    let runtime = get_runtime();
    runtime.spawn(async move {
        let local_service = get_local_service(service_id, request_id, true).await;
        if local_service.is_none() {
            return;
        }
        let local_service = local_service.unwrap();

        let airport = Application::get_airport();

        let channel_manager = airport.get_channel_manager();
        let channel = channel_manager.get_channel(channel_id).await;
        if channel.is_none() {
            pack_and_return_error(
                request_id,
                local_service.get_service_key(),
                Status::error("channel 不存在".into()),
            );
            return;
        }
        //
        let channel = channel.unwrap();
        channel.set_channel_port(port as u16).await;
        let conn_id = channel.conn_id();

        let mut resp = I64Response::default();
        resp.value = conn_id;

        pack_and_return_message(request_id, local_service.get_service_key(), resp);
    });
}

pub extern "C" fn get_self_conn_id(_service_id: i64, _request_id: i64) -> i64 {
    let airport = Application::get_airport();

    airport.xrpc_net.conn_id()
}

pub extern "C" fn get_all_conn_id(service_id: i64, request_id: i64) {
    let runtime = get_runtime();

    runtime.spawn(async move {
        let local_service = get_local_service(service_id, request_id, false).await;

        if local_service.is_none() {
            return;
        }
        let local_service = local_service.unwrap();
        async_get_all_conn_id(local_service.get_service_key(), request_id).await;
    });
}

//
pub async fn async_get_all_conn_id(service_key: ServiceKey, request_id: i64) {
    let airport = Application::get_airport();

    let channel_manager = airport.get_channel_manager();

    let conn_id_vec = channel_manager.get_all_conn_id().await;

    let mut conn_ids = ConnIds::default();

    conn_ids.conn_id = conn_id_vec;

    pack_and_return_message(request_id, service_key, conn_ids);
}
//
pub extern "C" fn get_channel_id_by_conn_id(service_id: i64, request_id: i64, conn_id: i64) {
    let runtime = get_runtime();

    runtime.spawn(async move {
        let local_service = get_local_service(service_id, request_id, false).await;
        if local_service.is_none() {
            return;
        }
        let local_service = local_service.unwrap();
        async_get_channel_id_by_conn_id(local_service.get_service_key(), request_id, conn_id).await;
    });
}
//
pub async fn async_get_channel_id_by_conn_id(
    service_key: ServiceKey,
    request_id: i64,
    conn_id: i64,
) {
    let airport = Application::get_airport();

    let channel_id = if airport.is_self_conn_id(conn_id) {
        Some(SELF_CHANNEL)
    } else {
        let channel_manager = airport.get_channel_manager();
        channel_manager.get_channel_id_by_conn_id(conn_id).await
    };

    let mut channel_id_info = ChannelId::default();

    channel_id_info.channel_id = channel_id;

    pack_and_return_message(request_id, service_key, channel_id_info);
}

pub extern "C" fn remove_remote_services_all_channel_id(
    service_id: i64,
    request_id: i64,
    channel_id: i64,
    buffer: *const u8,
    buffer_len: u32,
) {
    let slice = unsafe { std::slice::from_raw_parts(buffer, buffer_len as usize) };
    let mut service_info = ServiceInfo::default();
    service_info.parse_from_bytes(slice).unwrap();

    let runtime = get_runtime();

    runtime.spawn(async move {
        let local_service = get_local_service(service_id, request_id, true).await;

        if local_service.is_none() {
            return;
        }

        let local_service = local_service.unwrap();

        let dxc_version = Version::parse(&service_info.dxc_version);

        if let Err(_) = dxc_version {
            pack_and_return_error(
                request_id,
                local_service.get_service_key(),
                Status::error("版本号格式错误！".into()),
            );
            return;
        }

        let airport = Application::get_airport();

        let channel_manager = airport.get_channel_manager();

        let conn_id = channel_manager.get_conn_id_by_channel_id(channel_id).await;

        if conn_id.is_none() {
            pack_and_return_success(request_id, local_service.get_service_key());
            return;
        }

        let dxc_version = dxc_version.unwrap();
        //
        let dxc_manager = airport.get_dxc_manager();

        let dxc = dxc_manager
            .get_dxc_by_name_and_version(&service_info.dxc_name, &dxc_version)
            .await;

        if dxc.is_none() {
            pack_and_return_success(request_id, local_service.get_service_key());
            return;
        }

        let remove_service = dxc
            .unwrap()
            .get_remote_service_by_conn_id(conn_id.unwrap())
            .await;

        if remove_service.is_some() {
            remove_service.unwrap().clear_channel();
        }
        pack_and_return_success(request_id, local_service.get_service_key());
    });
}
//

/**
*
*/
pub extern "C" fn gen_id(_service_id: i64) -> i64 {
    id_generator::gen_id()
}

/**
*
*/
pub extern "C" fn offset_whole_seconds() -> i32 {
  time_utils::get_offset_whole_seconds()
}


/**
 * 回复消息
 */
pub extern "C" fn response(service_id: i64, request_id: i64, buffer: *const u8, buffer_len: u32) {
    let slice = unsafe { std::slice::from_raw_parts(buffer, buffer_len as usize) };
    let vec_buffer = slice.to_vec();

    let runtime = get_runtime();

    runtime.spawn(async move {
        let local_service = get_local_service(service_id, request_id, false).await;
        if local_service.is_none() {
            return;
        }

        let local_service = local_service.unwrap();
        local_service.hanlde_return_message(request_id, vec_buffer);
    });
}

/**
 *
 */
pub extern "C" fn log_output(_service_id: i64, log: *const u8, log_len: u32) {
    let slice = unsafe { std::slice::from_raw_parts(log, log_len as usize) };
    let log_vec = slice.to_vec();
    log::append_log(log_vec);
}
