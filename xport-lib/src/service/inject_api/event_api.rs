use x_common_lib::base::dll_api::SubscribeHandler;

use crate::{
    application::{get_runtime, Application},
    event::{add_subscriber, remove_subscriber},
};

pub extern "C" fn subscribe(event_id: u16, pri: u32, handler: SubscribeHandler) -> i64 {
    //
    let runtime = get_runtime();
    let subscribe_id =
        runtime.block_on(async move { add_subscriber(event_id, pri, handler).await });
    subscribe_id.unwrap_or(-1)
}

pub extern "C" fn unsubscribe(event_id: u16, subscribe_id: i64) {
    //
    let runtime = get_runtime();

    runtime.block_on(async move {
        remove_subscriber(event_id, subscribe_id).await;
    });
}

pub extern "C" fn event_response(publish_id: i64, msg: *const u8, len: u32) {
    let event = Application::get_event();
    event.on_resp(publish_id, msg, len);
    //
}
