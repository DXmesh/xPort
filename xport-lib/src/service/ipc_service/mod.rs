pub mod ipc_service;

mod xport_api {
    use std::sync::Arc;
    use x_common_lib::{
        protocol::{protocol_ipc::ProtocolIPCReader, MsgType},
        serial::request_message::RequestMessage,
        service::sys_service_api::ServiceKey,
    };

    use crate::{
        airport::channel::SELF_CHANNEL,
        application::Application,
        protocol::pack_message_in_ipc_service,
        service::inject_api::{self, pack_and_return_error},
    };

    use x_common_lib::serial::read_sfixed64;

    use super::ipc_service::IPCService;

    pub(super) async fn send_message(
        ipc_service: Arc<Box<IPCService>>,
        sender_service_key: ServiceKey,
        msg: Vec<u8>,
    ) {
        let reader = ProtocolIPCReader::new(&msg);
        //
        let request_id = reader.request_id();
        //
        let receive_channel_id = reader.channel_id();
        //
        let msg_body = reader.msg_body();
        //
        let mut receiver_service_key = ServiceKey::default();
        //
        let read_bytes = receiver_service_key
            .parse_from_bytes_return_num(&msg_body)
            .unwrap();
        //
        let airport = Application::get_airport();
        let dxc_manager = airport.get_dxc_manager();

        let ret = dxc_manager
            .get_service_md5_and_channel_id(receive_channel_id, &receiver_service_key)
            .await;


        if let Err(err) = ret {
            return pack_and_return_error(request_id, sender_service_key, err);
        }

        //

        let (md5, receive_channel_id) = ret.unwrap();
        receiver_service_key.dxc_md5 = md5;
        //
        let msg_buffer = pack_message_in_ipc_service(
            request_id,
            &sender_service_key,
            &receiver_service_key,
            MsgType::Normal,
            &msg_body[read_bytes as usize..],
        );
        //
        ipc_service.add_waitting_request_id(request_id);
        //
        let airport = Application::get_airport();

        airport
            .message_in(SELF_CHANNEL, receive_channel_id, msg_buffer)
            .await;
    }

    pub(super) async fn get_all_conn_id(service_key: ServiceKey, msg: Vec<u8>) {
        //
        let reader = ProtocolIPCReader::new(&msg);
        //
        let request_id = reader.request_id();
        //
        inject_api::xport_api::async_get_all_conn_id(service_key, request_id).await;
    }
    //
    pub(super) async fn get_channel_id_by_conn_id(service_key: ServiceKey, msg: Vec<u8>) {
        //
        let reader = ProtocolIPCReader::new(&msg);
        //
        let request_id = reader.request_id();
        //
        let msg_body = reader.msg_body();
        //
        let conn_id = read_sfixed64(msg_body);
        //
        inject_api::xport_api::async_get_channel_id_by_conn_id(service_key, request_id, conn_id)
            .await;
    }
}

mod log_api {
    use crate::log::append_log;
    use x_common_lib::{
        protocol::protocol_ipc::ProtocolIPCReader, service::sys_service_api::ServiceKey,
    };

    pub async fn output(_service_key: ServiceKey, msg: Vec<u8>) {
        let reader = ProtocolIPCReader::new(&msg);

        let msg_body = reader.msg_body().to_vec();

        append_log(msg_body);
    }
}
