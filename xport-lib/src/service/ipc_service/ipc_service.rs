use async_trait::async_trait;
use protobuf::CodedInputStream;
use semver::Version;
use std::collections::HashMap;
use std::process::Child;
use std::{
    collections::LinkedList,
    sync::{
        atomic::{AtomicBool, AtomicI64},
        Arc, Mutex,
    },
};

#[cfg(unix)]
use tokio::io::BufWriter;

#[cfg(unix)]
use tokio::net::unix::OwnedWriteHalf;

use tokio::sync::mpsc;

use tokio::io::AsyncWriteExt;

#[cfg(windows)]
use tokio::net::windows::named_pipe::NamedPipeServer;

use tokio::sync::mpsc::{UnboundedReceiver, UnboundedSender};
use tracing::{debug, info, warn};
use x_common_lib::base::id_generator;
use x_common_lib::base::status::Status;
use x_common_lib::protocol::protocol_ipc::{self, ProtocolIPCReader, ProtocolIPCWriter};
use x_common_lib::protocol::protocol_v1::ProtocolV1Writer;
use x_common_lib::protocol::{to_dxc_protocol_msg, MsgType};
use x_common_lib::serial::request_message::RequestMessage;
use x_common_lib::service::sys_service_api::{
    ServiceInfo, ServiceKey, StringResponse, SubscribeTopicInfo,
};
use x_common_lib::{protocol::XID, utils::time_utils};

use crate::airport::channel::SELF_CHANNEL;
use crate::application::{get_runtime, Application};
use crate::protocol::pack_message_in_ipc_service;
use crate::service::inject_api::{pack_and_return_message, stream_api};
use crate::service::service::Service;
use crate::service::timeout_checker::TimeoutChecker;

use super::{log_api, xport_api};

use x_common_lib::protocol::SYSTEM_CALL_FINALIZE_REQ_ID;

pub struct IPCService {
    dxc_name: String,
    //
    dxc_version: Version,

    service_id: i64,
    //
    md5: String,
    //
    is_init: AtomicBool,

    // 是否退出
    is_exit: AtomicBool,

    /**
     * 上线时间
     */
    online_time: AtomicI64,

    only_in_node: bool,

    // only_build_ipc: bool,
    /**
     * 通知有消息
     */
    message_receiver: tokio::sync::Mutex<UnboundedReceiver<bool>>,

    /**
     * 消息借
     */
    message_sender: UnboundedSender<bool>,

    resp_message_list: Mutex<LinkedList<XID>>,
    /**
     *
     */
    normal_message_list: Mutex<LinkedList<XID>>,
    /**
     *
     */
    system_message_list: Mutex<LinkedList<XID>>,
    /**
     *
     */
    stream_message_list: Mutex<LinkedList<XID>>,

    compatible_versions: Vec<String>,

    #[cfg(windows)]
    writer: tokio::sync::Mutex<Option<NamedPipeServer>>,

    #[cfg(unix)]
    writer: tokio::sync::Mutex<Option<BufWriter<OwnedWriteHalf>>>,
    //
    response_handler_map: Mutex<HashMap<i64, Box<dyn Fn(ServiceKey, Vec<u8>) + Send + Sync>>>,
    //
    process: Mutex<Option<Child>>,
    // 初始化参数
    config: String,
    //
    init_sender: Mutex<Option<tokio::sync::oneshot::Sender<Status>>>,
    //
    finalize_sender: Mutex<Option<tokio::sync::oneshot::Sender<()>>>,
    /**
     * 超时检测器
     */
    timeout_checker: Arc<Box<TimeoutChecker>>,
}

impl IPCService {
    pub fn new(
        dxc_name: String,
        dxc_version: Version,
        dxc_id: i64,
        md5: String,
        config: &str,
        compatible_versions: Vec<String>,
        only_in_node: bool,
        _only_build_ipc: bool,
        timeout: Option<i32>
    ) -> Box<IPCService> {
        //
        let (sender, receiver) = mpsc::unbounded_channel::<bool>();
        let service_id = id_generator::gen_service_id(dxc_id);


        Box::new(IPCService {
            dxc_name,
            service_id,
            md5,
            online_time: AtomicI64::new(0),
            is_init: AtomicBool::new(false),
            dxc_version: dxc_version,
            compatible_versions,
            message_receiver: tokio::sync::Mutex::new(receiver),
            message_sender: sender,
            system_message_list: Mutex::new(LinkedList::new()),
            resp_message_list: Mutex::new(LinkedList::new()),
            normal_message_list: Mutex::new(LinkedList::new()),
            only_in_node,
            // only_build_ipc,
            stream_message_list: Mutex::new(LinkedList::new()),
            is_exit: AtomicBool::new(false),
            writer: tokio::sync::Mutex::new(None),
            response_handler_map: Mutex::new(HashMap::new()),
            process: Mutex::new(None),
            config: config.into(),
            init_sender: Mutex::new(None),
            finalize_sender: Mutex::new(None),
            timeout_checker: Arc::new(Box::new(TimeoutChecker::new(timeout))),
        })
    }

    #[cfg(unix)]
    pub fn get_listen_addr(&self) -> String {
        let version = self.dxc_version.to_string();
        format!("/xport/{}_{}.sock", self.dxc_name, version)
    }

    #[cfg(windows)]
    pub fn get_listen_addr(&self) -> (String, String) {
        let version = self.dxc_version.to_string();
        (
            format!(r"\\.\pipe\\{}_{}_read", self.dxc_name, version),
            format!(r"\\.\pipe\\{}_{}_write", self.dxc_name, version),
        )
    }

    #[cfg(windows)]
    pub async fn set_writer(&self, _writer: NamedPipeServer) {
        let mut writer = self.writer.lock().await;
        *writer = Some(_writer);
    }

    #[cfg(unix)]
    pub async fn set_writer(&self, _writer: OwnedWriteHalf) {
        let mut writer = self.writer.lock().await;
        *writer = Some(BufWriter::new(_writer));
    }

    pub fn is_only_in_node(&self) -> bool {
        self.only_in_node
    }

    pub fn is_compatible_version(&self, version: &String) -> bool {
        self.compatible_versions.contains(version)
    }
    // 设置进程
    pub fn set_process(&self, new_process: Child) {
        let mut process = self.process.lock().unwrap();
        if process.is_some() {
            let old_process = process.take();
            old_process.unwrap().kill().unwrap();
        }
        *process = Some(new_process)
    }

    pub async fn wait_inited(&self) -> Status {
        let (inited_sender, inited_receiver) = tokio::sync::oneshot::channel::<Status>();
        {
            let mut sender = self.init_sender.lock().unwrap();
            *sender = Some(inited_sender);
        }
        inited_receiver.await.unwrap()
    }

    pub fn start(self: &Arc<Box<Self>>) {
        //
        self.online_time.store(
            time_utils::cur_timestamp(),
            std::sync::atomic::Ordering::Relaxed,
        );
        let clone_self = self.clone();
        get_runtime().spawn(async move {
            clone_self.handle_message().await;
        });
        let airport = Application::get_airport();
        let clone_self = self.clone();
        get_runtime().spawn(async move {
            airport.ipc_net.start_receive_message(clone_self).await;
        });
        
        let timeout_checker = self.timeout_checker.clone();
        let service_key = self.get_service_key();
        get_runtime().spawn(async move {
            timeout_checker
                .check_timeout_message(service_key, false)
                .await;
        });
    }

    /**
     * 添加等待回复的请求,用于判断是否超时
     */
    pub fn add_waitting_request_id(&self, request_id: i64) {
        self.timeout_checker.add_waitting_request_id(request_id);
    }

    pub fn get_service_info(&self) -> Box<ServiceInfo> {
        let mut service_info = Box::new(ServiceInfo::default());
        service_info.dxc_name = self.dxc_name();
        service_info.dxc_version = self.dxc_version.to_string();
        service_info.service_id = self.service_id;
        service_info.md5 = self.md5();
        service_info.online_time = self.oline_time();
        service_info.compatible_versions = self.compatible_versions.clone();
        service_info.only_in_node = self.only_in_node;
        service_info
    }

    pub fn dxc_name(&self) -> String {
        self.dxc_name.clone()
    }

    pub fn md5(&self) -> String {
        self.md5.clone()
    }

    pub fn is_exit(&self) -> bool {
        self.is_exit.load(std::sync::atomic::Ordering::Acquire)
    }

    fn get_xid(&self) -> Option<(XID, MsgType)> {
        let xid = {
            let mut message_list = self.system_message_list.lock().unwrap();
            message_list.pop_front()
        };

        if let Some(xid) = xid {
            return Some((xid, MsgType::System));
        }

        let xid = {
            let mut message_list = self.resp_message_list.lock().unwrap();
            message_list.pop_front()
        };

        if let Some(xid) = xid {
            return Some((xid, MsgType::RspNormal));
        }

        let xid = {
            let mut message_list = self.normal_message_list.lock().unwrap();
            message_list.pop_front()
        };

        if let Some(xid) = xid {
            return Some((xid, MsgType::Normal));
        }

        let xid = {
            let mut message_list = self.stream_message_list.lock().unwrap();
            message_list.pop_front()
        };

        if let Some(xid) = xid {
            return Some((xid, MsgType::Stream));
        }

        None
    }

    fn putback_msg(&self, xid: XID, msg_type: MsgType) {
        if msg_type == MsgType::System {
            let mut message_list = self.system_message_list.lock().unwrap();
            message_list.push_front(xid)
        } else if msg_type == MsgType::RspNormal {
            let mut message_list = self.resp_message_list.lock().unwrap();
            message_list.push_front(xid)
        } else if msg_type == MsgType::Normal {
            let mut message_list = self.normal_message_list.lock().unwrap();
            message_list.push_front(xid)
        } else if msg_type == MsgType::Stream {
            let mut message_list = self.stream_message_list.lock().unwrap();
            message_list.push_front(xid)
        }
    }

    fn add_response_handler(
        &self,
        xid: XID,
        receiver_service_key: Box<ServiceKey>, // 消息
    ) {
        //
        let mut response_handler_map = self.response_handler_map.lock().unwrap();
        response_handler_map.insert(
            xid.request_id,
            Box::new(move |self_service_key, msg| {
                let reader = ProtocolIPCReader::new(&msg);
                let buffer = reader.msg_body();

                let msg_buffer = pack_message_in_ipc_service(
                    xid.request_id,
                    &self_service_key,
                    &receiver_service_key,
                    MsgType::RspNormal,
                    buffer,
                );

                //
                let airport = Application::get_airport();
                get_runtime().spawn(async move {
                    airport.response_message(xid, msg_buffer).await;

                    let message_manager = airport.get_message_manager();
                    //
                    message_manager.delete_normal_message(xid).await;
                });
            }),
        );
    }
    //
    fn handle_response(&self, request_id: i64, self_service_key: ServiceKey, message: Vec<u8>) {
        let handler = {
            let mut response_handler_map = self.response_handler_map.lock().unwrap();
            response_handler_map.remove(&request_id)
        };
        if handler.is_none() {
            return;
        }
        let handler = handler.unwrap();

        (handler)(self_service_key, message);
    }
    // 发送给 ipc 失败，删除等待 ipc 返回的处理函数
    async fn delete_reponse_handler(&self, request_id: i64) {
        //
        let mut response_handler_map = self.response_handler_map.lock().unwrap();
        response_handler_map.remove(&request_id);
    }
    //

    async fn handle_message(self: Arc<Box<Self>>) {
        //
        let airport = Application::get_airport();
        let mut message_receiver = self.message_receiver.lock().await;
        while let Some(is_handle) = message_receiver.recv().await {
            if !is_handle {
                break;
            }
            //
            let mut writer = self.writer.lock().await;

            if writer.is_none() {
                continue;
            }
            // 增加 loop 处理完消息
            loop {
                //
                let xid = self.get_xid();
                // 处理消息
                if xid.is_none() {
                    break;
                }
                let (xid, msg_type) = xid.unwrap();
                //
                let message_manager = airport.get_message_manager();
                //
                let message = if msg_type == MsgType::System || msg_type == MsgType::Normal {
                    message_manager.get_normal_msg(&xid).await
                } else if msg_type == MsgType::RspNormal {
                    message_manager.get_resp_msg(&xid).await
                } else if msg_type == MsgType::Stream {
                    message_manager.get_stream_message(&xid)
                } else {
                    None
                };
                if message.is_none() {
                    info!(
                        "service_id={}, 消息:{:?} msg_type:{:?}不存在...",
                        self.service_id, xid, msg_type
                    );
                    continue;
                }
                let (message, channel_id, from_addr) = message.unwrap();

                let dxc_msg = to_dxc_protocol_msg(channel_id, from_addr, message.as_slice());
                //
                if dxc_msg.is_none() {
                    warn!("无法识别协议！");
                    return;
                }
                let (dxc_msg, sender_service_key) = dxc_msg.unwrap();
                if msg_type == MsgType::Normal {
                    //
                    if !self.is_init.load(std::sync::atomic::Ordering::Relaxed) {
                        info!(
                            "{} {} 未初始化完成，消息做丢弃处理。。。",
                            self.dxc_name, self.dxc_version
                        );
                        message_manager.delete_normal_message(xid).await;
                        continue;
                    }
                    // 添加消息返回值的处理函数
                    // normal 的消息，在返回处理器中删除内存
                    self.add_response_handler(xid, sender_service_key);
                }

                // 转换为 ipc 协议
                let mut ipc_writer = ProtocolIPCWriter::new(dxc_msg.len() as u32, 0, 0);
                //
                ipc_writer.write_msg_body(&dxc_msg);
                //
                let writer = writer.as_mut().unwrap();
                // channel_id 设置源
                let ret = writer.write_all(&ipc_writer.msg_buffer).await;
                //
                if let Err(_) = ret {
                    info!(
                        "service_id={},  发送消息 {:?} 失败, 长度：{}",
                        self.service_id,
                        xid,
                        message.len()
                    );
                    if msg_type == MsgType::Normal {
                        self.delete_reponse_handler(xid.request_id).await;
                    }
                    self.putback_msg(xid, msg_type);
                    break;
                }
                //
                let ret = writer.flush().await;
                if let Err(_) = ret {
                    info!(
                        "service_id={},  发送消息 {:?} 失败, 长度：{}",
                        self.service_id,
                        xid,
                        message.len()
                    );
                    if msg_type == MsgType::Normal {
                        self.delete_reponse_handler(xid.request_id).await;
                    }
                    self.putback_msg(xid, msg_type);
                    break;
                }
                // resp 消息发送完成后，直接删除即可
                if msg_type == MsgType::RspNormal {
                    message_manager.delete_resp_message(xid).await;
                } else if msg_type == MsgType::Stream {
                    message_manager.deref_stream_message(&xid);
                }
            }
        }
        message_receiver.close();
    }

    // 分发 ipc 回调的信息
    pub fn disptach_api_invoke(self: &Arc<Box<Self>>, msg: Vec<u8>) {
        // 消息协议：
        let reader = ProtocolIPCReader::new(&msg);
        let api_id = reader.api_id();
        let service_key = self.get_service_key();
        let service_id = self.service_id;
        if api_id == protocol_ipc::xport::RESP_MESSAGE_API_ID {
            let self_clone = self.clone();
            let request_id = reader.request_id();
            get_runtime().spawn(async move {
                self_clone.handle_response(request_id, service_key, msg);
            });
        } else if api_id == protocol_ipc::xport::GET_CONFIG_API_ID {
            //
            // 获取配置，表示是第一次启动
            //
            self.is_init
                .store(false, std::sync::atomic::Ordering::Release);
            //
            let request_id = reader.request_id();
            let mut result = StringResponse::default();
            result.value = self.config.clone();
            pack_and_return_message(request_id, service_key, result);
            //
        } else if api_id == protocol_ipc::xport::NOTICE_READY_API_ID {
            //
            let msg_body = reader.msg_body();
            let mut status = Status::default();
            let mut is = CodedInputStream::from_bytes(msg_body);
            let ret = status.parse_from_input_stream_with_tag_and_len(&mut is);
            if let Err(err) = ret {
                status.err_code = err.err_code;
                status.err_msg = err.err_msg;
            }

            if status.err_code == 0 {
                self.is_init
                    .store(true, std::sync::atomic::Ordering::Release);
            }

            let mut inited_sender = self.init_sender.lock().unwrap();
            if let Some(inited_sender) = inited_sender.take() {
                let _ = inited_sender.send(status);
            }
        } else if api_id == protocol_ipc::xport::NOTICE_FINALIZE_API_ID {
            //

            debug!("ipc service {:?} 结束。。。", &service_key);
            let mut finalize_sender = self.finalize_sender.lock().unwrap();
            if let Some(finalize_sender) = finalize_sender.take() {
                let _ = finalize_sender.send(());
            }
        } else if api_id == protocol_ipc::xport::NOTICE_ERROR_API_ID {
            debug!("ipc service {:?} 错误。。。", &service_key);
            // todo!("实现处理错误的")；

            let mut inited_sender = self.init_sender.lock().unwrap();
            //  等待初始化的，都提示初始化失败
            if let Some(inited_sender) = inited_sender.take() {
                info!("service {:?} 错误, 移除实例", service_key);
                let _ = inited_sender.send(Status::error("启动失败".to_owned()));
            }

            // else if !self.only_build_ipc {
            //     // 不是 ipc 模式的话，移除
            //     get_runtime().spawn(async move {
            //         let airport = Application::get_airport();
            //         let dxc_manager = airport.get_dxc_manager();

            //         let dxc_version = Version::parse(&service_key.dxc_version).unwrap();

            //         let local_dxc = dxc_manager
            //             .get_dxc_by_name_and_version(&service_key.dxc_name, &dxc_version)
            //             .await;

            //         if let Some(local_dxc) = local_dxc {
            //             local_dxc.remove_local_or_ipc_service().await;
            //             dxc_manager
            //                 .remove_dxc(&service_key.dxc_name, &dxc_version)
            //                 .await;
            //         }
            //     });
            // }
        } else if api_id == protocol_ipc::stream::PUBLISH_MESSAGE_API_ID {
            // 增加一个消息
            stream_api::ipc_publish_message(service_id, msg);
            // stream_api::ipc_publish_message(service_id, msg).await;
        } else {
            let ipc_service = self.clone();
            get_runtime().spawn(async move {
                match api_id {
                    protocol_ipc::xport::SEND_MESSAGE_API_ID => {
                        // 往外发送消息
                        xport_api::send_message(ipc_service, service_key, msg).await;
                    }
                    protocol_ipc::xport::GET_ALL_CONN_ID_API_ID => {
                        xport_api::get_all_conn_id(service_key, msg).await;
                    }
                    protocol_ipc::xport::GET_CHANNEL_ID_BY_CONN_API_ID => {
                        xport_api::get_channel_id_by_conn_id(service_key, msg).await;
                    }
                    // log_api
                    protocol_ipc::log::OUTPUT_API_ID => {
                        log_api::output(service_key, msg).await;
                    }
                    // protocol_ipc::stream::PUBLISH_MESSAGE_API_ID => {
                    //     stream_api::ipc_publish_message(service_id, msg).await;
                    // }
                    // stream_api
                    protocol_ipc::stream::XPORT_SUBSCRIBE_TOPIC_API_ID
                    | protocol_ipc::stream::XPORT_CANCEL_SUBSCRIBE_TOPI_API_ID
                    | protocol_ipc::stream::DXC_SUBSCRIBE_TOPIC_API_ID
                    | protocol_ipc::stream::DXC_CANCEL_SUBSCRIBE_TOPI_API_ID => {
                        //
                        let reader = ProtocolIPCReader::new(&msg);
                        //
                        let request_id = reader.request_id();
                        let msg_body = reader.msg_body();
                        let mut subscribe_info = SubscribeTopicInfo::default();
                        subscribe_info.parse_from_bytes(msg_body).unwrap();

                        subscribe_info.service_id = service_id;

                        match api_id {
                            protocol_ipc::stream::XPORT_SUBSCRIBE_TOPIC_API_ID => {
                                //
                                stream_api::async_xport_subscribe_topic(
                                    service_key,
                                    subscribe_info,
                                    request_id,
                                )
                                .await;
                            }
                            protocol_ipc::stream::XPORT_CANCEL_SUBSCRIBE_TOPI_API_ID => {
                                //
                                stream_api::async_xport_cancel_subscribe_topic(
                                    service_key,
                                    subscribe_info,
                                    request_id,
                                )
                                .await;
                            }
                            protocol_ipc::stream::DXC_SUBSCRIBE_TOPIC_API_ID => {
                                //
                                stream_api::async_dxc_subscribe_topic(
                                    service_key,
                                    subscribe_info,
                                    request_id,
                                )
                                .await;
                            }
                            protocol_ipc::stream::DXC_CANCEL_SUBSCRIBE_TOPI_API_ID => {
                                //
                                stream_api::async_dxc_cancel_subscribe_topic(
                                    service_key,
                                    subscribe_info,
                                    request_id,
                                )
                                .await;
                            }
                            _ => {}
                        }
                    }

                    _ => {
                        //
                    }
                }
            });
        }
    }
    //
    pub async fn async_finalize(&self) {
        //
        let (finalize_sender, finalize_receiver) = tokio::sync::oneshot::channel::<()>();
        {
            let mut sender = self.finalize_sender.lock().unwrap();
            *sender = Some(finalize_sender);
        }
        let receiver = self.get_service_key();
        //
        get_runtime().spawn(async move {
            let default_service_key = ServiceKey::default();

            let mut v1_writer = ProtocolV1Writer::new(
                0,
                SYSTEM_CALL_FINALIZE_REQ_ID,
                &default_service_key,
                &receiver,
            );
            v1_writer.write_conn_id(0);
            v1_writer.write_msg_type(MsgType::System);
            let airport = Application::get_airport();
            let msg_buffer = v1_writer.msg_buffer;
            airport
                .message_in(SELF_CHANNEL, SELF_CHANNEL, msg_buffer)
                .await;
        });

        let _ = finalize_receiver.await;
    }
}

impl Drop for IPCService {
    fn drop(&mut self) {
        {
            let mut stream_message_list = self.stream_message_list.lock().unwrap();
            let airport = Application::get_airport();
            let message_manager = airport.get_message_manager();
            for xid in stream_message_list.iter() {
                message_manager.deref_stream_message(xid);
            }
            stream_message_list.clear();
        }
    }
}

#[async_trait]
impl Service for IPCService {
    fn id(&self) -> i64 {
        self.service_id
    }
    fn version(&self) -> Version {
        self.dxc_version.clone()
    }

    async fn init(self: Arc<Box<Self>>, _config: &str) -> Result<(), Status> {
        Ok(())
    }

    async fn stop(&self) {
        //
        self.timeout_checker.stop();
        let _ = self.message_sender.send(false);
        self.message_sender.closed().await;
        #[cfg(windows)]
        {
            let mut writer = self.writer.lock().await;
            writer.take();
        }
        self.is_exit
            .store(true, std::sync::atomic::Ordering::Release);
        // 杀死进程
        {
            let mut process = self.process.lock().unwrap();
            if process.is_some() {
                let process = process.take();
                let mut process = process.unwrap();
                process.kill().unwrap();
                let _ = process.wait();
            }
        }
    }

    fn dispatch_message_with_type(&self, msg_type: MsgType, xid: XID) {
        match msg_type {
            MsgType::System => {
                let mut msg_list = self.system_message_list.lock().unwrap();
                msg_list.push_back(xid)
            }
            MsgType::RspNormal => {
                self.timeout_checker.remove_message(xid.request_id);
                let mut msg_list = self.resp_message_list.lock().unwrap();
                msg_list.push_back(xid)
            }
            MsgType::Normal => {
                let mut msg_list = self.normal_message_list.lock().unwrap();

                msg_list.push_back(xid)
            }
            MsgType::Stream => {
                let mut msg_list = self.stream_message_list.lock().unwrap();

                msg_list.push_back(xid)
            }
            _ => {
                info!("不支持消息类型：{:?}", msg_type)
            }
        };
        let result: Result<(), mpsc::error::SendError<bool>> = self.message_sender.send(true);
        if result.is_err() {
            warn!(
                "dxc:{} version: {} 接收消息出错！",
                self.dxc_name, self.dxc_version
            )
        }
    }

    fn finalize(&self) {
        // ipc 忽略该函数
    }

    fn get_service_key(&self) -> ServiceKey {
        ServiceKey::new(
            self.dxc_name.clone(),
            self.dxc_version.to_string(),
            self.md5(),
        )
    }

    fn oline_time(&self) -> i64 {
        self.online_time.load(std::sync::atomic::Ordering::Relaxed)
    }
}
