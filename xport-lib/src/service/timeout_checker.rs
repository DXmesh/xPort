use std::{
    collections::BTreeSet,
    sync::{Arc, Mutex},
    time::Duration,
};

use tokio::sync::mpsc::{self, UnboundedReceiver, UnboundedSender};
use x_common_lib::{
    base::{id_generator, status::Status},
    service::sys_service_api::ServiceKey,
    utils::time_utils,
};

use crate::{
    application::Application,
    service::inject_api::{pack_and_return_error, pack_and_return_status_to_airport},
};

pub struct TimeoutChecker {
    timeout: i64,
    /**
     * 等待回复的信息, 用于超时判断
     */
    waitting_resp_message_set: Mutex<BTreeSet<i64>>,
    /**
     *
     */
    waitting_resp_sender: UnboundedSender<i64>,
    /**
     *
     */
    waitting_resp_receiver: tokio::sync::Mutex<UnboundedReceiver<i64>>,
}

impl TimeoutChecker {
    pub fn new(timeout: Option<i32>) -> Self {
        let (waitting_resp_sender, waitting_resp_receiver) = mpsc::unbounded_channel::<i64>();

        let timeout = if let Some(timeout) = timeout {
            timeout as i64 * 1000
        } else {
            Application::msg_timeout()
        };

        Self {
            timeout,
            waitting_resp_message_set: Mutex::new(BTreeSet::new()),
            waitting_resp_sender,
            waitting_resp_receiver: tokio::sync::Mutex::new(waitting_resp_receiver),
        }
    }

    pub fn stop(&self) {
        let _ = self.waitting_resp_sender.send(-1);
    }

    pub fn add_waitting_request_id(&self, request_id: i64) {
        let mut waitting_resp_message_map = self.waitting_resp_message_set.lock().unwrap();

        waitting_resp_message_map.insert(request_id);
    }

    pub fn remove_message(&self, request_id: i64) {
        let _ = self.waitting_resp_sender.send(request_id);
    }

    /**
     * 检查超时的消息
     */
    pub async fn check_timeout_message(
        self: Arc<Box<Self>>,
        service_key: ServiceKey,
        is_airport: bool,
    ) {
        let mut waitting_resp_receiver = self.waitting_resp_receiver.lock().await;
        //
        let timeout = self.timeout;

        loop {
            // 1s 检查换一次超时情况
            let timer = tokio::time::sleep(Duration::from_millis(1000));
            tokio::select! {
                request_id = waitting_resp_receiver.recv() => {
                    if let Some(request_id) = request_id {
                        if request_id == -1 {
                            break;
                        }
                        //
                        let mut waitting_resp_message_set = self.waitting_resp_message_set.lock().unwrap();
                        waitting_resp_message_set.remove(&request_id);
                    } else {
                        break;
                    }
                }
                _= timer => {
                  let timeout_request_ids =   {
                        let cur_timeout = time_utils::cur_timestamp() - timeout;
                        let mut waitting_resp_message_set = self.waitting_resp_message_set.lock().unwrap();
                        let mut timeout_request_ids = Vec::with_capacity(100);
                        for request_id in waitting_resp_message_set.iter() {
                            let request_id = *request_id;
                            //
                            let request_time = id_generator::get_id_timestamp(request_id);
                            // id 是单项递增的，id 所包含的时间也是递增的
                            if cur_timeout < request_time {
                                break;
                            }
                            timeout_request_ids.push(request_id);
                        }
                        for timeout_request_id in timeout_request_ids.iter() {
                            waitting_resp_message_set.remove(timeout_request_id);
                        }
                        timeout_request_ids
                    };

                    for timeout_request_id in timeout_request_ids.into_iter() {
                        let status = Status::error(String::from("请求超时!"));
                        if is_airport {
                          pack_and_return_status_to_airport(timeout_request_id, service_key.clone(), status);
                        } else {
                          pack_and_return_error(timeout_request_id, service_key.clone(),status);
                        }
                    }
                }
            }
        }
    }
}
