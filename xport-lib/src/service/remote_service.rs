use std::{
    collections::HashSet,
    sync::{atomic::AtomicI64, Arc, RwLock},
};

use async_trait::async_trait;
use semver::Version;
use x_common_lib::{
    base::status::Status,
    protocol::{
        MsgType, XID,
    },
    service::sys_service_api::ServiceKey,
    utils::time_utils,
};


use super::service::Service;


pub struct RemoteService {
    dxc_name: String,

    dxc_version: Version,

    md5: String,

    service_id: AtomicI64,

    online_time: AtomicI64,
    /**
     * 兼容的版本号
     */
    compatible_versions: Vec<String>,
    // /**
    //  * 通知有消息
    //  */
    // message_receiver: Mutex<UnboundedReceiver<MessageSignal>>,
    // /**
    //  * 消息借
    //  */
    // message_sender: UnboundedSender<MessageSignal>,
    channel_id_set: RwLock<HashSet<i64>>,
    // wait_ack_message_id_map: Mutex<HashMap<i64, XID>>,
}

impl RemoteService {
    pub fn new(
        service_id: i64,
        dxc_name: String,
        dxc_version: Version,
        md5: String,
        compatible_versions: Vec<String>,
    ) -> Box<RemoteService> {
        Box::new(RemoteService {
            service_id: AtomicI64::new(service_id),
            dxc_name: dxc_name,
            md5,
            compatible_versions,
            dxc_version: dxc_version,
            online_time: AtomicI64::new(0),
            channel_id_set: RwLock::new(HashSet::new()),
        })
    }

    pub fn md5(&self) -> String {
        self.md5.clone()
    }

    pub fn is_compatible_version(&self, version: &String) -> bool {
        self.compatible_versions.contains(version)
    }

    pub fn add_channel_id(&self, channel_id: i64) {
        let mut channel_set = self.channel_id_set.write().unwrap();

        channel_set.insert(channel_id);

    }

    pub fn has_channel(&self) -> bool {
        let channel_set = self.channel_id_set.read().unwrap();
        !channel_set.is_empty()
    }
    //
    pub fn clear_channel(&self) {
        let mut channel_set = self.channel_id_set.write().unwrap();

        channel_set.clear();
    }

    pub fn remove_channel_id(&self, channel_id: i64) {
        let mut channel_set = self.channel_id_set.write().unwrap();
        channel_set.remove(&channel_id);
    }

    pub fn get_one_channel_id(&self) -> Option<i64> {
        let channel_set = self.channel_id_set.read().unwrap();
        if channel_set.is_empty() {
            None
        } else {
            Some(*channel_set.iter().next().unwrap())
        }
    }



    pub fn start(self: Arc<Box<Self>>) {
        self.online_time.store(
            time_utils::cur_timestamp(),
            std::sync::atomic::Ordering::Relaxed,
        );
    }
}

#[async_trait]
impl Service for RemoteService {
    /**
     * id
     */
    fn id(&self) -> i64 {
        self.service_id.load(std::sync::atomic::Ordering::Acquire)
    }

    /**
     * 获取版本号, 返回的是序列化后的 protobuf 格式的字符串
     */

    fn version(&self) -> Version {
        self.dxc_version.clone()
    }

    /**
     * 初始化, 此时还不能接受任务
     */
    async fn init(self: Arc<Box<Self>>, _config: &str) -> Result<(), Status> {
        Ok(())
    }
    /**
     * 可以开始处理任务
     */

    fn dispatch_message_with_type(&self, _msg_type: MsgType, _xid: XID) {}

    /**
     * 主动关闭
     */
    async fn stop(&self) {
        // let _ = self.message_sender.send(MessageSignal::Exit);
        // self.message_sender.closed().await;
    }

    /**
     * 终结
     */
    fn finalize(&self) {}

    fn get_service_key(&self) -> ServiceKey {
        ServiceKey::new(
            self.dxc_name.clone(),
            self.dxc_version.to_string(),
            self.md5(),
        )
    }

    /**
     * 在线时间
     */
    fn oline_time(&self) -> i64 {
        self.online_time.load(std::sync::atomic::Ordering::Relaxed)
    }
}
