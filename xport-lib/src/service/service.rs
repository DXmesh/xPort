use std::sync::Arc;

use async_trait::async_trait;
use semver::Version;
use tracing::info;
use x_common_lib::{
    base::status::Status,
    protocol::{
        get_version, protocol_v1::ProtocolV1Reader, protocol_v2::ProtocolV2Reader,
        protocol_v3::ProtocolV3Reader, MsgType, XID,
    },
    service::sys_service_api::ServiceKey,
};

/**
 * Service
 */

#[async_trait]
pub trait Service: Send + Sync {
    /**
     * id
     */
    fn id(&self) -> i64;
    /**
     * 获取版本号, 返回的是序列化后的 protobuf 格式的字符串
     */
    fn version(&self) -> Version;

    /**
     * 初始化, 此时还不能接受任务
     */
    async fn init(self: Arc<Box<Self>>, config: &str) -> Result<(), Status>;

    /**
     * 通知消息进来
     */

    fn message_in(&self, msg_buffer: &Vec<u8>) -> bool {
        let protocol_version = get_version(msg_buffer);
        //

        match protocol_version {
            1 => {

                let reader = ProtocolV1Reader::new(msg_buffer);
                let xid = reader.xid();
                let msg_type = reader.msg_type();
                if msg_type.is_none() {
                    return false;
                }
                let msg_type: MsgType = msg_type.unwrap();
                self.dispatch_message_with_type(msg_type, xid);
                true
            }
            2 => {
                let reader = ProtocolV2Reader::new(msg_buffer);
                let xid = reader.xid();
                let msg_type = reader.msg_type();
                if msg_type.is_none() {
                    return false;
                }
                let msg_type = msg_type.unwrap();
                self.dispatch_message_with_type(msg_type, xid);

                true
            }
            3 => {
                let reader = ProtocolV3Reader::new(msg_buffer);
                let xid = reader.xid();
                self.dispatch_message_with_type(MsgType::Stream, xid);

                true
            }
            _ => {
                info!("不支持 {} 协议的消息", protocol_version);
                false
            }
        }
    }

    fn dispatch_message_with_type(&self, msg_type: MsgType, xid: XID);

    /**
     * 主动关闭
     */
    async fn stop(&self);

    /**
     * 终结
     */
    fn finalize(&self);

    fn get_service_key(&self) -> ServiceKey;

    /**
     * 在线时间
     */
    fn oline_time(&self) -> i64;
}
