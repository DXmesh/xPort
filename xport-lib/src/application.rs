use get_if_addrs::get_if_addrs;
use semver::Version;
use tokio::runtime::Runtime;
use x_common_lib::base::id_generator::make_node_id;
use x_common_lib::utils::{string_utils, time_utils};

use std::mem::MaybeUninit;
use std::panic;
use std::sync::{Arc, Mutex};
use tokio::sync::oneshot;
use tracing::info;

use crate::airport::airport::Airport;
use crate::airport::channel::SELF_CHANNEL;
use crate::event::Event;
use crate::log::{self, append_log};
use crate::net::ipc_net::IPCNet;
use crate::net::tcp_net::TCPNet;
use crate::net::xrpc_event_handler::XRPCEventHandler;
use crate::protocol::protocol_hearbeat::ProtocolHeartbeatHandler;
use crate::protocol::protocol_v1::ProtocolV1Handler;
use crate::protocol::protocol_v2::ProtocolV2Handler;
use crate::protocol::protocol_v3::ProtocolV3Handler;
use crate::sign::Signer;

use x_common_lib::base::config::Config;

pub static mut APP: MaybeUninit<Application> = MaybeUninit::uninit();

pub static mut RUNTIME: MaybeUninit<Runtime> = MaybeUninit::uninit();

pub fn init_runtime() {
    // 初始化时间的偏移
    time_utils::init_offset_whole_seconds();

    let runtime = tokio::runtime::Builder::new_multi_thread()
        .worker_threads(4) // 设置线程池中的工作线程数量
        .enable_all()
        .build()
        .unwrap();
    #[allow(static_mut_refs)]
    unsafe {
        RUNTIME.as_mut_ptr().write(runtime);
    }
}

pub fn get_runtime() -> &'static Runtime {
    #[allow(static_mut_refs)]
    unsafe {
        RUNTIME.assume_init_ref()
    }
}

pub struct Application {
    //
    signer: Signer,
    //
    exit_sender: Mutex<Option<oneshot::Sender<()>>>,
    //
    exit_reciver: Option<oneshot::Receiver<()>>,
    // 默认 30_000 毫秒.
    msg_timeout: i64,
    // 消息中心
    airport: Arc<Box<Airport>>,
    //
    config: Arc<Box<Config>>,
    //
    event: Arc<Box<Event>>,
}

impl Application {
    //
    pub fn get_airport() -> Arc<Box<Airport>> {
        #[allow(static_mut_refs)]
        unsafe {
            APP.assume_init_ref().airport.clone()
        }
    }

    pub fn get_event() -> Arc<Box<Event>> {
        #[allow(static_mut_refs)]
        unsafe {
            APP.assume_init_ref().event.clone()
        }
    }

    //
    pub fn get_config() -> Arc<Box<Config>> {
        #[allow(static_mut_refs)]
        unsafe {
            APP.assume_init_ref().config.clone()
        }
    }

    pub fn get_signer() -> &'static Signer {
        #[allow(static_mut_refs)]
        unsafe {
            &APP.assume_init_ref().signer
        }
    }

    pub fn msg_timeout() -> i64 {
        #[allow(static_mut_refs)]
        unsafe {
            APP.assume_init_ref().msg_timeout
        }
    }

    /**
     *
     */
    async fn load_airport(config: &Box<Config>) -> Arc<Box<Airport>> {
        //
        let xrpc_event_handler = XRPCEventHandler::new(config);

        let mut xrpc_net = Box::new(TCPNet::new(xrpc_event_handler));

        let xrpc_port = config.get_i32("xport", "xrpc-port").unwrap_or(8090) as u16;

        let xrpc_ip = config.get_str("xport", "xrpc-ip").unwrap_or("127.0.0.1");

        let mut conn_ids = Vec::new();
        let interfaces = get_if_addrs().expect("获取 ip 失败！");

        conn_ids.push(SELF_CHANNEL);
        for iface in interfaces {
            //
            match iface.addr.ip() {
                std::net::IpAddr::V4(ipv4) => {
                    let ipv4 = ipv4.to_string();
                    let conn_id = make_node_id(&ipv4, xrpc_port);
                    conn_ids.push(conn_id);
                }
                _ => {}
            }
        }

        xrpc_net.init(String::from(xrpc_ip), xrpc_port);

        let xrpc_net = Arc::new(xrpc_net);

        let ipc_net = Arc::new(Box::new(IPCNet::new()));

        let mut airport = Airport::new(xrpc_net, ipc_net, conn_ids);

        let heartbeat_handler = Box::new(ProtocolHeartbeatHandler::new());
        airport.add_protocol_handler(heartbeat_handler);

        // v1 协议
        let v1_hanlder = Box::new(ProtocolV1Handler::new());
        airport.add_protocol_handler(v1_hanlder);

        // v2 协议
        let v2_hanlder = Box::new(ProtocolV2Handler::new());
        airport.add_protocol_handler(v2_hanlder);

        // v3 协议
        let v3_hanlder = Box::new(ProtocolV3Handler::new());
        airport.add_protocol_handler(v3_hanlder);

        Arc::new(airport)
    }

    pub async fn boot_net() {
        let (xrpc_sender, xprc_receicer) = oneshot::channel::<bool>();

        get_runtime().spawn(async move {
            let airport = Self::get_airport();
            airport.xrpc_net.clone().start(xrpc_sender).await;
        });

        let is_succeed = xprc_receicer.await;

        let is_succeed = is_succeed.expect("启动 xrpc 失败");

        if !is_succeed {
            panic!("启动 xrpc 失败");
        }
    }

    /**
     * 加载默认的 dxc ，如 XComService
     */
    async fn load_default_dxc(config: Arc<Box<Config>>) {
        let default_dxc = config.get_str_array("xport", "default-dxc");

        let dxc_manager = Self::get_airport().get_dxc_manager();

        for dxc_name in default_dxc {
            // 获取 dxc 配置，如果只 ipc 模式，只是启动 ipc 监听的功能，不会运行对应的进程，如python， jvm进程。该模式用于调试 python，java编写的 dxc
            let only_build_ipc = config.get_bool(dxc_name, "only-build-ipc").unwrap_or(false);

            let config = Application::get_config();

            let compatible_versions = config.get_str_array(dxc_name, "compatible-versions");

            let only_in_node = config.get_bool(dxc_name, "only-in-node").unwrap_or(false);

            let timeout = config.get_i32(dxc_name, "timeout");

            let compatible_versions: Vec<String> =
                compatible_versions.iter().map(|s| s.to_string()).collect();

            let config = if let Some(table) = config.get_table(dxc_name) {
                table.to_string()
            } else {
                String::default()
            };
            let result = string_utils::extract_dxc_name_and_version(&dxc_name);
            if result.is_none() {
                panic!("加载 dxc {} 失败， 版本号解析错误", dxc_name);
            }

            let (dxc_name, dxc_version) = result.unwrap();
            let dxc_version = Version::parse(&dxc_version);

            if dxc_version.is_err() {
                panic!("加载 dxc {} 失败， 版本号解析错误", dxc_name);
            }

            let dxc_version = dxc_version.unwrap();

            dxc_manager
                .load_local_or_ipc_service(
                    &dxc_name,
                    &dxc_version,
                    only_build_ipc,
                    &config,
                    compatible_versions,
                    only_in_node,
                    timeout,
                )
                .await
                .expect(format!("加载默认服务 {}-{} 失败！", dxc_name, dxc_version).as_str())
        }
    }

    fn set_panic_hook() {
        panic::set_hook(Box::new(|info| {
            use std::io::Write;

            let location = info.location();

            if location.is_none() {
                return;
            }
            let message = if let Some(s) = info.payload().downcast_ref::<&str>() {
                s.to_string()
            } else if let Some(s) = info.payload().downcast_ref::<String>() {
                s.clone()
            } else {
                String::default()
            };

            let location = location.unwrap();

            let mut buffer: Vec<u8> = Vec::new();

            write!(
                &mut buffer,
                "{} ERROR xport {} {}: {}\n",
                time_utils::cur_time_str(),
                location.file(),
                location.line(),
                message
            )
            .expect("打印日志失败！");

            append_log(buffer);
        }));
    }

    /**
     * 初始化
     */
    pub async fn init(private_key: String) {
        // 设置全局 panic 钩子
        Self::set_panic_hook();
        let (sender, receiver) = oneshot::channel::<()>();
        let config = Config::load("dxmesh.toml");
        let config = Box::new(config);
        // 初始化日志
        log::init(&config);
        info!("启动 Airport...");
        let airport = Self::load_airport(&config).await;
        let config = Arc::new(config);
        // 消息请求的超时时间为 30s
        let msg_timeout = config.get_i64("xport", "msg-timeout").unwrap_or(30) * 1000;

        #[allow(static_mut_refs)]
        unsafe {
            APP.as_mut_ptr().write(Application {
                signer: Signer::new(private_key),
                airport,
                msg_timeout,
                exit_sender: Mutex::new(Some(sender)),
                exit_reciver: Some(receiver),
                config: config.clone(),
                event: Arc::new(Box::new(Event::new())),
            });
        }

        let is_boot = config.get_bool("xport", "xrpc-listen").unwrap_or(true);

        Self::load_default_dxc(config).await;

        get_runtime().spawn(async {
            let airport = Application::get_airport();
            airport.get_stream().receive_publish_message().await;
        });

        if is_boot {
            Self::boot_net().await;
        }
    }

    #[allow(dead_code)]
    pub fn exit() {
        #[allow(static_mut_refs)]
        unsafe {
            // 方式多次调用
            let app_ins = APP.assume_init_ref();
            let mut sender = app_ins.exit_sender.lock().unwrap();
            if let Some(sender) = sender.take() {
                let _ = sender.send(());
            }
        }
    }
    /**
     * 全局只执行一次，不需要加锁
     */
    pub async fn run_loop() {
        #[allow(static_mut_refs)]
        unsafe {
            let app_ins = APP.assume_init_mut();
            if let Some(receiver) = app_ins.exit_reciver.take() {
                receiver.await.expect("等待退出异常");
            }
        }
    }
}
