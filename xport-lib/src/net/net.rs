

use async_trait::async_trait;



#[async_trait]
pub trait SocketEventHandler: Sync + Send {
    fn max_message_size(&self) -> u32;

    fn on_start(&self);

    fn on_stop(&self);

    async fn on_accept(&self, session_id: i64);

    async fn on_connect(&self, session_id: i64);

    async fn on_receive(&self, session_id: i64, message: Vec<u8>);

    async fn on_disconnected(&self, session_id: i64);
}


