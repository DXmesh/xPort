use std::{net::SocketAddr, sync::atomic::AtomicI64};

use tracing::info;
use x_common_lib::{base::id_generator::make_channel_id, utils::time_utils};

use tokio::{
    io::BufWriter,
    net::tcp::OwnedWriteHalf,
    sync::{broadcast, Mutex},
};

pub struct Session {
    // 上一次接收信息的时间
    last_recv_timestamp: AtomicI64,
    //
    id: i64,
    // ip 地址
    ip: String,
    // 端口号
    port: u16,

    pub exit_signal: broadcast::Sender<()>,
    //
    // is_active: bool, // 是否是主动连接
    // 连接信息
    conn_id: i64,
    // is_close: AtomicBool,
    pub writer: Mutex<BufWriter<OwnedWriteHalf>>,
}

impl Session {
    pub fn new(
        addr: SocketAddr,
        conn_id: i64,
        is_active: bool,
        writer: OwnedWriteHalf,
    ) -> Box<Session> {
        let port = addr.port();
        let ip = addr.ip().to_string();

        info!("ip = {}, port = {}, conn_id = {}", ip, port, conn_id);
        let (exit_signal, _) = broadcast::channel::<()>(1);

        let session_id = make_channel_id(&ip, port, is_active);
        Box::new(Session {
            last_recv_timestamp: AtomicI64::new(0),
            ip,
            port,
            exit_signal,
            id: session_id,
            conn_id,
            writer: Mutex::new(BufWriter::new(writer)),
        })
    }

    pub fn get_ip(&self) -> String {
        self.ip.clone()
    }
    

    pub fn get_conn_id(&self) -> i64 {
        self.conn_id
    }

    pub fn get_id(&self) -> i64 {
        self.id
    }

    pub fn get_port(&self) -> u16 {
        self.port
    }

    pub fn update_recv_timestamp(&self) {
        self.last_recv_timestamp.store(time_utils::cur_timestamp(), std::sync::atomic::Ordering::Relaxed);
    }
}
