use x_common_lib::base::config::Config;

pub mod ipc_net;
pub mod net;
mod session;
pub mod tcp_net;
pub mod xrpc_event_handler;

pub fn get_max_message_size(config: &Box<Config>) -> u32 {
    let max_size = config.get_str("xport", "message-max-size").unwrap_or("4M");

    let multiplier: u32 = match max_size.chars().last().expect("message-max-size 格式出错！") {
        'K' | 'k' => 1024,
        'M' | 'm' => 1024 * 1024,
        'G' | 'g' => 1024 * 1024 * 1024,
        _ => 1,
    };
    let num_part: &str = if max_size.chars().last().expect("message-max-size 格式出错！").is_ascii_alphabetic() {
        &max_size[..max_size.len() - 1]
    } else {
        max_size
    };

    num_part
        .parse::<u32>()
        .ok()
        .map(|num| num * multiplier)
        .unwrap()
}
