use crate::service::ipc_service::ipc_service::IPCService;
use crate::service::service::Service;
use std::sync::Arc;
use tokio::io::AsyncReadExt;
#[cfg(unix)]
use tokio::io::BufReader;
use tracing::{info, warn};

use std::ptr;

pub struct IPCNet {}

impl IPCNet {
    pub fn new() -> Self {
        IPCNet {}
    }

    #[cfg(unix)]
    pub async fn start_receive_message(&self, ipc_service: Arc<Box<IPCService>>) {
        use crate::{application::get_runtime, event::publish_ipc_service_disconnect};
        use tokio::net::UnixListener;
        use x_common_lib::utils::file_utils;
        let listen_addr = ipc_service.get_listen_addr();

        let is_succeed = file_utils::create_file_path_sync(&listen_addr);

        if !is_succeed {
            info!(
                "创建 unix socket {:?} 出错，关闭 IPC service。。。",
                listen_addr
            );
            ipc_service.stop().await;

            return;
        }

        let ret = file_utils::remove_file_sync(&listen_addr);

        if let Err(_err) = ret {
            info!(
                "创建 unix socket {:?} 出错，关闭 IPC service。。。",
                listen_addr
            );
            ipc_service.stop().await;

            return;
        }

        let mut try_time = 0;
        info!("监听 ipc, 地址: {}", &listen_addr);
        let listener = UnixListener::bind(listen_addr.clone());
        if let Err(err) = listener {
            info!("监听 unix socket {:?} 出错，关闭 IPC service。。。", err);
            ipc_service.stop().await;
            return;
        }
        let listener = listener.unwrap();
        while !ipc_service.is_exit() || try_time >= 3 {
            let socket = listener.accept().await;
            if let Err(err) = socket {
                warn!("监听失败 ipc:{:?}", err);
                break;
            }
            let (stream, _) = socket.unwrap();
            let (reader, writer) = stream.into_split();
            ipc_service.set_writer(writer).await;

            try_time = 0;
            let mut header = [0u8; 4];
            let mut reader = BufReader::new(reader);

            info!("dxc 连接ipc : {}", &listen_addr);

            while !ipc_service.is_exit() {
                let result = reader.read_exact(&mut header).await;
                if let Err(err) = result {
                    // ipc 断开时发送发送事件
                    let service_info = ipc_service.get_service_info();
                    get_runtime().spawn(async move {
                        publish_ipc_service_disconnect(service_info).await;
                    });
                    warn!("监听失败 ipc:{:?}", err);
                    break;
                }
                let msg_size = u32::from_le_bytes(header);
                let message_len = msg_size as usize;
                let mut message: Vec<u8> = Vec::with_capacity(message_len);

                unsafe {
                    message.set_len(message_len);
                    ptr::copy_nonoverlapping(header.as_ptr(), message.as_mut_ptr(), 4);
                }

                let msg_buffer = &mut message[4..];

                let result = reader.read_exact(msg_buffer).await;
                if let Err(err) = result {
                    warn!("监听失败 ipc:{:?}", err);
                    break;
                }

                ipc_service.disptach_api_invoke(message);
            }
        }
    }

    #[cfg(windows)]
    pub async fn start_receive_message(&self, ipc_service: Arc<Box<IPCService>>) {
        use tokio::net::windows::named_pipe::ServerOptions;

        use crate::{application::get_runtime, event::publish_ipc_service_disconnect};
        let (read_listen_addr, write_listen_addr) = ipc_service.get_listen_addr();

        let mut try_time = 0;

        let mut is_first_time = true;

        while !ipc_service.is_exit() || try_time >= 3 {
            //
            info!(
                "监听 ipc, 读地址：{}, 写地址: {}",
                &read_listen_addr, &write_listen_addr
            );
            //
            let read_pipe = ServerOptions::new()
                .first_pipe_instance(is_first_time)
                .create(&read_listen_addr);
            //

            if let Err(err) = read_pipe {
                info!("监听命令管道出错:{:?}，关闭 IPC service。。。", err);
                ipc_service.stop().await;
                try_time += 1;
                continue;
            }

            let write_pipe = ServerOptions::new()
                .first_pipe_instance(is_first_time)
                .create(&write_listen_addr);

            if let Err(err) = write_pipe {
                info!("监听命令管道出错:{:?}，关闭 IPC service。。。", err);
                ipc_service.stop().await;
                try_time += 1;
                continue;
            }

            is_first_time = false;
            let mut read_pipe = read_pipe.unwrap();
            let write_pipe = write_pipe.unwrap();
            let ret = read_pipe.connect().await;

            //
            info!("dxc 连接ipc : {}", &read_listen_addr);

            if let Err(err) = ret {
                info!("监听命令管道出错:{:?}，关闭 IPC service。。。", err);
                ipc_service.stop().await;
                try_time += 1;
                continue;
            }
            let ret = write_pipe.connect().await;
            info!("dxc 连接ipc : {}", &write_listen_addr);
            if let Err(err) = ret {
                info!("监听命令管道出错:{:?}，关闭 IPC service。。。", err);
                ipc_service.stop().await;
                try_time += 1;
                continue;
            }

            ipc_service.set_writer(write_pipe).await;
            try_time = 0;
            let mut header = [0u8; 4];
            while !ipc_service.is_exit() {
                let result = read_pipe.read_exact(&mut header).await;
                if let Err(err) = result {
                    // ipc 断开时发送发送事件
                    let service_info = ipc_service.get_service_info();
                    get_runtime().spawn(async move {
                        publish_ipc_service_disconnect(service_info).await;
                    });

                    warn!("监听失败 ipc:{:?}", err);
                    break;
                }
                //
                let msg_size = u32::from_le_bytes(header);
                let message_len = msg_size as usize;
                let mut message: Vec<u8> = Vec::with_capacity(message_len);
                //
                unsafe {
                    message.set_len(message_len);
                    ptr::copy_nonoverlapping(header.as_ptr(), message.as_mut_ptr(), 4);
                }
                //
                let msg_buffer = &mut message[4..];
                let result = read_pipe.read_exact(msg_buffer).await;
                if let Err(err) = result {
                    warn!("监听失败 ipc:{:?}", err);
                    break;
                }
                ipc_service.disptach_api_invoke(message);
            }
        }
    }
}
