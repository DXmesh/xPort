use application::{get_runtime, init_runtime, Application};
use structopt::StructOpt;
use tracing::info;
use x_common_lib::utils::ps_utils;
use xport_lib::application;

#[derive(StructOpt, Debug)]
#[structopt(name = "xPort", about = "DXMesh 接入基座", version = "0.1.8")]

struct XPortCommand {
    #[structopt(short, long, help = "节点私钥", default_value = "")]
    private_key: String,
}

fn main() {
    let is_running = ps_utils::is_process_running_by_file("pid");
    if is_running {
        panic!("xPort 运行中，请勿重复运行！")
    }
    let mut env_args = XPortCommand::from_args();
    //
    if env_args.private_key.is_empty() {
        // 这个地址用于调试使用，正式使用要传入真实私钥
        env_args.private_key =
            String::from("41e5badd2bf1673c2bc77a7261d011d217cef73dde1735b69e1ef1c386becbe6");
    }

    // let  env_args = XPortCommand::from_args();

    init_runtime();
    let runtime = get_runtime();
    runtime.block_on(async move {
        // web3 模式启动，每一个 xNode 要有独立的私钥
        Application::init(env_args.private_key).await;

        ps_utils::save_pid_to_file(std::process::id(), "pid");
        info!("启动成功...");
        Application::run_loop().await;
    });
}
